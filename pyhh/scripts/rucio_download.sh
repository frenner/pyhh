#!/bin/bash

export X509_USER_PROXY=$1
voms-proxy-info -all -file $1
RUCIO_ACCOUNT=frenner

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

lsetup rucio

rucio download $2 --dir $3 --ndownloader 5 --replica-selection random 
