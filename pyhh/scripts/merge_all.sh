# samples to merge
declare -a samples=(
    run2
    l1cvv0cv1
    # ps
    # cHHH01d0
    # l1cvv1cv1
    # l1cvv1p5cv1
    # l2cvv1cv1
    # l10cvv1cv1
    # l1cvv1cv0p5
    # l5mcvv1cv0p5
    # ttbar
)

# now loop through the above array
for i in "${samples[@]}"; do
    echo "$i"
    pyhh merge --sample $i --hists &  
    pyhh merge --sample $i --dumped &

    # pyhh merge --sample $i --hists --debug &  

done

