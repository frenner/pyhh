#!/usr/bin/env python3

import subprocess

# copy template header to submit file
subprocess.call(
    (
        (
            "cp /lustre/fs22/group/atlas/freder/hh/pyhh/pyhh/scripts/rucio_download_config.txt"
            " /lustre/fs22/group/atlas/freder/hh/submit/rucio_download.sub"
        ),
    ),
    shell=True,
)

# fmt: off

# dont forget to add _TREE !!
# dont forget to voms and then start new shell!  
files = [
# "user.frenner.EJ_2024_03_27.512190.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1_largeRfilt.e8338_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512190.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1_largeRfilt.e8338_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512190.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1_largeRfilt.e8338_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512189.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1_largeRfilt.e8338_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512189.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1_largeRfilt.e8338_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512189.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1_largeRfilt.e8338_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.508788.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1_largeRfilt.e8338_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.508788.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1_largeRfilt.e8338_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.508788.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1_largeRfilt.e8338_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512188.MGPy8EG_hh_bbbb_vbf_novhh_l0cvv1cv1_largeRfilt.e8338_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512188.MGPy8EG_hh_bbbb_vbf_novhh_l0cvv1cv1_largeRfilt.e8338_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.512188.MGPy8EG_hh_bbbb_vbf_novhh_l0cvv1cv1_largeRfilt.e8338_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.507684.MGPy8EG_hh_bbbb_vbf_novhh_l5mcvv1cv0p5.e8338_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.507684.MGPy8EG_hh_bbbb_vbf_novhh_l5mcvv1cv0p5.e8338_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.507684.MGPy8EG_hh_bbbb_vbf_novhh_l5mcvv1cv0p5.e8338_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502979.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv0p5.e8263_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502979.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv0p5.e8263_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502979.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv0p5.e8263_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502978.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1.e8263_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502978.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1.e8263_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502978.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1.e8263_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502977.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1.e8263_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502977.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1.e8263_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502977.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1.e8263_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502973.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1p5cv1.e8263_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502973.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1p5cv1.e8263_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502973.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1p5cv1.e8263_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502970.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1.e8263_s3681_r13145_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502970.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1.e8263_s3681_r13167_p5855_TREE",
# "user.frenner.EJ_2024_03_27.502970.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1.e8263_s3681_r13144_p5855_TREE",
# "user.frenner.EJ_2024_04_19.600464.PhPy8EG_PDF4LHC15_HH4b_cHHH10d0.e8222_s3681_r13167_p6026_TREE",
# "user.frenner.EJ_2024_04_19.600464.PhPy8EG_PDF4LHC15_HH4b_cHHH10d0.e8222_s3681_r13145_p6026_TREE",
# "user.frenner.EJ_2024_04_19.600464.PhPy8EG_PDF4LHC15_HH4b_cHHH10d0.e8222_s3681_r13144_p6026_TREE",
# "user.frenner.EJ_2024_04_19.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.e8222_s3681_r13167_p6026_TREE",
# "user.frenner.EJ_2024_04_19.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.e8222_s3681_r13145_p6026_TREE",
# "user.frenner.EJ_2024_04_19.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.e8222_s3681_r13144_p6026_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506971.e8273_s3681_r13167_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506971.e8273_s3681_r13145_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506971.e8273_s3681_r13144_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506970.e8273_s3681_r13167_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506970.e8273_s3681_r13145_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506970.e8273_s3681_r13144_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506969.e8273_s3681_r13167_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506969.e8273_s3681_r13145_p5855_TREE",
"user.frenner.EJ_2024_06_15_T090710.2024_06_15_T090710.506969.e8273_s3681_r13144_p5855_TREE",
]

# write jobs per line

with open("/lustre/fs22/group/atlas/freder/hh/submit/rucio_download.sub", "a") as f:
    for i, file in enumerate(files):
        f.write(f"arguments = $(Proxy_path) {file} /lustre/fs22/group/atlas/freder/hh/samples")
        f.write("\n")
        f.write("queue")
        f.write("\n")
