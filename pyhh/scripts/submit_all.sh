# jump out if any error
set -e

echo "testing..."
pyhh select --test
echo "submit..."

# samples to submit
declare -a samples=(
    l1cvv0cv1
    ps
    cHHH01d0
    l1cvv1cv1
    # l1cvv1p5cv1
    # l2cvv1cv1
    # l10cvv1cv1
    # l1cvv1cv0p5
    # l5mcvv1cv0p5
    # ttbar
    run2
)

# copy and run from submit folder
rm -rf /lustre/fs22/group/atlas/freder/hh/submit/pyhh/
rsync -r --exclude=.git /lustre/fs22/group/atlas/freder/hh/pyhh /lustre/fs22/group/atlas/freder/hh/submit/

# now loop through the above array
for i in "${samples[@]}"; do

    if [[ $i == *"run2"* ]]; then
        pyhh make-submit --sample $i
    elif [[ $i == *"cHHH01d0"* ]]; then
        pyhh make-submit --sample $i
    elif [[ $i == *"ttbar"* ]]; then
        pyhh make-submit --sample $i
    elif [[ $i == *"ps"* ]]; then
        pyhh make-submit --sample $i
    else
        pyhh make-submit --sample $i --do_systematics
    fi
    mkdir /lustre/fs22/group/atlas/freder/hh/submit/$i -p
    cd /lustre/fs22/group/atlas/freder/hh/submit/$i
    condor_submit /lustre/fs22/group/atlas/freder/hh/submit/select_$i.sub

done
