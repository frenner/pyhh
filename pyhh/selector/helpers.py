import copy
import itertools
import json
from operator import xor

import equinox as eqx
import numpy as np
import tomatos.nn_setup
import vector
from sklearn.preprocessing import MinMaxScaler


def get_xbb_score(self, event, lrj_idx):
    # https://xbb-docs.docs.cern.ch/Xbb/GN2_track/#scores-in-the-dxaod
    # top fraction
    f_top = 0.25
    f_hcc = 0.02

    D_h_bb_score = np.log(
        self.xbb_phbb[event][lrj_idx]
        / (
            (f_hcc * self.xbb_phcc[event][lrj_idx])
            + (f_top * self.xbb_ptop[event][lrj_idx])
            + ((1.0 - f_top - f_hcc) * self.xbb_pqcd[event][lrj_idx])
        )
    )
    return D_h_bb_score


def get_xbb_tags(self, event):
    # set defaults
    self.lrj_xbb_wp[event] = np.full(self.n_large_R[event], 100, dtype=float)
    self.lrj_xbb_score[event] = np.full(self.n_large_R[event], -1, dtype=float)
    for lrj_idx in range(self.n_large_R[event]):
        # make sure inside flat mass window
        if 50e3 < self.lrj_m[event][lrj_idx] < 800e3:
            # find cut value for large R mass
            which_mass_bin = np.histogram(
                self.lrj_m[event][lrj_idx],
                self.config.xbb_flat_mass_eff["m_bin_edges"],
            )[0]
            bin_idx = np.where(which_mass_bin)[0][0]
            score = get_xbb_score(self, event, lrj_idx)
            if score > self.config.xbb_flat_mass_eff["50"][bin_idx]:
                self.lrj_xbb_wp[event][lrj_idx] = 50
                self.lrj_xbb_score[event][lrj_idx] = score
            elif score > self.config.xbb_flat_mass_eff["55"][bin_idx]:
                self.lrj_xbb_wp[event][lrj_idx] = 55
                self.lrj_xbb_score[event][lrj_idx] = score
            elif score > self.config.xbb_flat_mass_eff["60"][bin_idx]:
                self.lrj_xbb_wp[event][lrj_idx] = 60
                self.lrj_xbb_score[event][lrj_idx] = score


def large_R_select(self, event):
    """
    this selects large R's in the procedure:
    - preselect kinematic Jet/ETmiss recommendation
    - find the ones with xbb <= 60
    - if two large R jets
        - Higgs candidates by pt sorting

    Parameters
    ----------
    event : int
        event nr
    """

    # Jet/ETmiss recommendation
    # pt_cuts = (self.lrj_pt[event] > 200e3) & (self.lrj_pt[event] < 2_400e3)
    # m_cuts = (self.lrj_m[event] > 40e3) & (self.lrj_m[event] < 600e3)

    # can go directly to pt>250e3 and m>50e3 for xbb calibration
    pt_cuts = (self.lrj_pt[event] > 250e3) & (self.lrj_pt[event] < 2_400e3)
    m_cuts = (self.lrj_m[event] > 50e3) & (self.lrj_m[event] < 600e3)
    eta_cut = np.abs(self.lrj_eta[event]) < 2.0

    kin_select = np.array((pt_cuts & m_cuts & eta_cut), dtype=bool)
    self.n_lrj_kin_sel[event] = np.count_nonzero(kin_select)
    if self.n_lrj_kin_sel[event] >= 2:
        self.pass_two_fat_jets[event] = True

    # looks a bit complicated, but necessary if we don't want to copy
    # containers around
    # example at the bottom
    if self.pass_two_fat_jets[event]:
        # find the leading pt ones that pass the kinemattic selection
        # (this becomes a subset of the original array)
        pt_ordered_indices = np.flip(np.argsort(self.lrj_pt[event][kin_select]))
        # find the true bool indices of kin_select
        # these are the corresponding kinemattic selected indices of the original
        # array
        kin_select_indices = np.where(kin_select)[0]
        # self.lrj_pt[event] = [20, 400e3, 500e3]
        # kin_select = [False, True, True]
        # pt_ordered_indices = [1, 0]
        # kin_select_indices = [1, 2]
        # kin_select_indices[pt_ordered_indices] = [2, 1]
        self.sel_lrj_idx_1[event] = kin_select_indices[pt_ordered_indices[0]]
        self.sel_lrj_idx_2[event] = kin_select_indices[pt_ordered_indices[1]]

        # write xbb score and count
        self.xbb_score_h1[event] = self.lrj_xbb_score[event][self.sel_lrj_idx_1[event]]
        self.xbb_score_h2[event] = self.lrj_xbb_score[event][self.sel_lrj_idx_2[event]]
        h1_pass = self.lrj_xbb_wp[event][self.sel_lrj_idx_1[event]] <= 60
        h2_pass = self.lrj_xbb_wp[event][self.sel_lrj_idx_2[event]] <= 60
        self.n_lrj_xbb_pass[event] = sum([h1_pass, h2_pass])
        if self.n_lrj_xbb_pass[event] >= 2:
            self.pass_two_hbb_jets[event] = True

        self.lead_xbb_score[event] = np.max(
            [self.xbb_score_h1[event], self.xbb_score_h2[event]]
        )

        pt_h1 = self.lrj_pt[event][self.sel_lrj_idx_1[event]]
        # m_h1 = self.lrj_m[event][self.sel_lrj_idx_1[event]]
        # if pt_h1 > 520e3 and m_h1 > 70e3:

        if pt_h1 > 450e3:
            if self.config.is_mc and pt_h1 < 800e3:
                apply_trigger_sf(self, event, pt_h1)
            self.pass_fat_jet_pt[event] = True


def apply_trigger_sf(self, event, pt_h1):
    binned_pt_h1 = np.histogram(pt_h1, self.config.trigger_sf["edges"])[0]
    which_bin = np.where(binned_pt_h1)[0][0]
    trigger_sf = self.config.trigger_sf["eff"][which_bin]
    if self.sys == "trigger_sf__1up":
        self.weights[event] *= trigger_sf + self.config.trigger_sf["error"][which_bin]
    elif self.sys == "trigger_sf__1down":
        self.weights[event] *= trigger_sf - self.config.trigger_sf["error"][which_bin]
    else:
        self.weights[event] *= trigger_sf
    return


def hh_p4(self, event):
    if self.pass_fat_jet_pt[event]:
        pt_h1 = self.lrj_pt[event][self.sel_lrj_idx_1[event]]
        eta_h1 = self.lrj_eta[event][self.sel_lrj_idx_1[event]]
        phi_h1 = self.lrj_phi[event][self.sel_lrj_idx_1[event]]
        m_h1 = self.lrj_m[event][self.sel_lrj_idx_1[event]]

        pt_h2 = self.lrj_pt[event][self.sel_lrj_idx_2[event]]
        eta_h2 = self.lrj_eta[event][self.sel_lrj_idx_2[event]]
        phi_h2 = self.lrj_phi[event][self.sel_lrj_idx_2[event]]
        m_h2 = self.lrj_m[event][self.sel_lrj_idx_2[event]]

        self.h1_p4 = vector.obj(
            pt=pt_h1,
            eta=eta_h1,
            phi=phi_h1,
            m=m_h1,
        )
        self.h2_p4 = vector.obj(
            pt=pt_h2,
            eta=eta_h2,
            phi=phi_h2,
            m=m_h2,
        )

        self.pt_h1[event] = pt_h1
        self.eta_h1[event] = eta_h1
        self.phi_h1[event] = phi_h1
        self.m_h1[event] = m_h1

        self.pt_h2[event] = pt_h2
        self.eta_h2[event] = eta_h2
        self.phi_h2[event] = phi_h2
        self.m_h2[event] = m_h2

        if self.h1_p4.deltaR(self.h2_p4) < 2.0:
            self.pass_fat_jet_pt[event] = False
            return

        # this is expensive
        hh = self.h1_p4 + self.h2_p4

        self.pt_hh[event] = hh.pt
        self.eta_hh[event] = hh.eta
        self.phi_hh[event] = hh.phi
        self.m_hh[event] = hh.mass
        self.m_hh_3[event] = hh.mass
        self.m_hh_4[event] = hh.mass
        self.m_hh_5[event] = hh.mass
        self.m_hh_6[event] = hh.mass
        self.m_hh_7[event] = hh.mass

        self.pt_hh_scalar[event] = pt_h1 + pt_h2


def vbf_select(self, event):
    if self.pass_fat_jet_pt[event]:
        etaCut = np.abs(self.srj_eta[event]) < 4.5
        ptCut = np.abs(self.srj_pt[event]) > 20e3
        selected = np.array((etaCut & ptCut), dtype=bool)
        n_jets_selected = np.count_nonzero(selected)
        if n_jets_selected >= 2:
            # get the indices of the selected ones
            selected_indices = np.nonzero(selected)[0]
            for i in selected_indices:
                # check if they are outside of the large R's
                jet_p4 = vector.obj(
                    pt=self.srj_pt[event][i],
                    eta=self.srj_eta[event][i],
                    phi=self.srj_phi[event][i],
                    m=self.srj_m[event][i],
                )
                if (jet_p4.deltaR(self.h1_p4) > 1.4) & (
                    jet_p4.deltaR(self.h2_p4) > 1.4
                ):
                    self.vbf_candidates_p4[event].append(jet_p4)
        # save two leading vbf jets
        if len(self.vbf_candidates_p4[event]) >= 2:
            self.pass_vbf_jets[event] = True
            vbf_p4_pts = [p4.pt for p4 in self.vbf_candidates_p4[event]]
            sorted_indices = np.argsort(vbf_p4_pts)
            self.vbf_idx_1[event] = sorted_indices[-1]
            self.vbf_idx_2[event] = sorted_indices[-2]


def vbf_cut(self, event):
    if self.pass_vbf_jets[event]:
        vbf_jet_1_p4 = self.vbf_candidates_p4[event][self.vbf_idx_1[event]]
        vbf_jet_2_p4 = self.vbf_candidates_p4[event][self.vbf_idx_2[event]]
        jj = vbf_jet_1_p4 + vbf_jet_2_p4
        self.m_jj[event] = jj.mass
        m_jj_pass = jj.mass > self.config.vbf_m_jj_cut
        self.eta_jj[event] = np.abs(vbf_jet_1_p4.eta - vbf_jet_2_p4.eta)
        eta_jj_pass = self.eta_jj[event] > self.config.vbf_eta_jj_cut
        if m_jj_pass and eta_jj_pass:
            self.pass_vbf_cut[event] = True
            self.pt_j1[event] = vbf_jet_1_p4.pt
            self.eta_j1[event] = vbf_jet_1_p4.eta
            self.phi_j1[event] = vbf_jet_1_p4.phi
            self.E_j1[event] = vbf_jet_1_p4.E
            self.pt_j2[event] = vbf_jet_2_p4.pt
            self.eta_j2[event] = vbf_jet_2_p4.eta
            self.phi_j2[event] = vbf_jet_2_p4.phi
            self.E_j2[event] = vbf_jet_2_p4.E


class NeuralNet:
    def __init__(self, model_name, epoch_name):
        self.name = model_name
        model_path = (
            "/lustre/fs22/group/atlas/freder/hh/run/tomatos/" + model_name + "/"
        )
        with open(model_path + "metadata.json", "r") as file:
            nn_metadata = json.load(file)
            # create nn
            self.vars = nn_metadata["config"]["vars"]
            if "cls" in model_name:
                with open(model_path + "models/infer_metrics.json", "r") as file:
                    infer_metrics = json.load(file)
                    self.inverted = infer_metrics[epoch_name]["inverted"]
                    self.vbf_cut = infer_metrics[epoch_name]["vbf_cut"]
                    self.eta_cut = infer_metrics[epoch_name]["eta_cut"]
            else:
                self.vbf_cut = 0.0
                self.eta_cut = 0.0
                self.inverted = 0

            self.n_features = len(self.vars)
            model = tomatos.nn_setup.NeuralNetwork(n_features=self.n_features)
            # load nn parameters
            self.model = eqx.tree_deserialise_leaves(
                model_path + f"/models/{epoch_name}.eqx", model
            )
            # remove k for write out
            self.merged_name, k_fold_str = model_name.split("_k_")
            self.k_fold = int(k_fold_str)

            # min max scaling the inputs as used for the training
            # clipping sets outliers to min, max
            self.scaler = MinMaxScaler(clip=True)
            self.scaler.scale_ = nn_metadata["config"]["scaler_scale"]
            self.scaler.min_ = nn_metadata["config"]["scaler_min"]

    def predict(self, input):
        # scale
        scaled_input = self.scaler.transform(input)
        # predict
        prediction = self.model(scaled_input[0, :])[0]
        # flip the hist in case (bkg,signal) is inverted
        if self.inverted:
            prediction = np.abs(prediction - 1.0)
        # to capture 1.0 in 1.0 bin
        return np.minimum(prediction, 0.999)


def get_nn_inputs(self, vars, event):
    input = np.zeros((1, len(vars)))
    # fill input vector with values
    for i, var in enumerate(vars):
        input[0, i] = getattr(self, var)[event]
    return input


def nn(self, event):
    if self.pass_vbf_cut[event]:
        fold_idx = self.k_fold_idx_lookup[event]
        for nn in self.neural_nets:
            if nn.k_fold == fold_idx:
                input = get_nn_inputs(self, nn.vars, event)
                # this logic is odd i know, but for now
                vbf_pass = input[0, -2] > nn.vbf_cut and input[0, -1] > nn.eta_cut
                self.pass_vbf_cut[event] = vbf_pass
                if vbf_pass:
                    getattr(self, nn.merged_name)[event] = nn.predict(input)


# in GeV to be correct
m_h1_center = 124.0
m_h2_center = 117.0
fm_h1 = 1500.0
fm_h2 = 1900.0


def Xhh(m_h1, m_h2):
    m_h1 = m_h1 * 1e-3
    m_h2 = m_h2 * 1e-3
    return np.sqrt(
        np.power((m_h1 - m_h1_center) / (fm_h1 / m_h1), 2)
        + np.power((m_h2 - m_h2_center) / (fm_h2 / m_h2), 2)
    )


def CR_hh(m_h1, m_h2):
    m_h1 = m_h1 * 1e-3
    m_h2 = m_h2 * 1e-3
    return (
        np.sqrt(
            np.power((m_h1 - m_h1_center) / (0.1 * np.log(m_h1)), 2)
            + np.power((m_h2 - m_h2_center) / (0.1 * np.log(m_h2)), 2)
        )
        * 1e3  # go back to MeV
    )


def above_diag_line(m_h1, m_h2):
    # digonal line from y=mx+b
    m = m_h2_center / m_h1_center
    m_h2_line = m * m_h1

    return m_h2 > m_h2_line


def hh_regions(self, event):
    # calculate region variables
    if self.pass_vbf_cut[event]:
        # https://cds.cern.ch/record/2848140/files/ATL-COM-PHYS-2023-033.pdf
        self.X_HH[event] = Xhh(self.m_h1[event], self.m_h2[event])
        self.CR_hh[event] = CR_hh(self.m_h1[event], self.m_h2[event])

        self.SR_mass_sel[event] = self.X_HH[event] < 1.6
        self.VR_mass_sel[event] = (self.X_HH[event] > 1.6) & (
            self.CR_hh[event] < 100.0e3
        )
        self.CR_mass_sel[event] = (self.CR_hh[event] > 100.0e3) & (
            self.CR_hh[event] < 170.0e3
        )

        self.NW[event] = above_diag_line(self.m_h1[event], self.m_h2[event])


def get_scale_factor(self, pt, wp):
    xbb_sf = self.config.xbb_sf
    which_bin = np.histogram(pt, xbb_sf["pt_bin_edges"])[0]
    pt_bin_idx = np.where(which_bin)[0][0]
    if self.xbb_sys:
        if self.sys == f"xbb_pt_bin_{pt_bin_idx}__1up":
            return xbb_sf[wp]["up"][pt_bin_idx]
        elif self.sys == f"xbb_pt_bin_{pt_bin_idx}__1down":
            return xbb_sf[wp]["down"][pt_bin_idx]
        else:
            return xbb_sf[wp]["nominal"][pt_bin_idx]
    else:
        return xbb_sf[wp]["nominal"][pt_bin_idx]


def apply_xbb_sf(self, event):
    if self.pass_vbf_cut[event]:
        xbb_wp_h1 = self.lrj_xbb_wp[event][self.sel_lrj_idx_1[event]]
        xbb_wp_h2 = self.lrj_xbb_wp[event][self.sel_lrj_idx_2[event]]
        if xbb_wp_h1 < 100:
            self.weights[event] *= get_scale_factor(
                self, pt=self.pt_h1[event], wp=xbb_wp_h1
            )
        if xbb_wp_h2 < 100:
            self.weights[event] *= get_scale_factor(
                self, pt=self.pt_h2[event], wp=xbb_wp_h2
            )


def cut_flow(self):
    self.initial_events = np.ones(self.n_events, dtype=int)
    self.pass_trig_boosted = self.trigger
    # self.pass_two_fat_jets
    # self.pass_two_hbb_jets
    # self.pass_fat_jet_pt
    self.pass_vbf_jets = self.pass_two_hbb_jets & self.pass_vbf_jets
    self.pass_vbf_cut = self.pass_two_hbb_jets & self.pass_vbf_cut
    if self.config.BLIND:
        self.pass_SR = np.zeros(self.n_events, dtype=bool)
    else:
        self.pass_SR = self.pass_vbf_cut & self.SR_mass_sel


def trigger_reference(self, event):
    # check if event has at least one Large R
    if self.lrj_pt[event].shape[0] > 0:
        # cannot use sel_pt_sort as they are selected!
        # maxPtIndex = np.argmax(self.lrj_pt[event])
        self.leadingLargeRpt[event] = np.max(self.lrj_pt[event])
        # maxMIndex = np.argmax(self.lrj_m[event])
        self.leadingLargeRm[event] = np.max(self.lrj_m[event])
        if self.leadingLargeRm[event] > 50_000.0:
            self.leadingLargeRmassGreater50[event] = True
        if self.leadingLargeRpt[event] > 500_000.0:
            self.leadingLargeRpTGreater500[event] = True


def get_vr(self, event):
    if self.sel_two_lrj[event]:
        # get their corresponding vr jets, (vector of vectors)
        # need ._values as it comes as STL vector uproot object, instead of
        # .tolist() it comes already as np.ndarray
        j1_VRs = self.vr_btag_77[event]._values[self.sel_lrj_idx_1[event]]._values
        j2_VRs = self.vr_btag_77[event]._values[self.sel_lrj_idx_2[event]]._values
        # count their tags
        j1_VRs_Btag = np.count_nonzero(j1_VRs)
        j2_VRs_Btag = np.count_nonzero(j2_VRs)
        j1_VRs_noBtag = len(j1_VRs) - j1_VRs_Btag
        j2_VRs_noBtag = len(j2_VRs) - j2_VRs_Btag

        # this is not mutually exclusive with >=
        if xor(
            j1_VRs_Btag == 1 and j2_VRs_noBtag >= 1,
            j2_VRs_Btag == 1 and j1_VRs_noBtag >= 1,
        ):
            self.btagLow_1b1j[event] = True
        if xor(
            j1_VRs_Btag >= 2 and j2_VRs_noBtag >= 1,
            j2_VRs_Btag >= 2 and j1_VRs_noBtag >= 1,
        ):
            self.btagLow_2b1j[event] = True
        if xor(
            j1_VRs_Btag >= 2 and j2_VRs_noBtag >= 2,
            j2_VRs_Btag >= 2 and j1_VRs_noBtag >= 2,
        ):
            self.btagLow_2b2j[event] = True
        if j1_VRs_Btag == 1 and j2_VRs_Btag == 1:
            self.btagHigh_1b1b[event] = True
        if xor(
            j1_VRs_Btag >= 2 and j2_VRs_Btag == 1,
            j2_VRs_Btag >= 2 and j1_VRs_Btag == 1,
        ):
            self.btagHigh_2b1b[event] = True
        if j1_VRs_Btag >= 2 and j2_VRs_Btag >= 2:
            self.btagHigh_2b2b[event] = True

        self.dR_VR_h1[event] = self.vr_deltaR12[event][self.sel_lrj_idx_1[event]]
        self.dR_VR_h2[event] = self.vr_deltaR12[event][self.sel_lrj_idx_2[event]]

        # get pt of the btagged ones
        h1_btag_VR_pts = self.vr_pt[event][self.sel_lrj_idx_1[event]][
            j1_VRs.astype(bool)
        ]
        h2_btag_VR_pts = self.vr_pt[event][self.sel_lrj_idx_2[event]][
            j2_VRs.astype(bool)
        ]
        # the following is fine as they come pt sorted already
        # h1
        if h1_btag_VR_pts.shape[0] == 2:
            self.pt_h1_btag_vr_1[event] = h1_btag_VR_pts[0]
            self.pt_h1_btag_vr_2[event] = h1_btag_VR_pts[1]
        # h2
        if h2_btag_VR_pts.shape[0] == 2:
            self.pt_h2_btag_vr_1[event] = h2_btag_VR_pts[0]
            self.pt_h2_btag_vr_2[event] = h2_btag_VR_pts[1]


def truth_mhh(self, event):
    truth_h1_p4 = vector.obj(
        pt=self.vars_arr["truth_H1_pt"][event],
        eta=self.vars_arr["truth_H1_eta"][event],
        phi=self.vars_arr["truth_H1_phi"][event],
        m=self.vars_arr["truth_H1_m"][event],
    )
    truth_h2_p4 = vector.obj(
        pt=self.vars_arr["truth_H2_pt"][event],
        eta=self.vars_arr["truth_H2_eta"][event],
        phi=self.vars_arr["truth_H2_phi"][event],
        m=self.vars_arr["truth_H2_m"][event],
    )
    self.truth_m_hh[event] = (truth_h1_p4 + truth_h2_p4).mass


def hist_mapping(self):
    """
    Define which array and selection the to be filled hist gets, for all
    variables/hists and regions, in the format m_hh -> m_hh.CR_xbb_2. This puts
    a dict with the structure onto self.final_selection like:

    self.final_selection = {
        "histname": {
            "var": some_array
            "sel": None or bool_array,
        },
        ...
    }
    """

    self.final_selection = {}

    hists_without_sel = {}
    for var in self.config.cutflow_vars:
        hists_without_sel[var] = {
            "var": getattr(self, var),
            "sel": None,
        }

    # add single hists to final selection
    self.final_selection.update(hists_without_sel)

    # init values and weights for hists
    hists_with_sel = {}
    for var in self.config.vars:
        if "massplane" in var:
            continue
        hists_with_sel[var + "_" + self.sys] = {
            "var": getattr(self, var),
            "sel": None,
        }

    # now replicate hists that will get a selection applied
    # e.g. m_hh --> m_hh.CR_xbb_2
    for sel_key, sel in self.selections.items():
        for hist_name, var_sel_dict in hists_with_sel.items():
            # write sel e.g. SR_xbb_2
            var_sel_dict["sel"] = sel
            # need to make a deep copy as dict assignments just creates references
            self.final_selection[hist_name + "." + sel_key] = copy.deepcopy(
                var_sel_dict
            )


def decorate(self):
    """
    applies selection to vars and attach weights like
    results["histkey"] = [selected_var, weights]
    """
    for hist in self.final_selection.keys():
        # if list of lists build var/weights manually
        # this is bad...
        if isinstance(self.final_selection[hist]["var"], list) or (
            self.final_selection[hist]["var"].dtype == object
        ):
            self.final_selection[hist]["var"], w = flatten2d(
                self.final_selection[hist]["var"],
                self.weights,
                self.final_selection[hist]["sel"],
            )
            self.final_selection[hist]["sel"] = None
        else:
            w = None

        # get final values with according weights
        self.results[hist] = attach_weights(
            self,
            var=self.final_selection[hist]["var"],
            sel=self.final_selection[hist]["sel"],
            custom_weights=w,
        )

    # add massplane
    if self.sys == "NOSYS":
        for region, region_bool in self.selections.items():
            self.results["massplane_" + self.sys + "." + region] = [
                np.array(
                    [
                        self.m_h1[region_bool],
                        self.m_h2[region_bool],
                    ]
                ).T,
                np.array(self.weights[region_bool]),
            ]


def attach_weights(self, var, sel=None, custom_weights=None):
    """
    Apply selection to var and get their corresponding weights

    Parameters
    ----------
    var : np.ndarray
        var
    sel : np.ndarray, optional
        array holding booleans to select on var, by default None
    weight : float, optional
        weights for the hists, by default None

    Returns
    -------
    var_with_weights : list
        selected var with corresponding weights
    """

    if sel is None:
        if custom_weights is None:
            var_with_weights = [var, self.weights]
        else:
            var_with_weights = [var, custom_weights]
    else:
        if custom_weights is None:
            var_with_weights = [var[sel], self.weights[sel]]

    return var_with_weights


def flatten2d(arr, weights, sel=None):
    """
    flatten 2d inhomogeneous array and replicate weights values per event

    Parameters
    ----------
    arr : list of lists
        list holding lists of values
    weights : nd.array
        weights per event same shape as len(arr)
    sel : np.array, optional
        event selection, by default None
    Returns
    -------
    flatArr, flatArrWeights

    """
    if sel is None:
        selection = np.full(len(arr), True, dtype=bool)
    else:
        selection = sel

    # get selection, list is dimensionally inhomogeneous
    selectedArr = list(itertools.compress(arr, selection))
    # itertools.chain.from_iterable flattens
    flatArr = np.array(list(itertools.chain.from_iterable(selectedArr)))
    # replicate weight values per number of objects in event
    flatArrWeights = np.array(
        list(
            itertools.chain.from_iterable(
                [
                    np.repeat(w, len(values))
                    for values, w in zip(selectedArr, weights[selection])
                ]
            )
        )
    )

    return flatArr, flatArrWeights
