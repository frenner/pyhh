import copy
import json
import os
import pathlib

import h5py

# import numpy as np
import selector.tools
import uproot
from selector.histograms import FloatHistogram, FloatHistogram2D
from tools.logging import log

run_path = "/lustre/fs22/group/atlas/freder/hh/run/"
hist_top_path = run_path + "histograms/"
dump_top_path = run_path + "dump/"


class Setup:
    def __init__(self, args):
        if args.cluster:
            # multiprocessing is nice to properly release memory per batch
            # to run on same cpu core as main program, even with cpus=1 a child
            # process is spawned on another cpu core if not dummy
            import multiprocessing.dummy as multiprocessing

            self.cpus = 1

        else:
            import multiprocessing

            # let's not be too mean :)
            self.cpus = int(multiprocessing.cpu_count() * 2 / 3)

        # fill hists
        self.fill = args.fill
        # dump vars
        self.dump = args.dump
        self.do_systematics = args.do_systematics
        self.file = args.file
        self.tree = uproot.models.TTree
        file_parts = self.file.split("/")
        dataset = file_parts[-2]
        file = file_parts[-1]

        histpath = hist_top_path + dataset + "/"
        dumppath = dump_top_path + dataset + "/"
        if args.fill:
            if not os.path.isdir(histpath):
                os.makedirs(histpath)
        if args.dump:
            if not os.path.isdir(dumppath):
                os.makedirs(dumppath)
        self.hist_out_file = histpath + file + ".h5"
        self.dump_file = dumppath + file + ".h5"

        # auto setup blind if data
        if "data1" in self.file or "data2" in self.file:
            self.is_data = True
            self.is_mc = False
        else:
            self.is_data = False
            self.is_mc = True

        self.BLIND = self.is_data

        # get metadata
        if self.is_data:
            self.meta_data = {}
            dataset_folder = pathlib.PurePath(args.file).parent.name
            yr = [y for y in dataset_folder.split("_") if "data1" in y]
            self.meta_data["dataYear"] = "20" + yr[0].rsplit("data")[-1]
        if self.is_mc:
            self.meta_data = selector.tools.get_metadata_from_file(args.file)
            # no calibration in release 24 yet --> 30% estimate
            self.xbb_sf = selector.tools.get_xbb_sf(estimate=True)
            self.trigger_sf = selector.tools.get_trigger_sf()
        self.xbb_flat_mass_eff = selector.tools.get_xbb_flat_mass_efficiency()

        # init event range to load
        self.batch = [0, 0]

        self.n_subsets = 4
        # splits sets for the folds
        self.subset_permutations = {
            3: [0, 1, 2],  # k_fold = 3
            0: [1, 2, 3],  # k_fold = 0
            1: [2, 3, 0],  # k_fold = 1
            2: [3, 0, 1],  # k_fold = 2
        }

        # split the given data into reproducible shuffled subsets
        self.run_event_subset = False
        self.this_subset = 3

        # # this is global so needs to be set to 0.0 for nn currently
        self.vbf_m_jj_cut = 0  # 327321.8137719149  # mhh neos 327321.8137719149
        self.vbf_eta_jj_cut = 0  # 0.10157335292534106  # mhh neos 0.10157335292534106

        # vars to write with selections
        # works with the ones that are set up in selector.analysis

        self.vars = [
            # "m_hh_5",
            "tomatos_bce_5_10000_lr_0p0005",
            # "tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_1000",
            # "tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_20000"
            # "tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000"
            # "tomatos_cls_5_2000_study_4_lr_0p0005_bw_min_0p005_slope_20000_k2v1p5"
            # "tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p001_slope_5000",
            # "tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p01_slope_5000",
            # "tomatos_cls_5_2000_study_10_lr_0p0005_bw_min_0p001_slope_20000_protect_0p001"
            # "tomatos_cls_5_2000_study_11_lr_0p0005_bw_min_0p001_slope_20000_protect_0p001_best_train"
            # "tomatos_cls_5_10000_study_8_lr_0p0005_bw_min_0p001_slope_20000"
            # "tomatos_cls_5_2000_study_12_lr_0p0005_bw_min_0p001_slope_20000_best_bins"
            # check bins and cut before submit!
            # ----
            # "pt_h1",
            # "eta_h1",
            # "phi_h1",
            # "m_h1",
            # "pt_h2",
            # "eta_h2",
            # "phi_h2",
            # "m_h2",
            # "pt_hh",
            # "eta_hh",
            # "phi_hh",
            # "m_hh",
            # "pt_j1",
            # "eta_j1",
            # "phi_j1",
            # "E_j1",
            # "pt_j2",
            # "eta_j2",
            # "phi_j2",
            # "E_j2",
            # "lead_xbb_score",
            # "m_jj",
            # "eta_jj",
            # "massplane",  # runs over NOSYS only, careful with selection here
            # ------
            # "leadingLargeRpt",
            # "leadingLargeRm",
        ]

        self.epoch_name = "epoch_05000"
        self.nn_models = [v for v in self.vars if "tomatos" in v]
        self.selections = [
            "SR_xbb_2",
            "SR_xbb_1",
            "VR_xbb_1",
            "VR_xbb_2",
            "CR_xbb_2",
            "CR_xbb_1",
            # "VR_xbb_1_NW",
            # "VR_xbb_2_NW",
            # "VR_xbb_1_SE",
            # "VR_xbb_2_SE",
            # "xbb_2",
            # "xbb_1",
            # "trigger_trigger_ref_m_50",
            # "trigger_ref_m_50",
            # "trigger_m_50",
            # "all",
            # "trigger_ref_pt_500",
            # "trigger_pt_500",
        ]

        # basic settings
        self.n_events = "All"
        self.batch_size = 10_000
        self.sys_type_filter = []
        self.sys_filter = []

        if args.do_systematics:
            self.batch_size = 2_000
            # some filtering if desired, if empty run all
            self.sys_type_filter = [
                # "NOSYS",
                # "xbb_sf",
                # "generator",
                # "large_r_sys",
                # "pileup",
                # "trigger_sf",
            ]
            self.sys_filter = {
                # "large_r_sys": [
                # ],
                # "small_r_sys": [
                #     "JET_EtaIntercalibration_NonClosure_PreRec__1up",
                #     "JET_EtaIntercalibration_Modelling__1up",
                #     "JET_EtaIntercalibration_NonClosure_PreRec__1down",
                #     "JET_EtaIntercalibration_Modelling__1down",
                # ],
                # "generator": [
                #     "GEN_MUR05_MUF05_PDF260000",
                #     "GEN_MUR05_MUF10_PDF260000",
                #     "GEN_MUR10_MUF05_PDF260000",
                #     "GEN_MUR10_MUF10_PDF260000",
                #     "GEN_MUR10_MUF20_PDF260000",
                #     "GEN_MUR20_MUF10_PDF260000",
                #     "GEN_MUR20_MUF20_PDF260000",
                # ],
            }

        if args.cluster:
            self.cpus = 1
            if args.do_systematics:
                self.batch_size = 1000

        if args.debug:
            self.hist_out_file = run_path + "histograms/hists-debug.h5"
            self.dump_file = run_path + "dump/dump-debug.h5"
            self.n_events = 200
            self.batch_size = 100
            if args.do_systematics:
                self.n_events = 20
                self.batch_size = 10
            self.cpus = 1
            self.fill = True
            self.dump = True
            if not os.path.isdir(dumppath):
                os.makedirs(dumppath)

    def get_vars_to_load(self):
        """
        Figure out vars to load from the ones used in the analysis script and all
        the systematics if wanted. Decorates config with list : config.load_vars
        and dict : config.systematics

        e.g.
        config.systematics["small_r_jets"] = [JET_JER_DataVsMC_MC16__1up,...]

        """

        # vars in the file
        tree_vars = self.tree.keys()

        # figure out vars to load from the ones used in the analysis script
        start_var = 'vars_arr["'
        start_sys = 'vars_arr[f"'
        end = '"]'
        analysis_vars = []
        systematics = {
            "NOSYS": ["NOSYS"],  # looks odd but keeps compatibility
            "small_r_sys": [],
            "large_r_sys": [],
            "pileup": [],
            "xbb_sf": [],
            "generator": [],
            "trigger_sf": [],
        }
        analysis_path = pathlib.Path(__file__).parent / "analysis.py"

        for line in open(analysis_path, "r"):
            if "#" not in line:
                # non-systematic variables
                if start_var in line:
                    var = (line.split(start_var))[1].split(end)[0]
                    analysis_vars.append(var)
                # transform found systematic variables to NOSYS format
                if start_sys in line:
                    var = (line.split(start_sys))[1].split(end)[0]
                    if "lrj_sys" in var:
                        var = var.replace("{lrj_sys", "")
                    if "srj_sys" in var:
                        var = var.replace("{srj_sys", "")
                    var = var.replace("}", "NOSYS")
                    analysis_vars.append(var)

        # take only vars that exist
        load_vars = list(set(tree_vars).intersection(analysis_vars))
        load_vars += ["eventNumber"]

        # save them for once per vars to avoid matching strings everywhere
        if self.do_systematics:
            for v in tree_vars:
                if "NOSYS" in v:
                    continue
                # get existing systs, just choose one variable like "pt" as the
                # same systs are on each kinematic variable
                if "recojet_antikt4PFlow_" in v and "pt" in v:
                    # somehow flavor systematics are on both large and small r,
                    # remove small r ones
                    if "Flavor" in v:
                        continue
                    v = v.replace("recojet_antikt4PFlow_", "")
                    v__split = v.split("__")
                    sys = v__split[0]
                    up_down_rest = v__split[1]
                    up_down = up_down_rest.split("_")[0]
                    full_sys = sys + "__" + up_down
                    if full_sys not in systematics["small_r_sys"]:
                        systematics["small_r_sys"] += [full_sys]
                if "recojet_antikt10UFO_" in v and "pt" in v:
                    load_vars += [v]
                    v = v.replace("recojet_antikt10UFO_", "")
                    v__split = v.split("__")
                    sys = v__split[0]
                    up_down_rest = v__split[1]
                    up_down = up_down_rest.split("_")[0]
                    full_sys = sys + "__" + up_down
                    if full_sys not in systematics["large_r_sys"]:
                        systematics["large_r_sys"] += [full_sys]
                if "PileupWeight_" in v:
                    load_vars += [v]
                    v = v.replace("PileupWeight_", "")
                    v__split = v.split("__")
                    sys = v__split[0]
                    up_down = v__split[1]
                    systematics["pileup"] += [sys + "__" + up_down]
                if "generatorWeight_" in v:
                    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgSystematicUncertaintyRecipes?rev=27
                    # chapter 6.2 https://arxiv.org/pdf/1510.03865.pdf
                    if any(
                        [
                            "260000" in v,  # scale variations
                            "GEN_MUR10_MUF10_PDF265000" in v,  # alpha_s variations
                            "GEN_MUR10_MUF10_PDF266000" in v,
                            "GEN_MUR10_MUF10_PDF904"
                            in v,  # 904xx are 32 pdf variations
                        ]
                    ):
                        # these should be excluded
                        if "MUR05_MUF20" in v or "GEN_MUR20_MUF05_PDF260000" in v:
                            continue

                        load_vars += [v]
                        v = v.replace("generatorWeight_", "")
                        systematics["generator"] += [v]

            for v in analysis_vars:
                # their systs are on the large R itself
                if "leadingVRTrackJets" in v or "Xbb2020v3" in v:
                    continue
                if "recojet_antikt4" in v:
                    for sys in systematics["small_r_sys"]:
                        load_vars += [v.replace("NOSYS", sys)]
                if "recojet_antikt10" in v:
                    for sys in systematics["large_r_sys"]:
                        load_vars += [v.replace("NOSYS", sys)]

            systematics["xbb_sf"] += [
                "xbb_pt_bin_0__1up",
                "xbb_pt_bin_0__1down",
                "xbb_pt_bin_1__1up",
                "xbb_pt_bin_1__1down",
                "xbb_pt_bin_2__1up",
                "xbb_pt_bin_2__1down",
                "xbb_pt_bin_3__1up",
                "xbb_pt_bin_3__1down",
            ]

            systematics["trigger_sf"] += [
                "trigger_sf__1up",
                "trigger_sf__1down",
            ]
        # if filter given, keep only systematics of a certain type
        if len(self.sys_type_filter) != 0:
            systematics = {key: systematics[key] for key in self.sys_type_filter}

        # if one wants to keep only specific ones
        if len(self.sys_filter) != 0:
            for k, v in self.sys_filter.items():
                systematics[k] = v

        self.systematics = systematics
        self.load_vars = load_vars

    def get_vars_with_syst_and_selections(self):
        """
        puts together vars like m_hh_NOSYS.SR_2b2b
        """
        self.vars_with_systs = []
        self.vars_with_systs_and_selections = []
        for sys_object, sys_list in self.systematics.items():
            for sys in sys_list:
                for var in self.vars:
                    vars_with_sys = var + "_" + sys
                    self.vars_with_systs += [vars_with_sys]
                    for reg in self.selections:
                        self.vars_with_systs_and_selections += [
                            vars_with_sys + "." + reg
                        ]

        self.cutflow_vars = [
            "initial_events",
            "pass_trig_boosted",
            "pass_two_fat_jets",
            "pass_two_hbb_jets",
            "pass_vbf_jets",
            "pass_fat_jet_pt",
            "pass_vbf_cut",
            "pass_SR",
        ]

    def setup_hists(self):
        """
        get defined hists and lists of variables and analysis regions

        """

        # some binning templates for hists
        # acc_eff_binning = {"binrange": (0, 5_000_000), "bins": 50}
        # pt_h_binning = {"binrange": (0.2e6, 1e6), "bins": 50}
        # trigger_eff_pt = {"binrange": (0, 3_000_000), "bins": 150}
        # trigger_eff_m = {"binrange": (0, 300_000), "bins": 150}
        # dR_bins = {"binrange": (0, 1.2), "bins": 50}
        # count = {"binrange": (0, 2), "bins": 2}
        # vr_binning = {"binrange": (0, 0.5e6), "bins": 50}
        template_bins = {}
        template_bins["pt_h"] = {"binrange": (0, 1_800e3), "bins": 20}
        template_bins["eta_h"] = {"binrange": (-2, 2), "bins": 20}
        template_bins["phi_h"] = {"binrange": (-3.15, 3.15), "bins": 20}
        template_bins["m_h"] = {"binrange": (0, 300e3), "bins": 20}
        template_bins["pt_hh"] = {"binrange": (0, 600e3), "bins": 20}
        template_bins["eta_hh"] = {"binrange": (-6, 6), "bins": 20}
        template_bins["phi_hh"] = {"binrange": (-3.15, 3.15), "bins": 20}
        template_bins["pt_vbf"] = {"binrange": (0, 400e3), "bins": 20}
        template_bins["eta_vbf"] = {"binrange": (-4.5, 4.5), "bins": 20}
        template_bins["phi_vbf"] = {"binrange": (-3.15, 3.15), "bins": 20}
        template_bins["E_vbf"] = {"binrange": (0, 4_000e3), "bins": 20}
        template_bins["m_hh"] = {
            "binrange": (500_000, 2_500_000),
            # "bins": [
            #     500000.0,
            #     1038400.0,
            #     1228200.0,
            #     1468000.0,
            #     1842800.0,
            #     5000000.0,
            # ],  # 0.0
            # "bins": [
            #     500000.0,
            #     1038200.0,
            #     1226600.0,
            #     1466800.0,
            #     1840800.0,
            #     5000000.0,
            # ],  # 250
            # "bins": [
            #     500000.0,
            #     907200.0,
            #     1023800.0,
            #     1157800.0,
            #     1407000.0,
            #     5000000.0,
            # ],  # 1000, 4.5
            # "bins": [
            #     500000.0,
            #     906000.0,
            #     1036800.0,
            #     1184800.0,
            #     1445200.0,
            #     5000000.0,
            # ],  # 1012,
            "bins": [
                500000,
                803003.64745,
                936398.80040,
                1386106.96867,
                2082128.54449,
                5000000.0,
            ],  # neos k2v=0
            # "bins": [
            #     500000,
            #     825310.44409,
            #     1029662.67060,
            #     1475197.80410,
            #     2062016.01605,
            #     5000000.0,
            # ],  # neos k2v=1p5
        }

        template_bins["count"] = {"binrange": (0, 1), "bins": 1}

        # map the binning to the vars
        vars_binning_map = {
            # h1
            "pt_h1": template_bins["pt_h"],
            "eta_h1": template_bins["eta_h"],
            "phi_h1": template_bins["phi_h"],
            "m_h1": template_bins["m_h"],
            # h2
            "pt_h2": template_bins["pt_h"],
            "eta_h2": template_bins["eta_h"],
            "phi_h2": template_bins["phi_h"],
            "m_h2": template_bins["m_h"],
            # hh
            "pt_hh": template_bins["pt_hh"],
            "eta_hh": template_bins["eta_hh"],
            "phi_hh": template_bins["phi_hh"],
            "m_hh": template_bins["m_hh"],
            "m_hh_3": {"binrange": (500_000, 3_000_000), "bins": 3},
            "m_hh_4": {"binrange": (500_000, 3_000_000), "bins": 4},
            # "m_hh_5": {"binrange": (500_000, 3_000_000), "bins": 5},
            "m_hh_5": template_bins["m_hh"],
            "m_hh_6": {"binrange": (500_000, 3_000_000), "bins": 6},
            "m_hh_7": {"binrange": (500_000, 3_000_000), "bins": 7},
            # vbf 1
            "pt_j1": template_bins["pt_vbf"],
            "eta_j1": template_bins["eta_vbf"],
            "phi_j1": template_bins["phi_vbf"],
            "E_j1": template_bins["E_vbf"],
            # vbf 2
            "pt_j2": template_bins["pt_vbf"],
            "eta_j2": template_bins["eta_vbf"],
            "phi_j2": template_bins["phi_vbf"],
            "E_j2": template_bins["E_vbf"],
            # m_jj
            "m_jj": {"binrange": (0, 4_000_000), "bins": 25},
            "eta_jj": template_bins["eta_hh"],
            "lead_xbb_score": {"binrange": (0, 10), "bins": 25},
            "leadingLargeRpt": {"binrange": (250_000, 1_000_000), "bins": 75},
            "leadingLargeRm": {"binrange": (40_000, 300_000), "bins": 75},
        }
        # binning for nn's
        for model in self.nn_models:
            model_path = (
                "/lustre/fs22/group/atlas/freder/hh/run/tomatos/" + model + "_k_0/"
            )
            with open(model_path + "metadata.json", "r") as file:
                nn_metadata = json.load(file)
                bins = nn_metadata["config"]["bins"]
                # if nn_metadata["config"]["include_bins"]:

                #     # with open(model_path + "models/infer_metrics.json", "r") as file:
                #     #     infer_metrics = json.load(file)
                #     #     self.inverted = infer_metrics[epoch_name]["inverted"]

                #     bins = nn_metadata["metrics"]["bins"][-1]
                # in case binning optimization was used
                bins[0] = 0.0
                bins[-1] = 1.0
                vars_binning_map[model] = {
                    "binrange": (0, 1),
                    # "bins": [
                    #     0.0,
                    #     0.35444,
                    #     0.50956,
                    #     0.62511,
                    #     0.74342,
                    #     0.90549,
                    # ],
                    "bins": bins,
                }

        # warning if no hist setup for a variable
        for v in self.vars:
            if "massplane" in v:
                continue
            if v not in vars_binning_map.keys():
                log.warning(
                    f"variable: {v}, is setup but there is no histogram defined"
                )

        self.hists = []

        for cut in self.cutflow_vars:
            self.hists += [
                FloatHistogram(
                    name=cut,
                    binrange=(0.5, 1.5),
                    bins=1,
                ),
            ]

        # use hists_with_sel as template to construct further down for all selections
        hists_without_sel = [
            # FloatHistogram2D(
            #     name="massplane",
            #     binrange1=(50_000, 250_000),
            #     binrange2=(50_000, 250_000),
            #     bins=100,
            # ),
        ]

        hists_with_systs = []
        for sys_type, sys_list in self.systematics.items():
            for sys in sys_list:
                for var in self.vars:
                    if "massplane" in var and sys == "NOSYS":
                        hists_with_systs += (
                            FloatHistogram2D(
                                name="massplane" + "_" + sys,
                                binrange1=(50_000, 250_000),
                                binrange2=(50_000, 250_000),
                                bins=100,
                            ),
                        )
                    elif var not in vars_binning_map.keys():
                        continue
                    else:
                        hists_with_systs += [
                            FloatHistogram(
                                name=var + "_" + sys,
                                binrange=vars_binning_map[var]["binrange"],
                                bins=vars_binning_map[var]["bins"],
                            ),
                        ]
        hists_without_sel += hists_with_systs

        # construct hists for all regions and kinematic vars
        for h in hists_without_sel:
            # without selection
            var = getattr(h, "_name")
            for reg in self.selections:
                var_with_reg = var + "." + reg
                newHist = copy.deepcopy(h)
                newHist._name = var_with_reg
                self.hists.append(newHist)

    def event_ranges(self):
        """
        construct ranges, batch_size=1000 gives e.g.
        [[0, 1000], [1000, 2000], [2000, 3000],...]

        """
        ranges = []
        batch_ranges = []
        if self.n_events == "All":
            self.n_events = self.tree.num_entries
        for i in range(0, self.n_events, self.batch_size):
            ranges += [i]
        # add very last index to not include
        if self.n_events not in ranges:
            ranges += [self.n_events]
        for i, j in zip(ranges[:-1], ranges[1:]):
            batch_ranges += [[i, j]]

        self.event_batches = batch_ranges

    def init_dump_file(self):
        """
        init dump file with datasets from histkeys and the according weight

        """
        # weight_systs hold the systematics with modified weights
        self.weight_systs = []
        with h5py.File(self.dump_file, "w") as f:
            for var in self.vars_with_systs_and_selections:
                f.create_dataset(
                    var,
                    (0,),
                    maxshape=(None,),
                    compression="gzip",
                    dtype="f4",
                )
            if self.is_mc:
                for sys_object, sys_list in self.systematics.items():
                    for sys in sys_list:
                        self.weight_systs.append(sys)
                        for sel in self.selections:
                            f.create_dataset(
                                "weights_" + sys + "." + sel,
                                (0,),
                                maxshape=(None,),
                                compression="gzip",
                                dtype="f4",
                            )
