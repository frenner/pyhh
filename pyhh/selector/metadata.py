import csv
import glob
import json
import os
import pathlib
import re

import pyAMI.client
import pyAMI_atlas.api as AtlasAPI
import uproot
from tools.logging import log

mcCampaign = {
    "r13167": ["2015", "2016"],  # "mc20a", run2, 2015-16
    "r13144": ["2017"],  # "mc20d", run2, 2017
    "r13145": ["2018"],  # "mc20e", run2, 2018
    "r13829": ["2022"],  # "mc21a", run3, 2022
    # # ptag
    # mc20 = "p5057"
}
metadata_file = pathlib.Path(__file__).parent / "metadata.json"

# slide 11 here explains the following,
# rel21 sticks with one hypothesis for the k-factor normalization
# https://indico.cern.ch/event/919518/contributions/3921302/attachments/2070155/3475083/LHCHHMeeting_070720.pdf
# https://gitlab.cern.ch/hh4b/rdf-utils/-/blob/master/mc-config/update-xsecs.py?ref_type=heads#L11-12
vbf_kfac = 1.726 / 1.3943  # For now, extrapolate from SM point
vbf_kfac2021 = (0.000472947 / 0.000401477) * vbf_kfac


def setup_pyami():
    client = pyAMI.client.Client("atlas")
    AtlasAPI.init()
    return client


def CombineCutBookkeepers(filelist):
    cutBookkeeper = {}
    cutBookkeeper["initial_events"] = 0
    cutBookkeeper["initial_sum_of_weights"] = 0
    cutBookkeeper["initial_sum_of_weights_squared"] = 0
    for file_ in filelist:
        with uproot.open(file_) as file:
            for key in file.keys():
                if "CutBookkeeper" and "NOSYS" in key:
                    cbk = file[key].to_numpy()
                    cutBookkeeper["initial_events"] += cbk[0][0]
                    cutBookkeeper["initial_sum_of_weights"] += cbk[0][1]
                    cutBookkeeper["initial_sum_of_weights_squared"] += cbk[0][2]
    return cutBookkeeper


def get(filepath, client):
    """
    queries the ami info and writes it to a json file
    Parameters
    ----------
    file : str
       filepath
    client : pyAMI.client.Client
        pyami client object
    """

    datasetName = ConstructDatasetName(filepath)
    # query info
    if not os.path.exists(metadata_file):
        os.mknod(metadata_file)
        md = {}
    else:
        md = json.load(open(metadata_file))

    if datasetName not in md:
        log.info(f"query metadata for: {datasetName}")
        # need to do p wildcard search as too old ones get deleted
        datasetNames = datasetName[:-4] + "%"
        datasets = AtlasAPI.list_datasets(
            client, patterns=datasetNames, type="DAOD_PHYS"
        )
        ds_info = AtlasAPI.get_dataset_info(client, dataset=datasets[0]["ldn"])
        md[datasetName] = ds_info[0]
        ds_nr = int(ds_info[0]["datasetNumber"])

        # add kfactor either from ami or PMG file
        if "kFactor@PMG" in ds_info:
            md[datasetName]["kFactor"] = float(ds_info["kFactor@PMG"])
        else:
            if "mc20" in datasetName:
                pmgFile = pathlib.Path(__file__).parent / "PMGxsecDB_mc16.txt"
            if "mc21" in datasetName:
                pmgFile = pathlib.Path(__file__).parent / "PMGxsecDB_mc21.txt"
            with open(pmgFile) as fd:
                # dataset_number/I:physics_short/C:crossSection/D:genFiltEff/D:kFactor/D:relUncertUP/D:relUncertDOWN/D:generator_name/C:etag/C
                rd = csv.reader(fd, delimiter="\t")
                for i, row in enumerate(rd):
                    if i == 0:
                        continue
                    # print(int(row[0]))
                    if int(row[0]) == ds_nr:
                        # delete empty strings
                        row = list(filter(None, row))
                        md[datasetName]["kFactor"] = float(row[4])

        if 506969 <= ds_nr <= 506971:
            pmgFile = pathlib.Path(__file__).parent / "PMGxsecDB_mc16.txt"
            with open(pmgFile) as fd:
                # dataset_number/I:physics_short/C:crossSection/D:genFiltEff/D:kFactor/D:relUncertUP/D:relUncertDOWN/D:generator_name/C:etag/C
                rd = csv.reader(fd, delimiter="\t")
                for i, row in enumerate(rd):
                    if i == 0:
                        continue
                    if int(row[0]) == ds_nr:
                        # delete empty strings
                        row = list(filter(None, row))
                        md[datasetName]["crossSection"] = float(row[2]) * 1e-3
                        md[datasetName]["genFiltEff"] = float(row[3])
        if 502970 <= ds_nr <= 502981 or ds_nr == 507684 or 506969 <= ds_nr <= 506971:
            # correct N^3LO correction
            md[datasetName]["kFactor"] = (
                float(md[datasetName]["kFactor"]) * vbf_kfac2021
            )
            # multiply by 4b branching ratio for signal as xsec is always given for
            # entire hh decay
            md[datasetName]["crossSection"] = (
                float(md[datasetName]["crossSection"]) * 0.3392
            )

        # get all files in dataset to sum up their sum_of_weights
        filelist = glob.glob(os.path.dirname(filepath) + "/*.root")
        cbk = CombineCutBookkeepers(filelist)
        md[datasetName]["initial_events"] = cbk["initial_events"]
        md[datasetName]["initial_sum_of_weights"] = cbk["initial_sum_of_weights"]

    json.dump(md, open(metadata_file, "w"))


def ConstructDatasetName(filepath):
    """
    Constructs original logical datasetname from ntuples folder name

    Parameters
    ----------
    filepath : str

    Returns
    -------
    datasetName : str
    """
    folder = filepath.split("/")[-2]
    # get ami tags until r-tag as p always changes
    ami = re.findall("e[0-9]{4}.s[0-9]{4}.r[0-9]{5}", folder)
    r_tag = ami[0][-6:]
    # construct logical dataset name from ntuple name
    ds_parts = folder.split(".")
    ds_parts = ds_parts[-3:]
    # remove TREE
    ds_parts[-1] = ds_parts[-1][:-5]
    if int(r_tag[1:]) < 13829:
        project = ["mc20_13TeV"]
    else:
        project = ["mc21_13p6TeV"]
    ds = project + ds_parts
    ds.insert(-1, "deriv.DAOD_PHYS")
    datasetName = ".".join([str(x) for x in ds])

    return datasetName
