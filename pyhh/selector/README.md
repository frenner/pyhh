flowchart TB

    subgraph Selector
        Configuration --> init([Initialize])
        Configuration --> mp[Multiprocessing Pool]
        init --> histograms
        init --> ntuple[n-tuple]

        callback --> append
        append --> ntuple
        callback --> fill
        fill --> histograms
        callback --> |Next Event Batch| mp
        
    end
        Results--> callback

    subgraph Analysis

        os[Object Selection] -->lv[load variables]
        lv--> select[select]

        select --> dc[decorate Results]

        dc --> |Next Inputset| os
        Results 

    end

    mp -->|Child Process|Analysis

    
