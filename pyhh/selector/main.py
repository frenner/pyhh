#!/usr/bin/env python3
import multiprocessing

import h5py
import selector.analysis
import selector.configuration
import selector.tools
import uproot
from tools.logging import log
from tqdm.auto import tqdm

# success state of the program
success = 0


def run(args):
    """
    Main program for HH-->4b VBF boosted selection. It runs
    selector.analysis.run() jobs per event batch and executes the callback
    function each time it finishes. The callback does thread-safe histogram
    filling and variable dumping.

    Parameters
    ----------
    args : namespace
        args from the pyhh.main entry program
    """

    def callback(results):
        """
        The filling/dumping is executed each time a selector.analysis.run job finishes.
        This is executed sequentially so no data races.

        Parameters
        ----------
        results : dict
            analysis.ObjectSelection.results
        """

        # update bin heights per iteration
        if config.fill:
            for hist in config.hists:
                if hist._name not in results.keys():
                    log.warning(
                        f"histogram with name: {hist._name} defined but not in results"
                    )
                    continue
                res = results[hist._name]
                hist.fill(values=res[0], weights=res[1])

        # dump variables by appending to dump file
        if config.dump:
            with h5py.File(config.dump_file, "r+") as f:
                for var in config.vars_with_systs_and_selections:
                    # skip the massplane for now
                    if "massplane" in var:
                        continue
                    res = results[var]
                    selector.tools.write_vars_to_h5(f, res[0], var)

                if config.is_mc:
                    # go over weights which are modified by a sys
                    for weight_sys in config.weight_systs:
                        for sel in config.selections:
                            for var in config.vars_with_systs_and_selections:
                                # need to only keep one per selection
                                if sel in var and weight_sys in var:
                                    selector.tools.write_vars_to_h5(
                                        f,
                                        results[var][1],
                                        "weights_" + weight_sys + "." + sel,
                                    )
                                    break

        pbar.update(config.batch_size)

        return

    def error_callback(error):
        """
        handles error if selector.analysis.job fails

        Parameters
        ----------
        error : BaseException
            error
        """
        log.error(error.__cause__)
        # stops currently running processes
        pool.terminate()
        # prevents more jobs submissions
        pool.close()
        # can't come up with something better, it is just terrible to get the success
        # state of apply_async jobs without blowing up the memory with returned results
        global success
        success = 1

    # ----------- here the actual program starts ----------

    # get configuration
    config = selector.configuration.Setup(args)

    log.info("Processing file " + args.file)
    with uproot.open(args.file) as file:
        # access the tree
        config.tree = file["AnalysisMiniTree"]
        # figure out which vars to load
        config.get_vars_to_load()
        config.get_vars_with_syst_and_selections()
        if config.fill:
            config.setup_hists()
        if config.dump:
            config.init_dump_file()
        # make list of event ranges for batching
        config.event_ranges()
        # progressbar
        pbar = tqdm(total=config.tree.num_entries, position=0, leave=True)

        # RUN
        if args.debug:
            # this is basically no multiprocessing
            for batch in config.event_batches:
                results = selector.analysis.run(config, batch)
                callback(results)
        else:
            # a pool object can start child processes on different cpu cores,
            # this properly releases memory per batch
            pool = multiprocessing.Pool(config.cpus)
            for batch in config.event_batches:
                # can't put batch in config because of async
                pool.apply_async(
                    selector.analysis.run,
                    (config, batch),
                    callback=callback,
                    error_callback=error_callback,
                )
            pool.close()
            pool.join()
            pbar.close()

    # write histograms after filling to file
    if config.fill:
        with h5py.File(config.hist_out_file, "w") as outfile:
            for hist in config.hists:
                hist.write(outfile)
            log.info("Wrote Hists to: " + config.hist_out_file)

    # just log if dumped
    if config.dump:
        log.info("Dumped selected vars to: " + config.dump_file)

    return success
