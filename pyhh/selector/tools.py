import csv
import glob
import json
import os
import pathlib
import re

import numpy as np
import pandas as pd
import selector.analysis
import selector.configuration
import selector.metadata
from selector.metadata import ConstructDatasetName
from tools.logging import log

md_file = pathlib.Path(__file__).parent / "metadata.json"

mcCampaign = {
    "r13167": ["2015", "2016"],  # "mc20a", run2, 2015-16
    "r13144": ["2017"],  # "mc20d", run2, 2017
    "r13145": ["2018"],  # "mc20e", run2, 2018
    "r13829": ["2022"],  # "mc21a", run3, 2022
}


def get_metadata_from_file(filepath):
    """
    Gets necessary metdadata from metdata file for selection, e.g. eventweight.
    Also does a ami query if metadata for file doesn't exist.

    Parameters
    ----------
    file : string
        filepath
    Returns
    -------
    metadata : dict
        metadata
    """
    meta_data = {}

    # get r-tag for datayears
    ami = re.findall("e[0-9]{4}.s[0-9]{4}.r[0-9]{5}", filepath)
    r_tag = ami[0][-6:]
    meta_data["dataYears"] = mcCampaign[r_tag]

    # get logical dataset name from ntuple name
    datasetName = ConstructDatasetName(filepath)
    log.info("Original Dataset Name: " + datasetName)

    if not os.path.exists(md_file):
        os.mknod(md_file)
        md = {}
    else:
        md = json.load(open(md_file))

    if datasetName not in md:
        log.info("metadata not in json yet, will query from ami")
        client = selector.metadata.setup_pyami()
        selector.metadata.get(filepath, client)
        md = json.load(open(md_file))

    ds_info = md[datasetName]
    meta_data["genFiltEff"] = float(ds_info["genFiltEff"])
    meta_data["crossSection"] = float(ds_info["crossSection"])
    meta_data["kFactor"] = float(ds_info["kFactor"])
    meta_data["events"] = float(ds_info["initial_events"])
    meta_data["sum_of_weights"] = float(ds_info["initial_sum_of_weights"])

    return meta_data


def construct_file_list(
    sample_name, merge_processed_hists=False, merge_processed_dumps=False, verbose=False
):
    """
    make list with files of a given dataset

    Parameters
    ----------
    sample_name : str
        short name e.g. l1cvv1cv1
    merge_processed_hists : bool, optional
        construct filelist for processed hists
    merge_processed_dumps : bool, optional
        construct filelist for processed dumps

    Returns
    -------
    filelist : list
        list of strings with full sample-paths
    """
    #  6-Term VBF Combination Sample Variations
    # 𝜿2𝑽 𝜿𝝀 𝜿𝑽
    # 1 1 1
    # 1.5 1 1
    # 1 2 1
    # 1 10 1
    # 1 1 0.5
    # 1 -5 0.5
    if sample_name == "l1cvv1cv1":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.508788.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1_largeRfilt.e8338_s3681_r*/*.root"
    if sample_name == "l1cvv1p5cv1":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.502973.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1p5cv1.e8263_s3681_r*/*.root"
    if sample_name == "l2cvv1cv1":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.512189.MGPy8EG_hh_bbbb_vbf_novhh_l2cvv1cv1_largeRfilt.e8338_s3681_r*/*.root"
    if sample_name == "l10cvv1cv1":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.512190.MGPy8EG_hh_bbbb_vbf_novhh_l10cvv1cv1_largeRfilt.e8338_s3681_r*/*.root"
    if sample_name == "l1cvv1cv0p5":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.502979.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv0p5.e8263_s3681_r*/*.root"
    if sample_name == "l5mcvv1cv0p5":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.507684.MGPy8EG_hh_bbbb_vbf_novhh_l5mcvv1cv0p5.e8338_s3681_r*/*.root"
    # k2v=0
    if sample_name == "l1cvv0cv1":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r*/*.root"
        # pattern_17 = "user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13144*/*.root"
        # pattern_18 = "user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13145*/*.root"
    # ggf
    if sample_name == "cHHH01d0":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_04_19.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.e8222_s3681_r*/*.root"
    # ttbar
    if sample_name == "ttbar":
        top_path = "/lustre/fs24/group/atlas/dbattulga/ntup_SH_Nov2023/MC_bkg/"
        pattern = "user.dabattul.ntup_phys_FS_09112023.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.e6337_s3681_r*/*.root"
    if sample_name == "run2":
        # /lustre/fs22/group/atlas/freder/hh/samples/Data
        top_path = "/lustre/fs24/group/atlas/dbattulga/ntup_SH_Nov2023/Data"
        pattern = "*09112023.data1*/*.root"
        # pattern_17 = "*09112023.data17*/*.root"
        # pattern_18 = "*09112023.data18*/*.root"
    # ps
    if sample_name == "ps":
        top_path = "/lustre/fs24/group/atlas/freder/hh/samples/"
        pattern = "user.frenner.EJ_2024_06_15_T090710.506970.MGH7EG_hh_bbbb_vbf_l1cvv0cv1.e8273_s3681_r*/*.root"

    # just changes top_path
    if merge_processed_hists:
        top_path = selector.configuration.hist_top_path
        pattern += ".h5"
        # pattern_17 += ".h5"
        # pattern_18 += ".h5"
    if merge_processed_dumps:
        top_path = selector.configuration.dump_top_path
        pattern += ".h5"

    if "top_path" not in locals():
        verbose.error(f"{sample_name} is not defined")
        raise NameError(f"{sample_name} is not defined")

    filelist = []

    # sort by descending file size to execute largest first
    for file in sorted(
        glob.iglob(top_path + "/" + pattern), key=os.path.getsize, reverse=True
    ):
        filelist += [file]

    # for file in sorted(
    #     glob.iglob(top_path + "/" + pattern_17), key=os.path.getsize, reverse=True
    # ):
    #     filelist += [file]
    # for file in sorted(
    #     glob.iglob(top_path + "/" + pattern_18), key=os.path.getsize, reverse=True
    # ):
    #     filelist += [file]

    if verbose:
        for f in filelist:
            print(f)

    return filelist


def get_lumi(years: list):
    """
    Get luminosity value per given year in fb-1

    Parameters
    ----------
    years : list
        Years corresponding to desired lumi

    Returns
    -------
    float
        lumi sum of given years
    """
    luminosities = {
        "2015": 3.4454,
        "2016": 33.4022,
        "2017": 44.6306,
        "2018": 58.7916,
        "all": 140.06894,
    }
    lumi = 0
    for yr in years:
        lumi += luminosities[yr]

    return lumi


def write_vars_to_h5(f, values, ds_key):
    """
    writes values from callback that would go to hists

    Parameters
    ----------
    f : h5py.File
        file
    values : np.ndarray
        values to write
    ds_key : string
        dataset name
    """

    ds = f[ds_key]
    # reshape if not empty
    if ds.shape[0] == 0:
        idx_start = 0
    else:
        idx_start = ds.shape[0]
    idx_end = idx_start + values.shape[0]
    ds.resize((idx_end,))
    # write values
    ds[idx_start:idx_end] = values


def get_xbb_flat_mass_efficiency():
    # https://gitlab.cern.ch/dabattul/ntup-histo-analysis-sh4b/-/blob/master/GN2Xv01_FlatMassEff/FlatMass_D_GN2Xv01_02032024.csv
    df = pd.read_csv(
        pathlib.Path(__file__).parent
        / "FlatMassDiscriminant_GN2Xv01_25042024_GPSmoothed.csv"
    )

    # get the bin edges
    df_edges = df.loc[2:, "Jet Mass bin [GeV]"]

    def get_between(s, start, end):
        return s.split(start)[1].split(end)[0]

    edges = []
    for e in df_edges:
        # make in MeV
        edges += [float(get_between(e, ">=", ". &&")) * 1e3]
    # binning ends at 800
    edges = [800.0e3] + edges

    # write out columns
    # flip here, for histogramming
    np.array(df.iloc[2:, 1]).dtype
    xbb_scores = {
        "m_bin_edges": np.flip(np.array(edges)),
        "50": np.flip(np.array(df.iloc[2:, 1], dtype=float)),
        "55": np.flip(np.array(df.iloc[2:, 3], dtype=float)),
        "60": np.flip(np.array(df.iloc[2:, 4], dtype=float)),
    }

    return xbb_scores


def get_xbb_sf(estimate=False):
    """
    make xbb scale factors from file into dict and make absolute up down
    variations

    scale factors from here:
    https://xbb-docs.docs.cern.ch/CalibrationR21/Results/#zbb-calibration-sherpa-2211

    file from:
    https://gitlab.cern.ch/hh4b/hh4b-resolved-reconstruction/-/blob/roosted_newBaseline/Xbb_SF.csv

    Parameters
    ----------
    estimate : +-30% estimate
        just estimate error in each bin to be 30%

    Returns
    -------
    dict
        xbb_sf = {
            "pt_bin_edges": [250000.0, 450000.0, 500000.0, 600000.0, inf],
            50: {
                "nominal": [1.054087, 0.898008, 1.334062, 1.216516],
                "up": [1.394087, 1.246008, 1.725062, 1.550516],
                "down": [0.713087, 0.5740080000000001, 0.992062, 0.872516],
            },
            60: {
                "nominal": [1.172437, 1.141827, 1.335885, 0.956322],
                "up": [1.5244369999999998, 1.540827, 1.736885, 1.263322],
                "down": [0.819437, 0.7738269999999999, 0.979885, 0.657322],
            },
            70: {
                "nominal": [0.814069, 1.171875, 1.11992, 0.557925],
                "up": [1.1910690000000002, 1.612875, 1.5059200000000001, 0.841925],
                "down": [0.43506900000000004, 0.7638750000000001, 0.76792, 0.269925],
            },
        }
    """
    xbb_sf = {
        "pt_bin_edges": [],
        50: {
            "nominal": [],
            "up": [],
            "down": [],
        },
        60: {
            "nominal": [],
            "up": [],
            "down": [],
        },
        70: {
            "nominal": [],
            "up": [],
            "down": [],
        },
    }

    if estimate:
        return {
            "pt_bin_edges": [250000.0, 450000.0, 500000.0, 600000.0, np.inf],
            50: {
                "nominal": [1.0, 1.0, 1.0, 1.0],
                "up": [1.3, 1.3, 1.3, 1.3],
                "down": [0.7, 0.7, 0.7, 0.7],
            },
            55: {
                "nominal": [1.0, 1.0, 1.0, 1.0],
                "up": [1.3, 1.3, 1.3, 1.3],
                "down": [0.7, 0.7, 0.7, 0.7],
            },
            60: {
                "nominal": [1.0, 1.0, 1.0, 1.0],
                "up": [1.3, 1.3, 1.3, 1.3],
                "down": [0.7, 0.7, 0.7, 0.7],
            },
        }

    file = pathlib.Path(__file__).parent / "Xbb_SF.csv"
    with open(file, "r") as infile:
        rd = csv.reader(
            infile, delimiter=" ", skipinitialspace=True, quoting=csv.QUOTE_NONNUMERIC
        )
        for row in rd:
            if row[0] == 50:
                xbb_sf["pt_bin_edges"].append(row[1] * 1e3)  # MeV
                xbb_sf[50]["nominal"].append(row[3])
                xbb_sf[50]["up"].append(row[3] + row[4])
                xbb_sf[50]["down"].append(row[3] - row[5])
                # xbb_sf["50"]["stat"].append(row[6])
            if row[0] == 60:
                xbb_sf[60]["nominal"].append(row[3])
                xbb_sf[60]["up"].append(row[3] + row[4])
                xbb_sf[60]["down"].append(row[3] - row[5])
            if row[0] == 70:
                xbb_sf[70]["nominal"].append(row[3])
                xbb_sf[70]["up"].append(row[3] + row[4])
                xbb_sf[70]["down"].append(row[3] - row[5])
        # append inf to catch also overflow
        xbb_sf["pt_bin_edges"].append(np.inf)

    return xbb_sf


def get_trigger_sf():
    file = pathlib.Path(__file__).parent / "trigger_sfs.json"
    return json.load(open(file))
