import numpy as np
import selector.helpers
import selector.tools

np.set_printoptions(threshold=np.inf)


def run(config, batch):
    """
    object selection sequence returning selected vars

    Parameters
    ----------
    config : configuration.setup
        configuration
    batch : list
        entry_start=batch[0], entry_stop=batch[1]

    Returns
    -------
    dict
        results["histkey"] = [selected_var, weights]
    """
    vars_arr = config.tree.arrays(
        config.load_vars, entry_start=batch[0], entry_stop=batch[1], library="np"
    )
    objects = ObjectSelection(config, vars_arr)
    # config.systematics =
    # {"NOSYS": ["NOSYS"], "small_r_sys": [], "large_r_sys": []}
    for sys_type, sys_list in config.systematics.items():
        # one could even parallelize this but the bottleneck is I/O anyway
        for sys in sys_list:
            objects.setup_vars(sys_type, sys)
            objects.select()
            objects.decorate_results()

    return objects.results


class ObjectSelection:
    def __init__(self, config, vars_arr):
        """
        init event-independent variables like weight_factor, nn

        Parameters
        ----------
        config : object
            basic setup configured in configuration.py
        vars_arr : dict
            holding vars loaded with uproot
        """
        self.config = config
        self.vars_arr = vars_arr

        if config.is_mc:
            lumi = selector.tools.get_lumi(config.meta_data["dataYears"])
            # cross-section comes in nb-1 (* 1e6 = fb-1)
            xsec = config.meta_data["crossSection"] * 1e6
            self.weight_factor = (
                xsec
                * lumi
                * config.meta_data["kFactor"]
                * config.meta_data["genFiltEff"]
                / config.meta_data["sum_of_weights"]
            )
        if any("truth" in x for x in vars_arr):
            self.has_truth = True
        else:
            self.has_truth = False

        # fmt: off
        if self.config.is_mc:
            # mc20
            # r13144
            if "trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100" in vars_arr:
                self.trigger = vars_arr["trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100"]
            # r13145
            if "trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100" in vars_arr:
                self.trigger = vars_arr["trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100"]
            # r13167
            if "trigPassed_HLT_j420_a10_lcw_L1J100" in vars_arr:
                self.trigger = vars_arr["trigPassed_HLT_j420_a10_lcw_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j360_a10_lcw_sub_L1J100"]
        if self.config.is_data:
            yr = config.meta_data["dataYear"]
            # same trigger as reference for 2015 and 2016
            if yr == "2015":
                self.trigger = vars_arr["trigPassed_HLT_j360_a10_lcw_sub_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j360_a10_lcw_sub_L1J100"]
            if yr == "2016":
                self.trigger = vars_arr["trigPassed_HLT_j420_a10_lcw_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j420_a10_lcw_L1J100"]
            if yr == "2017":
                self.trigger = vars_arr["trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100"]
            if yr == "2018":
                self.trigger = vars_arr["trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100"]
                self.triggerRef = vars_arr["trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100"]
        # fmt: on

        # init neural nets
        self.neural_nets = []
        for model in self.config.nn_models:
            # load all folds
            for i in range(self.config.n_subsets):
                self.neural_nets += [
                    selector.helpers.NeuralNet(
                        model + "_k_" + str(i), config.epoch_name
                    )
                ]

        # results to return
        self.results = {}

    def setup_vars(self, sys_type, sys):
        """
        setup vars by systematic type and init arrays

        Parameters
        ----------
        sys_type : str
            systematics of which object
        sys : str
            the systematic name
        """
        self.sys_type = sys_type
        self.sys = sys

        if sys_type == "large_r_sys":
            lrj_sys = sys
            srj_sys = "NOSYS"
        elif sys_type == "small_r_sys":
            lrj_sys = "NOSYS"
            srj_sys = sys
        else:
            lrj_sys = "NOSYS"
            srj_sys = "NOSYS"

        # fmt: off
        # large R jet p4
        self.lrj_pt = self.vars_arr[f"recojet_antikt10UFO_{lrj_sys}_pt"]
        self.lrj_eta = self.vars_arr[f"recojet_antikt10UFO_{lrj_sys}_eta"]
        self.lrj_phi = self.vars_arr[f"recojet_antikt10UFO_{lrj_sys}_phi"]
        self.lrj_m = self.vars_arr[f"recojet_antikt10UFO_{lrj_sys}_m"]
        # small R jet p4
        self.srj_pt = self.vars_arr[f"recojet_antikt4PFlow_{srj_sys}_pt"]
        self.srj_eta = self.vars_arr[f"recojet_antikt4PFlow_{srj_sys}_eta"]
        self.srj_phi = self.vars_arr[f"recojet_antikt4PFlow_{srj_sys}_phi"]
        self.srj_m = self.vars_arr[f"recojet_antikt4PFlow_{srj_sys}_m"]
        # b-tagging
        self.xbb_phbb = self.vars_arr["recojet_antikt10UFO_NOSYS_GN2Xv01_phbb"]
        self.xbb_phcc = self.vars_arr["recojet_antikt10UFO_NOSYS_GN2Xv01_phcc"]
        self.xbb_ptop = self.vars_arr["recojet_antikt10UFO_NOSYS_GN2Xv01_ptop"]
        self.xbb_pqcd = self.vars_arr["recojet_antikt10UFO_NOSYS_GN2Xv01_pqcd"]

        # fmt: on

        self.n_events = len(self.lrj_pt)
        self.event_indices = np.arange(self.n_events)

        # vectors of vectors init
        # if we don't know the size, lists are faster
        self.lrj_xbb_wp = [[] for x in self.event_indices]
        self.lrj_xbb_score = [[] for x in self.event_indices]
        self.vbf_candidates_p4 = [[] for x in self.event_indices]

        self.xbb_sys = False
        if self.config.is_mc:
            if sys_type == "pileup":
                self.pileup_weight = self.vars_arr[f"PileupWeight_{sys}"]
            else:
                self.pileup_weight = self.vars_arr["PileupWeight_NOSYS"]
            if self.sys_type == "xbb_sf":
                self.xbb_sys = True
            if self.sys_type == "generator":
                gen_weight = self.vars_arr["generatorWeight_" + sys]
            else:
                gen_weight = [i[0] for i in self.vars_arr["mcEventWeights"]]

            self.weights = self.weight_factor * self.pileup_weight * gen_weight
        else:
            self.weights = np.full(self.n_events, 1.0, dtype=float)

        # reserve np arrays and write to object
        # int init
        int_init_array = np.full(self.n_events, -1, dtype=int)
        self.n_large_R = np.copy(int_init_array)
        self.sel_lrj_idx_1 = np.copy(int_init_array)
        self.sel_lrj_idx_2 = np.copy(int_init_array)
        self.vbf_idx_1 = np.copy(int_init_array)
        self.vbf_idx_2 = np.copy(int_init_array)

        # numpy arrays are initialized for these event level vars
        bool_vars = [
            "sel_two_lrj",
            "SR_mass_sel",
            "VR_mass_sel",
            "CR_mass_sel",
            "leadingLargeRmassGreater50",
            "leadingLargeRpTGreater500",
            "initial_events",
            "pass_trig_boosted",
            "pass_two_fat_jets",
            "pass_two_hbb_jets",
            "pass_vbf_jets",
            "pass_fat_jet_pt",
            "pass_vbf_cut",
            "pass_SR",
            "NW",
            "SE",
        ]

        float_vars = [
            "pt_h1",
            "eta_h1",
            "phi_h1",
            "m_h1",
            "pt_h2",
            "eta_h2",
            "phi_h2",
            "m_h2",
            "pt_hh",
            "eta_hh",
            "phi_hh",
            "m_hh",
            "m_hh_3",
            "m_hh_4",
            "m_hh_5",
            "m_hh_6",
            "m_hh_7",
            "pt_j1",
            "eta_j1",
            "phi_j1",
            "E_j1",
            "pt_j2",
            "eta_j2",
            "phi_j2",
            "E_j2",
            "m_jj",
            "eta_jj",
            "xbb_score_h1",
            "xbb_score_h2",
            "lead_xbb_score",
            "n_lrj_kin_sel",
            "n_lrj_xbb_pass",
            "pt_hh_scalar",
            "X_HH",
            "CR_hh",
            "truth_m_hh",
            "leadingLargeRpt",
            "leadingLargeRm",
        ]

        float_vars += self.config.nn_models

        # bool init
        bool_init_array = np.zeros(self.n_events, dtype=bool)
        for var in bool_vars:
            setattr(self, var, np.copy(bool_init_array))

        # float init
        floatInitArray = np.full(self.n_events, -1.0, dtype=float)
        for var in float_vars:
            setattr(self, var, np.copy(floatInitArray))

        self.shuffled_indices = np.arange(self.n_events)
        # Set the random seed for reproducibility
        np.random.seed(42)
        # Shuffle the event indices
        np.random.shuffle(self.shuffled_indices)
        self.splitted_indices = np.array_split(
            self.shuffled_indices, self.config.n_subsets
        )
        # this is mainly for speed up
        self.k_fold_idx_lookup = {}
        for i, indices in enumerate(self.splitted_indices):
            for event in indices:
                self.k_fold_idx_lookup[event] = i

        self.nn_fold_lookup = {}
        for nn in self.neural_nets:
            if nn.k_fold not in self.nn_fold_lookup:
                self.nn_fold_lookup[nn.k_fold] = []
            self.nn_fold_lookup[nn.k_fold].append(nn)

        if self.config.run_event_subset:
            selected_splits = [
                self.splitted_indices[i]
                for i in self.config.subset_permutations[self.config.this_subset]
            ]
            self.event_indices = np.sort(np.concatenate(selected_splits))

    def select(self):
        """
        This does the actual analysis/selection steps
        """
        # indexed access is even faster with numpy
        for event in self.event_indices:
            # selector.helpers.trigger_reference(self, event)
            if self.trigger[event]:
                # order matters!
                self.n_large_R[event] = self.lrj_pt[event].shape[0]
                selector.helpers.get_xbb_tags(self, event)
                selector.helpers.large_R_select(self, event)
                # hhp4 and vbf_select are the most expensive ones because of
                # the vector package
                selector.helpers.hh_p4(self, event)
                selector.helpers.vbf_select(self, event)
                selector.helpers.vbf_cut(self, event)
                selector.helpers.nn(self, event)
                selector.helpers.hh_regions(self, event)
                if self.config.is_mc:
                    selector.helpers.apply_xbb_sf(self, event)
            # if self.has_truth:
            #     selector.helpers.truth_mhh(self, event)
        # only after eventloop
        selector.helpers.cut_flow(self)

    def decorate_results(self):
        """
        update the results dict
        """

        self.selections = {
            "SR_xbb_1": self.SR_mass_sel & (self.n_lrj_xbb_pass == 1),
            "VR_xbb_2": self.VR_mass_sel & (self.n_lrj_xbb_pass >= 2),
            "VR_xbb_1": self.VR_mass_sel & (self.n_lrj_xbb_pass == 1),
            "CR_xbb_2": self.CR_mass_sel & (self.n_lrj_xbb_pass >= 2),
            "CR_xbb_1": self.CR_mass_sel & (self.n_lrj_xbb_pass == 1),
            # "xbb_1": self.n_lrj_xbb_pass == 1 & self.pass_vbf_cut,
            # for data
            # "trigger_trigger_ref_m_50": self.trigger
            # & self.triggerRef
            # & self.leadingLargeRmassGreater50,
            # "trigger_ref_m_50": self.triggerRef & self.leadingLargeRmassGreater50,
            # # mc
            # "trigger_m_50": self.trigger & self.leadingLargeRmassGreater50,
            # "all": np.full(self.n_events, True),
            # "trigger_ref_pt_500": self.triggerRef & self.leadingLargeRpTGreater500,
            # "trigger_pt_500": self.trigger & self.triggerRef & self.leadingLargeRpTGreater500,
            # "VR_xbb_1_NW": self.VR_mass_sel & (self.n_lrj_xbb_pass == 1) & self.NW,
            # "VR_xbb_2_NW": self.VR_mass_sel & (self.n_lrj_xbb_pass >= 2) & self.NW,
            # "VR_xbb_1_SE": self.VR_mass_sel & (self.n_lrj_xbb_pass == 1) & ~self.NW,
            # "VR_xbb_2_SE": self.VR_mass_sel & (self.n_lrj_xbb_pass >= 2) & ~self.NW,
        }
        if self.config.BLIND:
            self.selections["SR_xbb_2"] = np.zeros(self.n_events, dtype=bool)
            # also need the last cut before signal region, simply inverting SR_mass_sel
            # reselects events which did not pass preceding cuts
            # self.selections["xbb_2"] = (
            #     self.pass_vbf_cut & (~self.SR_mass_sel) & (self.n_lrj_xbb_pass >= 2)
            # )
        else:
            self.selections["SR_xbb_2"] = self.SR_mass_sel & (self.n_lrj_xbb_pass >= 2)
            # self.selections["xbb_2"] = self.n_lrj_xbb_pass >= 2

        # map variables to hists and selections
        selector.helpers.hist_mapping(self)

        # decorate results dict also with weights
        selector.helpers.decorate(self)
