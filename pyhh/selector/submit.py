import selector.metadata
from selector.tools import construct_file_list
from tools.logging import log

submit_config = """
# Proxy_path = /afs/ifh.de/user/f/freder/x509up_u37827
# cpus = 1
# request_cpus = $(cpus)

executable              = /lustre/fs22/group/atlas/freder/hh/submit/pyhh/pyhh/selector/select.sh
log                     = log_$(Process).log
output                  = outfile_$(Process).txt
error                   = errors_$(Process).txt
# notification = Complete
should_transfer_files   = No
# when_to_transfer_output = ON_EXIT
getenv = True
request_memory = 4096

"""


def run(args):
    filelist = construct_file_list(sample_name=args.sample)

    if "run2" not in args.sample:
        client = selector.metadata.setup_pyami()
        for file in filelist:
            selector.metadata.get(file, client)

    # write jobs per line
    log.info(f"Made submit file for {args.sample} with {len(filelist)} jobs. ")
    with open(
        f"/lustre/fs22/group/atlas/freder/hh/submit/select_{args.sample}.sub", "w"
    ) as f:
        f.write(submit_config)
        for i, file in enumerate(filelist):
            if args.do_systematics:
                f.write(f"arguments = {file} --do_systematics")
            else:
                f.write(f"arguments = {file}")
            f.write("\n")
            f.write("queue")
            f.write("\n")
