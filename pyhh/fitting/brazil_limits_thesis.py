#! /usr/bin/python

import json

import fitting.tools
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MultipleLocator

# Creating a figure and an axis
fig, ax = plt.subplots(figsize=(4, 4))

k2v = 0

if k2v == 0:
    models = [
        "fit/m_hh_5_l1cvv0cv1_250_3",
        "fit/m_hh_5_l1cvv0cv1_neos_first",
        "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_250_3_last_epoch",
        "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1",
    ]
    xsec = fitting.tools.get_k2V_xsec(k2v)
if k2v == 1:
    models = [
        "fit/m_hh_5_l1cvv1cv1_250_3",
        "fit/m_hh_5_l1cvv1cv1_neos_first",
        "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv1cv1_250_3",
        "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv1cv1",
    ]
    xsec = fitting.tools.get_k2V_xsec(k2v)

values = []
labels = []
for m in models:
    with open(
        "/lustre/fs22/group/atlas/freder/hh/run/" + m + "/limits.json",
        "r",
    ) as f:
        lims = json.load(f)
        values.append(lims)
        labels.append(m.replace("_SR_xbb_2_bkg_only", ""))

labels = [r"$m_{HH}$", r"NEOS-$m_{HH}$", "BCE", "NEOS"]
values = np.array(values) * xsec
plt.figure(figsize=(3, 3))
print(values)
sigma = [-2, -1, 0, 1, 2]
mhh = values[0]
neos_mhh = values[1]
bce = values[2]
neos = values[3]
print(neos / bce)

plt.scatter(sigma, mhh / neos)
plt.scatter(sigma, neos_mhh / neos)
plt.scatter(sigma, bce / neos)

pdf_path = f"/lustre/fs22/group/atlas/freder/hh/run/limits/relative_limits_k2v{k2v}.pdf"
print(pdf_path)
plt.ylabel("neos Improvement")
plt.xlabel(r"Expected Limits ($\sigma$-Units)")
# plt.ylim(0, 1)
plt.grid()
plt.legend([r"$m_{HH}$/NEOS", "BCE/NEOS"])  # , loc="center left")
# plt.legend([r"NEOS-$m_{HH}$/NEOS", "BCE/NEOS"])  # , loc="center left")
plt.legend(
    [r"$m_{HH}$/NEOS", r"NEOS-$m_{HH}$/NEOS", "BCE/NEOS"],
    loc="upper left",
    bbox_to_anchor=(0.5, 0.5),
)
plt.tight_layout()
plt.savefig(pdf_path)
plt.close()
# Plot and fill between the lines
for i, y in enumerate(values):
    x = np.linspace(i, i + 1, 10)
    ax.fill_between(x, y[0], y[1], color="#fdc536")
    ax.fill_between(x, y[1], y[2], color="#36b1bf")
    ax.hlines(y[2], x[0], x[-1], color="black")
    ax.fill_between(x, y[2], y[3], color="#36b1bf")
    ax.fill_between(x, y[3], y[4], color="#fdc536")


ax.legend(
    [
        r"Expected Limit $\pm 2\sigma$",
        r"Expected Limit $\pm 1\sigma$",
        r"Expected Limit",
    ],
    fontsize=9,
    loc="upper right",
)
if k2v == 0:
    plt.ylabel(r"95% CL limit on $\sigma_{\kappa_{2v}=0}$ [fb]")
else:
    plt.ylabel(r"95% CL limit on $\sigma_{\kappa_{2v}=1}$ [fb]")
plt.xticks(
    np.arange(0.5, len(labels), step=1), labels
)  # Setting x-axis ticks in integer steps from 0 to 10

# plt.ylim([0, 70])
# plt.ylim([0, 50])
# ax.yaxis.set_minor_locator(MultipleLocator(5))
plt.yticks(minor=True)
plt.grid(alpha=0.35, which="both", color="grey")
plt.tight_layout()
pdf_path = f"/lustre/fs22/group/atlas/freder/hh/run/limits/brazil_limits_k2v{k2v}.pdf"
print(pdf_path)
plt.savefig(pdf_path)
