declare -a vars=(
    # "m_hh_5"
    # "tomatos_bce_5_10000_lr_0p0005"
    "tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000"
)

# copy and run from submit folder
rm -rf /lustre/fs22/group/atlas/freder/hh/submit/pyhh/
rsync -r --exclude=.git /lustre/fs22/group/atlas/freder/hh/pyhh /lustre/fs22/group/atlas/freder/hh/submit/
# also update the cabinetry installation used in the environment
rsync -r /lustre/fs22/group/atlas/freder/pip/lib/python3.9/site-packages/cabinetry /lustre/fs22/group/atlas/freder/hh/pyhh_env/lib/python3.9/site-packages/

# now loop through the above array
for i in "${vars[@]}"; do
    echo $i
    # pyhh fit --variable $i --combine
    python3 /lustre/fs22/group/atlas/freder/hh/pyhh/pyhh/fitting/make_scan_submit_file.py --variable $i
    mkdir /lustre/fs22/group/atlas/freder/hh/submit/fit/$i -p
    cd /lustre/fs22/group/atlas/freder/hh/submit/fit/$i
    condor_submit /lustre/fs22/group/atlas/freder/hh/submit/fit/scan_$i.sub
done
