#!/usr/bin/env python3
import argparse
import os

import numpy as np

settings = """ 
executable              = /lustre/fs22/group/atlas/freder/hh/pyhh/pyhh/fitting/fit.sh

log                     = log_$(Process).log
output                  = outfile_$(Process).txt
error                   = errors_$(Process).txt
# notification = Complete 
should_transfer_files   = No
# when_to_transfer_output = ON_EXIT
getenv = True
request_memory = 4096
"""

parser = argparse.ArgumentParser()
parser.add_argument("--variable", type=str, required=True)
args = parser.parse_args()
submitfolder = "/lustre/fs22/group/atlas/freder/hh/submit/fit"

if not os.path.isdir(submitfolder):
    os.makedirs(submitfolder)


with open(
    f"/lustre/fs22/group/atlas/freder/hh/submit/fit/scan_{args.variable}.sub", "w"
) as f:
    f.write(settings)
    f.write("\n")

    kappa_2v = np.round(np.arange(-0.1, 2.2, 0.01), 2)

    for k2v in kappa_2v:
        f.write(f"arguments = {args.variable} k2v_{k2v}")
        f.write("\n")
        f.write("queue")
        f.write("\n")
