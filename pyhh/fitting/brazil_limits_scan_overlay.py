#! /usr/bin/python

import json

import fitting.tools
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MultipleLocator

plt.rc("font", size=12)
plt.rc("legend", fontsize=10)
# Creating a figure and an axis
fig, ax = plt.subplots(figsize=(8, 6))


kappa_2v = np.round(np.arange(-0.1, 2.2, 0.01), 2)

k2v = np.array(kappa_2v)
sm = fitting.tools.get_k2V_xsec(k2v)
ax.plot(k2v, sm, color="black", linestyle="-", label="Theory Prediction")
ax.scatter(
    1,
    fitting.tools.get_k2V_xsec(1),
    s=200,
    marker="*",
    label="SM Prediction",
    edgecolor="k",
    color="#ebf5ee",
    zorder=5,
)

# folder = "/lustre/fs22/group/atlas/freder/hh/run/fit_neos_validation/"
folder = "/lustre/fs22/group/atlas/freder/hh/run/fit/"


variables = [
    "m_hh_5_250_3_scan/m_hh_5",
    "m_hh_5_neos_first_scan/m_hh_5",
    "tomatos_bce_5_10000_lr_0p0005_scan/tomatos_bce_5_10000_lr_0p0005",
    "tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000",
]

for var, color in zip(variables, matplotlib.colors.TABLEAU_COLORS):
    models = [f"{var}_k2v_{k2v}" for k2v in kappa_2v]

    values = []
    k2v = []
    for m in models:
        k2v.append(float(m.split("k2v_")[-1]))
        try:
            with open(folder + m + "/limits.json", "r") as f:
                lims = json.load(f)
                values.append(lims)
        except:
            print("no limits found for: ", m)
            values.append([0, 0, 0, 0, 0])

    k2v = np.array(k2v)
    xsec = fitting.tools.get_k2V_xsec(k2v)
    xsec_scale = np.full((5, len(xsec)), xsec).T

    # # do this before scaling
    # if var == "tomatos_cls_5_400_unbound_shapesys":
    #     with open(
    #         "/lustre/fs22/group/atlas/freder/hh/pyhh/pyhh/fitting/cls_limits.json", "w"
    #     ) as f:
    #         json.dump({"limits": values, "k2v": k2v.tolist()}, f)

    values = np.array(values) * xsec_scale

    # Plot and fill between the lines
    if "m_hh" in var:
        label = "$m_{HH}$"
    if "neos_first" in var:
        label = "NEOS-$m_{HH}$"
    if "bce" in var:
        label = "BCE"
    if "cls" in var:
        label = "neos"

    ax.plot(
        k2v,
        values[:, 2],
        color=color,
        label=f"{label} Expected Limit",
        linewidth=3,
    )

    # find the crossing with the sm
    print(
        f"constrained k2v for {var}: ",
        k2v[np.argwhere(np.diff(np.sign(sm - values[:, 2]))).flatten()],
    )

    std_linewidth = 0.8
    ax.plot(
        k2v,
        values[:, 3],
        color=color,
        # linestyle="dashed",
        linewidth=std_linewidth,
        label=r"Expected Limit $\pm 1\sigma$",
    )
    ax.plot(
        k2v,
        values[:, 4],
        color=color,
        linestyle="dotted",
        linewidth=std_linewidth,
    )
    ax.plot(
        k2v,
        values[:, 0],
        color=color,
        linestyle="dotted",
        linewidth=std_linewidth,
        label=r"Expected Limit $\pm 2\sigma$",
    )
    ax.plot(
        k2v,
        values[:, 1],
        color=color,
        # linestyle="dashed",
        linewidth=std_linewidth,
    )
    ax.legend(
        # [
        #     r"Expected Limit",
        #     r"Expected Limit $\pm 2\sigma$",
        #     r"Expected Limit $\pm 1\sigma$",
        # ],
        fontsize="large",
        loc="upper right",
    )
    plt.ylabel(r"95 % CL limit on $\sigma_\mathrm{VBF}}$ [fb]")
    plt.xlabel(r"$\kappa_\mathrm{2V}$ $(\kappa_\lambda=1 ,\kappa_\mathrm{V}=1 )$")
    ax.set_yscale("log")
    plt.ylim([0.9, 3e3])
    plt.xlim([-0.1, 2.2])
    ax.xaxis.set_minor_locator(MultipleLocator(0.1))
    # plt.xticks(minor=True)
    plt.grid(alpha=0.35, which="both", color="grey")
    plt.legend()
    plt.tight_layout()

pdf_path = f"/lustre/fs22/group/atlas/freder/hh/run/limits/k2v_scan_limits_overlay.pdf"
print(pdf_path)
plt.savefig(pdf_path)
