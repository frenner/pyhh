#! /usr/bin/python

import json

import fitting.tools
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MultipleLocator

kappa_2v = np.round(np.arange(-0.1, 2.2, 0.01), 2)

folder = "/lustre/fs22/group/atlas/freder/hh/run/fit_no_vbf_cut/"
# folder = "/lustre/fs22/group/atlas/freder/hh/run/fit_vbf_cut/"
folder = "/lustre/fs22/group/atlas/freder/hh/run/fit/"

variables = [
    "m_hh_5",
    "tomatos_bce_5",
    "tomatos_cls_5",
]

for var in variables:
    fig, ax = plt.subplots(figsize=(5, 4))

    models = [f"{var}_k2v_{k2v}" for k2v in kappa_2v]

    values = []
    k2v = []
    for m in models:
        k2v.append(float(m.split("k2v_")[-1]))
        try:
            with open(folder + m + "/limits.json", "r") as f:
                lims = json.load(f)
                values.append(lims)
        except:
            print("no limits found for: ", m)
            values.append([0, 0, 0, 0, 0])

    k2v = np.array(k2v)
    sm = fitting.tools.get_k2V_xsec(k2v)

    sm_scale = np.full((5, len(sm)), sm).T
    values = np.array(values) * sm_scale
    # Plot and fill between the lines
    ax.plot(
        k2v,
        values[:, 2],
        color="black",
        label="Expected Limit",
    )

    # find the crossing with the sm
    print(
        f"constrained k2v for {var}: ",
        k2v[np.argwhere(np.diff(np.sign(sm - values[:, 2]))).flatten()],
    )

    ax.fill_between(
        k2v,
        values[:, 2],
        values[:, 3],
        color="#36b1bf",
        label=r"Expected Limit $\pm 1\sigma$",
    )
    ax.fill_between(
        k2v,
        values[:, 3],
        values[:, 4],
        color="#fdc536",
    )
    ax.fill_between(
        k2v,
        values[:, 0],
        values[:, 1],
        color="#fdc536",
        label=r"Expected Limit $\pm 2\sigma$",
    )
    ax.fill_between(
        k2v,
        values[:, 1],
        values[:, 2],
        color="#36b1bf",
    )
    ax.plot(k2v, sm, color="red", linestyle="-", label="Theory Prediction")

    ax.scatter(
        1,
        fitting.tools.get_k2V_xsec(1),
        s=200,
        marker="*",
        label="SM Prediction",
        edgecolor="k",
        color="#ebf5ee",
        zorder=5,
    )

    ax.legend(
        # [
        #     r"Expected Limit",
        #     r"Expected Limit $\pm 2\sigma$",
        #     r"Expected Limit $\pm 1\sigma$",
        # ],
        fontsize="large",
        loc="upper right",
    )
    plt.ylabel(r"95 % CL limit on $\sigma}$ [fb]")
    plt.xlabel(r"$\kappa_\mathrm{2V} (\kappa_\lambda=1.0 ,\kappa_\mathrm{V}=1.0 )$")
    ax.set_yscale("log")
    plt.ylim([0.5, 1e4])
    ax.xaxis.set_minor_locator(MultipleLocator(0.1))
    # plt.xticks(minor=True)
    plt.grid(alpha=0.35, which="both", color="grey")
    plt.legend(fontsize="9")
    plt.tight_layout()
    pdf_path = (
        f"/lustre/fs22/group/atlas/freder/hh/run/limits/k2v_scan_limits_{var}.pdf"
    )
    print(pdf_path)
    plt.savefig(pdf_path)
