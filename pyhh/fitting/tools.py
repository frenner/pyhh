import json
import os

import h5py
import matplotlib.pyplot as plt
import numpy as np
import uproot
import yaml
from sympy import Eq, solve, symbols

files = {}
# fmt: off
files["run2"] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-run2.h5"
files["bkg_estimate"] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-run2.h5"
files["ggf"] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-cHHH01d0.h5"
files["ps"] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-ps.h5"

# fmt: on


def add_hist(out_file, path, hist, edges):
    # Uproot automatically converts histograms
    out_file[path] = (hist, edges)


def transform_hists_for_cabinetry(fit_variable, hypothesis):
    """
    make root file compatible with cabinetry

    Parameters
    ----------
    fit_variable : str
    hypothesis: str

    """
    files["signal"] = (
        f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-{hypothesis}.h5"
    )

    # abcd weight
    w_CR_xbb_2, w_CR_xbb_2_err = get_bkg_weight(fit_variable, subtract_ttbar=False)

    out_file_path = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{fit_variable}_{hypothesis}.root"

    with uproot.recreate(out_file_path) as out_file:
        for sample, file_path in files.items():
            with h5py.File(file_path, "r") as file:
                wrong_hists = get_unwanted_hists(file, fit_variable)
                for histname in file.keys():
                    if (
                        "massplane" in histname
                        or f"{fit_variable}" not in histname
                        or "GEN_MUR" in histname
                        or any([True for v in wrong_hists if v in histname])
                    ):
                        continue
                    # need to make this compatible with cabinetry histogram config
                    # https://github.com/cabinetry/cabinetry-tutorials/blob/master/config_histograms.yml
                    var = histname.split(".")[0]
                    region = histname.split(".")[1]
                    path = region + "/" + sample + "/" + var
                    h = file[histname]["histogram"][1:-1]
                    edges = file[histname]["edges"][:]
                    if sample == "bkg_estimate":
                        if "SR_xbb_2" in histname:
                            continue
                        if "SR_xbb_1" in histname:
                            # bkg estimate
                            h = h * w_CR_xbb_2
                            region_sample = "SR_xbb_2" + "/" + sample + "/"
                            out_file[region_sample + var] = (h, edges)

                            # manually add stat uncertainty per bin
                            stat_err = np.sqrt(file[histname]["w2sum"][1:-1])
                            stat_up, stat_down = symmetrize_up_down(
                                nom=h, var=h + (stat_err * w_CR_xbb_2)
                            )
                            out_file[region_sample + f"stat_bkg_estimate_up"] = (
                                stat_up,
                                edges,
                            )
                            out_file[region_sample + f"stat_bkg_estimate_down"] = (
                                stat_down,
                                edges,
                            )

                            # stat error per bin
                            for i, _ in enumerate(h):
                                hist_up = np.copy(h)
                                hist_down = np.copy(h)
                                hist_up[i] = stat_up[i]
                                hist_down[i] = stat_down[i]
                                out_file[
                                    region_sample + f"stat_bkg_estimate_up_bin_{i}"
                                ] = (hist_up, edges)
                                out_file[
                                    region_sample + f"stat_bkg_estimate_down_bin_{i}"
                                ] = (hist_down, edges)

                            # this is for binned w_CR
                            norm_up, norm_down = symmetrize_up_down(
                                nom=h,
                                var=h + (h * w_CR_xbb_2_err),
                            )
                            out_file[region_sample + "norm_bkg_estimate_up"] = (
                                norm_up,
                                edges,
                            )
                            out_file[region_sample + "norm_bkg_estimate_down"] = (
                                norm_down,
                                edges,
                            )

                            # add shape systematic for bkg estimate

                            up_factor, down_factor = get_shape_sys_factor(
                                fit_variable,
                                w_CR_xbb_2,
                                w_CR_xbb_2_err,
                                subtract_ttbar=False,
                            )
                            up_path = region_sample + "bkg_estimate_shape_up"  # noqa
                            down_path = region_sample + "bkg_estimate_shape_down"
                            out_file[up_path] = (h * up_factor, edges)
                            out_file[down_path] = (h * down_factor, edges)

                    else:
                        out_file[path] = (h, edges)

                if sample == "signal":
                    # scale variations
                    (
                        scale_up,
                        scale_down,
                        scale_acc_rel_up,
                        scale_acc_rel_down,
                        scale_edges,
                    ) = get_scale_variation_envelope(fit_variable, file)
                    up_path = "SR_xbb_2/" + sample + "/" + "scale_up"  # noqa
                    down_path = "SR_xbb_2/" + sample + "/" + "scale_down"
                    out_file[up_path] = (scale_up, edges)
                    out_file[down_path] = (scale_down, edges)

                    # pdf+alpha_s uncertainty
                    (
                        pdf_alpha_up,
                        pdf_alpha_down,
                        pdf_acc_rel_up,
                        pdf_acc_rel_down,
                        pdf_alpha_edges,
                    ) = get_pdf_alpha_s_uncertainty(fit_variable, file)
                    up_path = "SR_xbb_2/" + sample + "/" + "pdf_alpha_up"  # noqa
                    down_path = "SR_xbb_2/" + sample + "/" + "pdf_alpha_down"
                    out_file[up_path] = (pdf_alpha_up, edges)
                    out_file[down_path] = (pdf_alpha_down, edges)

                    # get stat error
                    nom = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]
                    edges = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["edges"][:]
                    stat_err = np.sqrt(
                        file[f"{fit_variable}_NOSYS.SR_xbb_2"]["w2sum"][1:-1]
                    )

                    # manually add stat uncertainty
                    stat_up, stat_down = symmetrize_up_down(
                        nom=nom, var=(nom + stat_err)
                    )
                    # this is just for plotting
                    out_file["SR_xbb_2/" + sample + "/" + f"stat_signal_up"] = (
                        stat_up,
                        edges,
                    )
                    out_file["SR_xbb_2/" + sample + "/" + f"stat_signal_down"] = (
                        stat_down,
                        edges,
                    )

                    for i, h_ith_bin in enumerate(nom):
                        hist_up = np.copy(nom)
                        hist_down = np.copy(nom)
                        hist_up[i] = stat_up[i]
                        hist_down[i] = stat_down[i]

                        out_file[
                            "SR_xbb_2/" + sample + "/" + f"stat_signal_up_bin_{i}"
                        ] = (hist_up, edges)
                        out_file[
                            "SR_xbb_2/" + sample + "/" + f"stat_signal_down_bin_{i}"
                        ] = (hist_down, edges)

                if sample == "ggf":
                    h = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]
                    out_file["SR_xbb_2/" + sample + "/" + f"{fit_variable}_NOSYS"] = (
                        h,
                        edges,
                    )

                if sample == "ps":
                    # get k2v0 histogram
                    with h5py.File(
                        "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-l1cvv0cv1.h5",
                        "r",
                    ) as sig:
                        h_k2v0 = sig[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][
                            1:-1
                        ]

                    # get ps histogram
                    h_ps = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]
                    edges = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["edges"][:]
                    ps_k2v0_up, ps_k2v0_down = symmetrize_up_down(
                        nom=h_k2v0, var=(h_ps)
                    )
                    # find diff between ps and k2v0 as scale factor
                    ps_up_factor = ps_k2v0_up / h_k2v0
                    ps_down_factor = ps_k2v0_down / h_k2v0

                    # apply to nominal signal
                    with h5py.File(files["signal"], "r") as sig:
                        h_nominal_signal = sig[f"{fit_variable}_NOSYS.SR_xbb_2"][
                            "histogram"
                        ][1:-1]
                    ps_up = ps_up_factor * h_nominal_signal
                    ps_down = ps_down_factor * h_nominal_signal
                    out_file["SR_xbb_2/signal/ps_signal_up"] = (ps_up, edges)
                    out_file["SR_xbb_2/signal/ps_signal_down"] = (ps_down, edges)

    print("wrote hists to " + out_file_path)


def get_unwanted_hists(file, fit_variable):
    wrong_hists = []
    for histname in file.keys():
        # remove non fit variables which are picked up because of a different
        # suffix
        if f"{fit_variable}" in histname and "NOSYS" in histname:
            potential_hist = histname.split("_NOSYS")[0]
            if potential_hist != fit_variable:
                wrong_hists += [potential_hist]
    return wrong_hists


def build_config(fit_variable, outfile, region, systs, hypothesis, subsys=False):
    files["signal"] = (
        f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-l1cvv0cv1.h5"
    )
    hist_folder = os.path.dirname(outfile) + "/histograms"

    # extract theory normalization error from k2v=0
    w_CR_xbb_2, w_CR_xbb_2_err = get_bkg_weight(fit_variable, subtract_ttbar=False)
    err_rel_w_CR = w_CR_xbb_2_err / w_CR_xbb_2

    if "m_hh" in fit_variable:
        fit_name = "m_HH"
    if "bce" in fit_variable:
        fit_name = "BCE"
    if "cls" in fit_variable:
        fit_name = "neos"

    bins = get_binning(fit_variable)
    stat_errors = []

    for i in range(len(bins) - 1):
        stat_errors.append(
            {
                "Name": f"Bkg Stat Error bin {i+1}",
                "Up": {"VariationPath": f"stat_bkg_estimate_up_bin_{i}"},
                "Down": {"VariationPath": f"stat_bkg_estimate_down_bin_{i}"},
                "Samples": "Background",
                "Type": "NormPlusShape",
            }
        )
        stat_errors.append(
            {
                "Name": f"Signal Stat Error bin {i+1}",
                "Up": {"VariationPath": f"stat_signal_up_bin_{i}"},
                "Down": {"VariationPath": f"stat_signal_down_bin_{i}"},
                "Samples": "VBF Signal",
                "Type": "NormPlusShape",
            },
        )
    # # uncomment just for plotting for the thesis in one plot
    # stat_errors.append(
    #         {
    #             "Name": f"Bkg Stat Error",
    #             "Up": {"VariationPath": "stat_bkg_estimate_up"},
    #             "Down": {"VariationPath": "stat_bkg_estimate_down"},
    #             "Samples": "Background",
    #             "Type": "NormPlusShape",
    #         }
    #     )
    # stat_errors.append(
    #         {
    #             "Name": f"Signal Stat Error",
    #             "Up": {"VariationPath": "stat_signal_up"},
    #             "Down": {"VariationPath": "stat_signal_down"},
    #             "Samples": "VBF Signal",
    #             "Type": "NormPlusShape",
    #         },
    #     )

    config = {
        "General": {
            "Measurement": "HH4b VBF boosted",
            "POI": "signal_norm",
            "HistogramFolder": hist_folder,
            "InputPath": f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{fit_variable}_{hypothesis}.root:{{RegionPath}}/{{SamplePath}}/{{VariationPath}}",
            "VariationPath": f"{fit_variable}_NOSYS",
        },
        "Regions": [
            {
                "Name": fit_name,
                "RegionPath": region,
                "Binning": bins,
            }
        ],
        "Samples": [
            {
                "Name": "Data",
                "SamplePath": "run2",
                "Data": True,
            },
            {
                "Name": "VBF Signal",
                "SamplePath": "signal",
                "DisableStaterror": True,
            },
            {
                "Name": "ggF Signal",
                "SamplePath": "ggf",
                "DisableStaterror": True,
            },
            {
                "Name": "Background",
                "SamplePath": "bkg_estimate",
                "DisableStaterror": True,
            },
        ],
        "NormFactors": [
            {
                "Name": "signal_norm",
                "Samples": "VBF Signal",
                "Nominal": 1,
                "Bounds": get_mu_range(fit_variable, hypothesis),
            }
        ],
        "Systematics": [
            # norm errors
            {
                "Name": "Luminosity",
                "Up": {"Normalization": 0.0083},
                "Down": {"Normalization": -0.0083},
                "Type": "Normalization",
            },
            {
                "Name": "Bkg Estimate Norm",
                "Up": {"Normalization": float(err_rel_w_CR)},
                "Down": {"Normalization": float(-err_rel_w_CR)},
                "Samples": "Background",
                "Type": "Normalization",
            },
            {
                "Name": "Branching Ratio bb",
                "Up": {"Normalization": 0.034230167215544958},
                "Down": {"Normalization": -0.03479541236132045},
                "Samples": "VBF Signal",
                "Type": "Normalization",
            },
            # shape errors
            {
                "Name": "Scale Variations",
                "Up": {"VariationPath": "scale_up"},
                "Down": {"VariationPath": "scale_down"},
                "Samples": "VBF Signal",
                "Type": "NormPlusShape",
            },
            {
                "Name": "pdf + alpha s",
                "Up": {"VariationPath": "pdf_alpha_up"},
                "Down": {"VariationPath": "pdf_alpha_down"},
                "Samples": "VBF Signal",
                "Type": "NormPlusShape",
            },
            # # for binned w_CR
            # {
            #     "Name": "Bkg Estimate Norm",
            #     "Up": {"VariationPath": "norm_bkg_estimate_up"},
            #     "Down": {"VariationPath": "norm_bkg_estimate_down"},
            #     "Samples": "Background",
            #     "Type": "NormPlusShape",
            # },
            {
                "Name": "Bkg Estimate Shape",
                "Up": {"VariationPath": "bkg_estimate_shape_up"},
                "Down": {"VariationPath": "bkg_estimate_shape_down"},
                "Samples": "Background",
                "Type": "NormPlusShape",
            },
            {
                "Name": "Parton Shower",
                "Up": {"VariationPath": "ps_signal_up"},
                "Down": {"VariationPath": "ps_signal_down"},
                "Samples": "VBF Signal",
                "Type": "NormPlusShape",
            },
            # add stat errors
            *stat_errors,
        ],
    }

    # add systematics
    systematics = []
    if systs:
        # figure out systematics from prepared file
        f_path = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{fit_variable}_{hypothesis}.root"
        with uproot.open(f_path) as f:
            nominal_hist = f[f"SR_xbb_2/signal/{fit_variable}_NOSYS"].to_numpy()[0]
            for name, classname in f.classnames(cycle=False).items():
                if "TH1" in classname:
                    # get the systs
                    if (
                        f"{fit_variable}" in name
                        and ("__1up" in name or "__1down" in name)
                        and "SR_xbb_2" in name
                    ):
                        hist = f[name].to_numpy()[0]
                        # pruning if per bin deviation is not at least 0.5%
                        relative_diff = np.abs((nominal_hist - hist) / nominal_hist)
                        if any(relative_diff > 0.005):
                            name = name.split("/")[-1]
                            systematics += [name.split("__")[0]]

            # remove duplicates by going to dict and back
            systematics = list(dict.fromkeys(systematics))
        for sys in systematics:
            config["Systematics"].append(
                {
                    "Name": sys.replace(fit_variable + "_", "").replace("_", " "),
                    "Up": {"VariationPath": sys + "__1up"},
                    "Down": {"VariationPath": sys + "__1down"},
                    "Samples": "VBF Signal",
                    "Type": "NormPlusShape",
                },
            )
    if subsys:
        subsysts = [
            "Bkg Estimate Norm",
            "Branching Ratio bb",
            "Scale Variations",
            "Bkg Estimate Shape",
            "Parton Shower",
            "xbb pt bin 0",
            "xbb pt bin 1",
            "xbb pt bin 2",
            "xbb pt bin 3",
        ]

        config["Systematics"] = [
            syst for syst in config["Systematics"] if syst["Name"] in subsysts
        ]
    with open(outfile, "w") as f_out:
        yaml.dump(config, f_out, sort_keys=False)


def get_mu_range(fit_variable, hypothesis):
    # figure signal range from k2v value
    if "k2v" in hypothesis:
        k2v = float(hypothesis.split("k2v_")[-1])
    elif "l1cvv0cv1" in hypothesis:
        k2v = 0.0
    elif "l1cvv1cv1" in hypothesis:
        k2v = 1.0
    elif "l1cvv1p5cv1" in hypothesis:
        k2v = 1.5
    with open(
        "/lustre/fs22/group/atlas/freder/hh/pyhh/pyhh/fitting/cls_limits.json", "r"
    ) as f:
        cls_limits = json.load(f)
        k2v_idx = np.where(np.array(cls_limits["k2v"]) == k2v)[0][0]
        upper_limit = cls_limits["limits"][k2v_idx][4]
        if "cls" in fit_variable:
            upper_limit *= 5
        if "bce" in fit_variable:
            upper_limit *= 10
        if "m_hh" in fit_variable:
            upper_limit *= 10
    mu_range = [0.0, upper_limit]
    return mu_range


def get_bkg_weight(fit_variable, subtract_ttbar=False):
    high_tag = "xbb_2"
    low_tag = "xbb_1"

    with h5py.File(files["run2"], "r") as run2:
        CR_4b_Data = np.sum(run2[f"{fit_variable}_NOSYS.CR_{high_tag}"]["histogram"][:])
        CR_2b_Data = np.sum(run2[f"{fit_variable}_NOSYS.CR_{low_tag}"]["histogram"][:])

        # CR_4b_Data = run2[f"{fit_variable}_NOSYS.CR_{high_tag}"]["histogram"][1:-1]
        # CR_2b_Data = run2[f"{fit_variable}_NOSYS.CR_{low_tag}"]["histogram"][1:-1]
        if subtract_ttbar:
            with h5py.File(files["ttbar"], "r") as ttbar:
                CR_4b_ttbar = np.sum(
                    ttbar[f"{fit_variable}_NOSYS.CR_{high_tag}"]["histogram"][:]
                )
                CR_2b_ttbar = np.sum(
                    ttbar[f"{fit_variable}_NOSYS.CR_{low_tag}"]["histogram"][:]
                )
        else:
            CR_4b_ttbar = 0
            CR_2b_ttbar = 0

    # make multijet only
    CR1 = CR_4b_Data - CR_4b_ttbar
    CR2 = CR_2b_Data - CR_2b_ttbar
    errCR1 = np.sqrt(CR_4b_Data) + np.sqrt(CR_4b_ttbar)
    errCR2 = np.sqrt(CR_2b_Data) + np.sqrt(CR_2b_ttbar)
    w_CR = CR1 / CR2
    err_w_CR = w_CR * np.sqrt(np.square(errCR1 / CR1) + np.square(errCR2 / CR2))

    print(w_CR, err_w_CR)
    return w_CR, err_w_CR


def get_shape_sys_factor(fit_variable, w_CR, err_w_CR, subtract_ttbar=False):
    """
    finds the up and down relative error factors from the background
    estimate

    Parameters
    ----------
    w_CR : float
        weight
    err_w_CR : float
        error weight

    Returns
    -------
    Tuple
        (up, down)
    """

    high_tag = "xbb_2"
    low_tag = "xbb_1"

    # high_tag = "xbb_2_SE"
    # low_tag = "xbb_1_SE"

    with h5py.File(files["run2"], "r") as run2:
        VR_4b_Data_err = np.sqrt(
            run2[f"{fit_variable}_NOSYS.VR_{high_tag}"]["w2sum"][1:-1]
        )
        VR_2b_Data_err = np.sqrt(
            run2[f"{fit_variable}_NOSYS.VR_{low_tag}"]["w2sum"][1:-1]
        )
        VR_4b_Data = run2[f"{fit_variable}_NOSYS.VR_{high_tag}"]["histogram"][1:-1]
        VR_2b_Data = run2[f"{fit_variable}_NOSYS.VR_{low_tag}"]["histogram"][1:-1]
        if subtract_ttbar:
            with h5py.File(files["ttbar"], "r") as ttbar:
                VR_4b_ttbar_err = np.sqrt(
                    ttbar[f"{fit_variable}_NOSYS.VR_{high_tag}"]["w2sum"][1:-1]
                )
                VR_2b_ttbar_err = np.sqrt(
                    ttbar[f"{fit_variable}_NOSYS.VR_{low_tag}"]["w2sum"][1:-1]
                )
                VR_4b_ttbar = ttbar[f"{fit_variable}_NOSYS.VR_{high_tag}"]["histogram"][
                    1:-1
                ]
                VR_2b_ttbar = ttbar[f"{fit_variable}_NOSYS.VR_{low_tag}"]["histogram"][
                    1:-1
                ]
        else:
            VR_4b_ttbar_err = np.zeros(VR_4b_Data_err.shape)
            VR_2b_ttbar_err = np.zeros(VR_2b_Data_err.shape)
            VR_4b_ttbar = np.zeros(VR_4b_Data.shape)
            VR_2b_ttbar = np.zeros(VR_2b_Data.shape)
    # # make multijet only

    VR_4b = VR_4b_Data - VR_4b_ttbar
    VR_2b = VR_2b_Data - VR_2b_ttbar
    bkg_estimate = VR_2b * w_CR
    # VR_4b[-1]-=1
    print(VR_4b)
    print(bkg_estimate)
    diff = np.abs(bkg_estimate - VR_4b)

    up = bkg_estimate + diff
    down = bkg_estimate - diff

    up_factor = up / bkg_estimate
    down_factor = down / bkg_estimate
    down_factor[down_factor < 0] = 0

    print(up_factor, down_factor)

    return up_factor, down_factor


def get_scale_variation_envelope(fit_variable, file):
    diffs = []
    nominal = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]
    edges = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["edges"][:]
    wrong_hists = get_unwanted_hists(file, fit_variable)
    for histname in file.keys():
        # remove non fit variables which are picked up because of a different
        # suffix
        if (
            "PDF260000" in histname
            and "SR_xbb_2" in histname
            and f"{fit_variable}" in histname
            and not any([True for v in wrong_hists if v in histname])
        ):
            diff = np.abs(file[histname]["histogram"][1:-1] - nominal)
            diffs += [diff]
    diffs = np.array(diffs)
    max_diffs = np.max(diffs, axis=0)
    envelope_up = nominal + max_diffs
    envelope_down = nominal - max_diffs
    acc_rel_up = 1 - (np.sum(nominal) / np.sum(envelope_up))
    acc_rel_down = 1 - (np.sum(nominal) / np.sum(envelope_down))
    return envelope_up, envelope_down, acc_rel_up, acc_rel_down, edges


def get_pdf_alpha_s_uncertainty(fit_variable, file):
    # equation 20 from https://arxiv.org/pdf/1510.03865.pdf
    nominal_pdf = file[f"{fit_variable}_GEN_MUR10_MUF10_PDF90400.SR_xbb_2"][
        "histogram"
    ][1:-1]
    i = 0
    delta_pdf = np.zeros_like(nominal_pdf)
    edges = file[f"{fit_variable}_GEN_MUR10_MUF10_PDF90400.SR_xbb_2"]["edges"][:]
    wrong_hists = get_unwanted_hists(file, fit_variable)

    for histname in file.keys():
        if (
            "PDF904" in histname
            and not "PDF90400" in histname
            and "SR_xbb_2" in histname
            and f"{fit_variable}" in histname
            and not any([True for v in wrong_hists if v in histname])
        ):
            i += 1
            delta_pdf += (file[histname]["histogram"][1:-1] - nominal_pdf) ** 2

    delta_pdf = np.sqrt(delta_pdf)

    alpha_s_up = file[f"{fit_variable}_GEN_MUR10_MUF10_PDF266000.SR_xbb_2"][
        "histogram"
    ][1:-1]
    alpha_s_down = file[f"{fit_variable}_GEN_MUR10_MUF10_PDF265000.SR_xbb_2"][
        "histogram"
    ][1:-1]
    delta_alpha_s = (alpha_s_up - alpha_s_down) / 2
    delta_pdf_alpha_s = np.sqrt((delta_pdf**2) + (delta_alpha_s**2))

    nominal = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]
    up, down = symmetrize_up_down(
        nom=nominal,
        var=nominal + delta_pdf_alpha_s,
    )

    acc_rel_up = 1 - (np.sum(nominal) / np.sum(up))
    acc_rel_down = 1 - (np.sum(nominal) / np.sum(down))
    return up, down, acc_rel_up, acc_rel_down, edges


def symmetrize_up_down(nom, var):
    diff = np.abs(nom - var)
    up = nom + diff
    down = nom - diff
    return up, down


def get_binning(fit_variable):
    with h5py.File(files["signal"], "r") as file:
        edges = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["edges"][:]

    return edges.tolist()


def get_k2V_xsec(k2V_points):
    # https://gitlab.cern.ch/hh4b/hh4b-vbf-limits/-/blob/9e5fcd54ff34288a7ba5c511462a08c158e9573b/utils/xs.py#L61
    # Convert to numpy array to allow vector-like calculations
    k2V_points = np.array(k2V_points)
    # Calculate inclusive VBF HH xs
    xsecs = (
        19.0965502180595 * k2V_points * k2V_points
        - 44.480523260754104 * k2V_points
        + 27.113741610743634
    )
    return xsecs


def solve_xsec_reweighting(fit_variable):
    """returns prefactor solutions for each bin

    Parameters
    ----------
    fit_variable : string

    Returns
    -------
    dict
        solutions per bin
    """
    # how to script in pyhh/fitting/reweighting.ipynb

    k2v, kv, kl = symbols("k_2v,k_v,k_l")
    a, b, c, d, e, f = symbols("a,b,c,d,e,f")

    def xsec(k2v, kv, kl):
        return (
            a * (kl**2 * kv**2)
            + b * (k2v * kl * kv)
            + c * (kl * kv**3)
            + d * (k2v**2)
            + e * (k2v * kv**2)
            + f * kv**4
        )

    hypotheses = [
        "l1cvv1cv1",
        "l1cvv1p5cv1",
        "l2cvv1cv1",
        "l10cvv1cv1",
        "l1cvv1cv0p5",
        "l5mcvv1cv0p5",
    ]

    # extract couplings from strings
    def get_between(s, start, end):
        return s.split(start)[1].split(end)[0]

    # correct float
    def transform(s):
        if "p" in s:
            s = s.replace("p", ".")
        if "m" in s:
            s = "-" + s.replace("m", "")
        return float(s)

    # get bins, get correct bin numbers, since idx 0 and -1 correspond to overflow bins
    with h5py.File(
        "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-l1cvv1cv1.h5", "r"
    ) as file:
        bins = np.arange(
            1, len(file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]) + 1
        )

    # this holds the solutions for individual bins
    solutions_per_bin = {}
    # plt.figure()
    # get solutions for a...f for each bin
    for i in bins:
        equations = []
        # collect equations with hypotheses for bin i
        for h in hypotheses:
            file = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-{h}.h5"
            kl = transform(get_between(h, "l", "cvv"))
            k2v = transform(get_between(h, "cvv", "cv"))
            kv = transform(h.split("cv")[-1])
            with h5py.File(file, "r") as file:
                bin_content = file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][i]
            # Eq is proxy for math equal
            equations += [Eq(xsec(k2v, kv, kl), bin_content)]
            # e.g.
            # equations = [
            #     Eq(xsec(1, 1, 1), bin_content),
            #     Eq(xsec(1.5, 1, 1), bin_content),
            #     Eq(xsec(1, 1, 2), bin_content),
            #     Eq(xsec(1, 1, 10), bin_content),
            #     Eq(xsec(1, 0.5, 1), bin_content),
            #     Eq(xsec(1, 0.5, -5), bin_content),
            # ]
        # Solve the system of equations
        ith_bin_solution = solve(equations, (a, b, c, d, e, f))
        # i-th bin solution with values for a...f
        solutions_per_bin[i] = ith_bin_solution
        # vals = np.array([solution[k] for k in solution.keys()])
        # print(vals)
    #     plt.step(np.arange(len(vals)), vals, label=i)
    # plt.legend()

    return solutions_per_bin


def get_reweighted_hist(k2v, kv, kl, solutions):
    """get hist for given kappas after solving the xsec formula with
    solve_xsec_reweighting

    Parameters
    ----------
    k2v : float
        coupling
    kv : float
        coupling
    kl : float
        coupling
    solutions : dict
        per bin solution from solve_xsec_reweighting

    Returns
    -------
    list
        reweighted hist
    """

    a, b, c, d, e, f = symbols("a,b,c,d,e,f")
    hist = []

    def get_reweight_bin(solution):
        return (
            solution[a] * (kl**2 * kv**2)
            + solution[b] * (k2v * kl * kv)
            + solution[c] * (kl * kv**3)
            + solution[d] * (k2v**2)
            + solution[e] * (k2v * kv**2)
            + solution[f] * kv**4
        )

    for key, sol in solutions.items():
        hist += [get_reweight_bin(sol)]
    return hist


def make_hypothesis_hists(fit_variable, k2v, reweighted_hist):
    """make new histograms from k2v=0 file by scaling it with the ratio to the
    reweighted hist"""

    output_file_name = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{fit_variable}_k2v_{k2v}.root"
    weight = 0
    k2v0_file = "/lustre/fs22/group/atlas/freder/hh/run/histograms/hists-l1cvv0cv1.h5"
    with h5py.File(k2v0_file, "r") as file:
        weight = (
            reweighted_hist / file[f"{fit_variable}_NOSYS.SR_xbb_2"]["histogram"][1:-1]
        )

    with uproot.recreate(output_file_name) as output_file:
        # read the k2v=0 file
        file_name = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{fit_variable}_l1cvv0cv1.root"
        with uproot.open(file_name) as file:
            for name, classname in file.classnames(cycle=False).items():
                if "TH1" in classname:
                    hist = file[name].to_numpy()
                    # Apply weight to signal histograms
                    if "signal" in name:
                        output_file[name] = [hist[0] * weight, hist[1]]
                    else:
                        output_file[name] = [hist[0], hist[1]]
