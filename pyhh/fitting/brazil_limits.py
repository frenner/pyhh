#! /usr/bin/python

import json

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MultipleLocator

xsec = 27.113741

# Creating a figure and an axis
fig, ax = plt.subplots(figsize=(8, 10))
fig, ax = plt.subplots(figsize=(5.5, 5))
# fig, ax = plt.subplots(figsize=(8, 5))
# fig, ax = plt.subplots(figsize=(5, 4))
# fig, ax = plt.subplots(figsize=(4, 4))


models = [
    "fit/m_hh_5_l1cvv0cv1_0",
    "fit/m_hh_5_l1cvv0cv1_250_3",
    # "fit/m_hh_5_l1cvv0cv1_neos_first",
    # "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_0_last_epoch",
    # "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_0_last_epoch",
    # "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_250_3_last_epoch",
    # "fit/tomatos_bce_5_10000_lr_0p0005_k2v_-0.0",
    "fit/tomatos_bce_5_20000_lr_0p0005_l1cvv0cv1_0_best_epoch",
    # "fit/tomatos_bce_5_20000_lr_0p0005_l1cvv0cv1_0_10k",
    # "fit/tomatos_bce_5_20000_lr_0p0005_l1cvv0cv1_0_last_epoch",
    "fit/tomatos_bce_5_20000_lr_0p0005_l1cvv0cv1_250_3_best_epoch",
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_20000_l1cvv0cv1",
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p001_slope_5000_l1cvv0cv1_best_epoch",
    # "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_best_epoch",
    # "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_1000",
    # "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_last_epoch",
    # "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_plus_1_VR",
    # "fit/tomatos_cls_5_2000_study_10_lr_0p0005_bw_min_0p001_slope_20000_protect_0p001_l1cvv0cv1",
    # "fit/tomatos_cls_5_2000_study_5_lr_0p0005_bw_min_0p005_slope_20000_batching_l1cvv0cv1",
    # "fit/tomatos_cls_4_2000_study_3_lr_0p0005_bw_min_0p005_slope_5000_l1cvv0cv1",
    # "fit/tomatos_cls_6_2000_study_3_lr_0p0005_bw_min_0p005_slope_5000_l1cvv0cv1",
    # slopes
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_1000_l1cvv0cv1",
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_5000_l1cvv0cv1_last_epoch",
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_20000_l1cvv0cv1",
    # bw
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p01_slope_5000_l1cvv0cv1",
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p005_slope_5000_l1cvv0cv1_last_epoch",
    # "fit/tomatos_cls_5_2000_study_1_lr_0p0005_bw_min_0p001_slope_5000_l1cvv0cv1_last_epoch"
    # final
    # "fit/m_hh_5_l1cvv0cv1_250_3",
    # "fit/m_hh_5_l1cvv0cv1_neos_first",
    # "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_250_3_last_epoch",
    # "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1",
    # "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_plus_1_VR",
    # "fit/tomatos_cls_5_10000_study_8_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_2000",
    # "fit/tomatos_cls_5_2000_study_12_lr_0p0005_bw_min_0p001_slope_20000_best_bins_l1cvv0cv1",
    # "fit/tomatos_cls_5_10000_study_8_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_5000",
    # "fit/tomatos_cls_5_10000_study_8_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_last_epoch",
    # "fit/tomatos_cls_5_10000_study_8_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1_last_epoch_minus_1_VR",
    # "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_250_3_last_epoch",
    # linear combination diff
    #     "fit/m_hh_5_l1cvv0cv1_250_3",
    #     "fit/m_hh_5_250_3_scan/m_hh_5_k2v_-0.0",
    #     "fit/m_hh_5_l1cvv0cv1_neos_first",
    #     "fit/m_hh_5_neos_first_scan/m_hh_5_k2v_-0.0",
    #     "fit/tomatos_bce_5_10000_lr_0p0005_l1cvv0cv1_250_3_last_epoch",
    #     "fit/tomatos_bce_5_10000_lr_0p0005_scan/tomatos_bce_5_10000_lr_0p0005_k2v_-0.0",
    #     "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_l1cvv0cv1",
    #     "fit/tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000_k2v_-0.0",
]


values = []
labels = []
for m in models:
    with open(
        "/lustre/fs22/group/atlas/freder/hh/run/" + m + "/limits.json",
        "r",
    ) as f:
        lims = json.load(f)
        values.append(lims)
        labels.append(m.replace("_SR_xbb_2_bkg_only", ""))

values = np.array(values) * xsec
print()
# Plot and fill between the lines

for i, y in enumerate(values):
    x = np.linspace(i, i + 1, 10)
    ax.fill_between(x, y[0], y[1], color="#fdc536")
    ax.fill_between(x, y[1], y[2], color="#36b1bf")
    ax.hlines(y[2], x[0], x[-1], color="black")
    ax.fill_between(x, y[2], y[3], color="#36b1bf")
    ax.fill_between(x, y[3], y[4], color="#fdc536")


ax.legend(
    [
        r"Expected Limit $\pm 2\sigma$",
        r"Expected Limit $\pm 1\sigma$",
        r"Expected Limit",
    ],
    fontsize="large",
    loc="upper right",
)
plt.ylabel(r"95% CL limit on $\sigma_{\kappa_{2v}=0}$ [fb]")


labels = [
    r"$m_\mathrm{HH}$" + "\n" + r"$m_{jj}$>0 GeV" + "\n" + r"$|\Delta\eta(j,j)|>0$",
    r"$m_\mathrm{HH}$" + "\n" + r"$m_{jj}$>250 GeV" + "\n" + r"$|\Delta\eta(j,j)|>3$",
    r"BCE" + "\n" + r"$m_{jj}$>0 GeV" + "\n" + r"$|\Delta\eta(j,j)|>0$",
    r"BCE" + "\n" + r"$m_{jj}$>250 GeV" + "\n" + r"$|\Delta\eta(j,j)|>3$",
]

# labels=[
#     "neos no batching",
#     "neos with 2 batches",
# ]

# labels = [
#     "slope\n1000",
#     "slope\n5000",
#     "slope\n20000",
# ]

# labels = [
#     "bw=0.01",
#     "bw=0.005",
#     "bw=0.001",
# ]

# labels = [
#     r"BCE" + "\n" + r"$m_{jj}$>0 GeV" + "\n" + r"$|\Delta\eta(j,j)|>0$"+ "\nbest epoch",
#     r"BCE" + "\n" + r"$m_{jj}$>0 GeV" + "\n" + r"$|\Delta\eta(j,j)|>0$"+ "\nlast epoch",
# ]

# labels = [
#     "BCE\nepoch best",
#     "BCE\nepoch 10k",
#     "BCE\nepoch 20k",
# ]

# labels = [
#     "NEOS\nepoch best",
#     "NEOS\nepoch 1k",
#     "NEOS\nepoch 2k",
# ]

# labels = [
#     "NEOS\nepoch 2k",
#     "NEOS\nepoch 5k",
#     "NEOS\nepoch 10k",
#     "NEOS\nepoch 10k\nVR bkg bin -1",
# ]

labels = [
    r"$m_\mathrm{HH}$" + "\n" + r"$m_{jj}$>0 GeV" + "\n" + r"$|\Delta\eta(j,j)|>0$",
    r"$m_\mathrm{HH}$" + "\n" + r"$m_{jj}$>250 GeV" + "\n" + r"$|\Delta\eta(j,j)|>3$",
    r"BCE" + "\n" + r"$m_{jj}$>0 GeV" + "\n" + r"$|\Delta\eta(j,j)|>0$",
    r"BCE" + "\n" + r"$m_{jj}$>250 GeV" + "\n" + r"$|\Delta\eta(j,j)|>3$",
]

# labels = [
#     r"$m_\mathrm{HH}$" + "\n" + "$\kappa_\mathrm{2V}=0$",
#     r"$m_\mathrm{HH}$" + "\nreweight",
#     r"NEOS-$m_\mathrm{HH}$" + "\n" + "$\kappa_\mathrm{2V}=0$",
#     r"NEOS-$m_\mathrm{HH}$" + "\nreweight",
#     r"BCE" + "\n" + "$\kappa_\mathrm{2V}=0$",
#     r"BCE" + "\nreweight",
#     r"NEOS" + "\n" + "$\kappa_\mathrm{2V}=0$",
#     r"NEOS" + "\nreweight",
# ]

# labels = [
#     "NEOS\n",
#     "NEOS\n opt bins",
# ]


plt.xticks(
    np.arange(0.5, len(labels), step=1), labels
)  # Setting x-axis ticks in integer steps from 0 to 10

# plt.xticks(rotation=45, ha="right")
# plt.ylim([0, 70])
plt.ylim([0, ax.get_ylim()[1] * 1.1])
# plt.ylim([0, ax.get_ylim()[1] * 1.4])
ax.yaxis.set_minor_locator(MultipleLocator(5))
plt.yticks(minor=True)
plt.grid(alpha=0.35, which="both", color="grey")
plt.tight_layout()
plt.tight_layout()
pdf_path = "/lustre/fs22/group/atlas/freder/hh/run/limits/brazil_limits.pdf"
print(pdf_path)
plt.savefig(pdf_path)
