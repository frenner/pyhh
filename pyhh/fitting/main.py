import json
import logging
import os

import cabinetry
import fitting.tools
import numpy as np


def prepare(args):
    # prepare hists
    fitting.tools.transform_hists_for_cabinetry(
        fit_variable=args.variable, hypothesis=args.hypothesis
    )
    return 0


def combine(args):
    # get the nominal k2v=0 hists
    fitting.tools.transform_hists_for_cabinetry(
        fit_variable=args.variable, hypothesis="l1cvv0cv1"
    )
    # find the solution to the xsec formula
    solutions_per_bin = fitting.tools.solve_xsec_reweighting(args.variable)
    kappa_2v = np.round(np.arange(-0.1, 2.2, 0.01), 2)
    print("making hists for k2v values: ", kappa_2v)
    for k2v in kappa_2v:
        print(f"combined k2v = {k2v}")
        reweighted_hist = fitting.tools.get_reweighted_hist(
            k2v=k2v, kl=1.0, kv=1.0, solutions=solutions_per_bin
        )
        # scale all hists relative to k2v=0
        fitting.tools.make_hypothesis_hists(args.variable, k2v, reweighted_hist)


def run(args):
    # make cabinetry config
    bkg_only = True
    region = "SR_xbb_2"

    fit_name = args.variable + "_" + args.hypothesis
    # if bkg_only:
    #     fit_name += "_bkg_only"
    subssys = "_sub_sys" if args.subsys else ""
    work_dir = "/lustre/fs22/group/atlas/freder/hh/run/fit/" + fit_name + subssys
    if not os.path.isdir(work_dir):
        os.makedirs(work_dir + "/histograms/")
    os.chdir(work_dir)

    logging.basicConfig(
        filename=f"{work_dir}/fit.log",
        filemode="w",
        level=logging.DEBUG,
        format="%(asctime)s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    # need this to also print to console
    logging.getLogger().addHandler(logging.StreamHandler())
    logging.getLogger("matplotlib").setLevel(logging.INFO)
    cabinetry.set_logging()

    # now run pipeline, k2v needs to be configurable from here to batch

    config_file_path = f"{work_dir}/cabinetry_config.yaml"
    fitting.tools.build_config(
        fit_variable=args.variable,
        outfile=config_file_path,
        region=region,
        systs=args.systs,
        hypothesis=args.hypothesis,
        subsys=args.subsys,
    )
    cabinetry_config = cabinetry.configuration.load(config_file_path)
    cabinetry.configuration.print_overview(cabinetry_config)

    # # load hists
    cabinetry.templates.collect(cabinetry_config, method="uproot")
    cabinetry.templates.postprocess(cabinetry_config)
    # plots all sys
    if "m_hh" in args.variable:
        label = "$m_{HH}$ (MeV)"
    elif "tomatos" in args.variable:
        label = "Neural Network Score"
    cabinetry.visualize.templates(cabinetry_config, close_figure=True, label=label)

    # make histfactory workspace
    workspace_path = f"workspaces/{fit_name}.json"
    ws = cabinetry.workspace.build(cabinetry_config)
    cabinetry.workspace.save(ws, workspace_path)
    ws = cabinetry.workspace.load(workspace_path)

    # get model and data
    model, data = cabinetry.model_utils.model_and_data(ws, asimov=bkg_only)
    cabinetry.visualize.modifier_grid(model)

    # print(model.__dir__())
    # print(model.fullpdf_tv)
    # print(model.constraint_model.__dir__())
    # print(model.expected_actualdata(model.config.suggested_init()))

    # fit!
    tolerance = 1
    fit_results = cabinetry.fit.fit(
        model, data, goodness_of_fit=True, tolerance=tolerance
    )

    data = cabinetry.model_utils.asimov_data(model=model, fit_results=fit_results)

    # pull plot
    cabinetry.visualize.pulls(fit_results, exclude=["signal_norm"])

    # correlation matrix
    cabinetry.visualize.correlation_matrix(fit_results)

    # plot pre- and post- fits

    # ticks=np.array([5.000e+5,3.000e+6])*1.0e-3
    model_pred = cabinetry.model_utils.prediction(model)
    model_pred_postfit = cabinetry.model_utils.prediction(
        model, fit_results=fit_results
    )
    # print(model_pred.model_yields)
    # print(data)

    figures = cabinetry.visualize.data_mc(
        model_pred, data, config=cabinetry_config, log_scale=False, label=label
    )
    # ratio_panel = figures[0]["figure"].get_axes()[1]
    # ratio_panel.set_xlabel(label)
    figures = cabinetry.visualize.data_mc(
        model_pred_postfit, data, config=cabinetry_config, log_scale=False, label=label
    )
    # ratio_panel = figures[0]["figure"].get_axes()[1]
    # ratio_panel.set_xlabel(label)
    _ = cabinetry.tabulate.yields(model_pred, data)

    # nuisance parameter ranking
    # https://trexfitter-docs.web.cern.ch/trexfitter-docs/inference/ranking/
    if args.ranking:
        ranking_results = cabinetry.fit.ranking(model, data, tolerance=tolerance)
        cabinetry.visualize.ranking(ranking_results)

    # scan signal norm, not really useful without parameterization of the
    # workspace in k2v
    # scan_results = cabinetry.fit.scan(
    #     model, data, "signal_norm", par_range=(0.3, 2.5), n_steps=25
    # )
    # cabinetry.visualize.scan(scan_results)

    # it seems like the fit doesn't like too large bounds
    par_bounds = model.config.suggested_bounds()
    signal_bounds = par_bounds[model.config.poi_index]

    # standard is theta=[-5,5]
    # below 0.2 means less than 1 std errors
    factor = 1

    # multiply all bounds except for the signal
    par_bounds = [(tuple[0] * factor, tuple[1] * factor) for tuple in par_bounds]
    par_bounds[model.config.poi_index] = signal_bounds

    limit_results = cabinetry.fit.limit(
        model, data, par_bounds=par_bounds, tolerance=tolerance
    )
    cabinetry.visualize.limit(limit_results)

    with open(work_dir + "/limits.json", "w") as file:
        json.dump(list(limit_results.expected_limit), file)

    significance_results = cabinetry.fit.significance(model, data)

    description = [
        "expected -2 sigma:  ",
        "expected -1 sigma:  ",
        "expected         :  ",
        "expected +1 sigma:  ",
        "expected +2 sigma:  ",
    ]
