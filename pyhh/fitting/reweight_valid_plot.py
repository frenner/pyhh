#!/usr/bin/env python

import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
import plotter.colors
import plotter.tools
import uproot
from matplotlib import ticker as mticker
from matplotlib.offsetbox import AnchoredText

var = "m_hh_5"
# var = "tomatos_bce_5_10000_lr_0p0005"
var = "tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000"

file_nom = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{var}_l1cvv0cv1.root"
file_reweight = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{var}_k2v_-0.0.root"

# this sample is used for retrieval of weights...
# file_nom = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{var}_l1cvv1p5cv1.root"
# file_reweight = f"/lustre/fs22/group/atlas/freder/hh/run/histograms/hists_for_cabinetry/{var}_k2v_1.5.root"


with uproot.open(file_nom) as file:
    hist_nom, edges = file[f"SR_xbb_2/signal/{var}_NOSYS"].to_numpy()
    hist_nom_stat, edges = file[f"SR_xbb_2/signal/stat_signal_up"].to_numpy()

with uproot.open(file_reweight) as file:
    hist_reweight, edges = file[f"SR_xbb_2/signal/{var}_NOSYS"].to_numpy()
    hist_reweight_stat, edges = file[f"SR_xbb_2/signal/stat_signal_up"].to_numpy()

print(hist_nom)
print(hist_reweight)
print(hist_reweight / hist_nom)


plt.figure()

plot_path = f"/lustre/fs22/group/atlas/freder/hh/run/plots/{var}"
var_2_label = r"nominal"
var_1_label = r"reweighted"
var_2_values = hist_nom
var_1_values = hist_reweight
var_2_err = hist_nom_stat - hist_nom
var_1_err = hist_reweight_stat - hist_reweight

ratio_stat_err_val_1 = True
normalize = False

fig, (ax, rax) = plt.subplots(
    nrows=2,
    ncols=1,
    figsize=(5, 5),
    gridspec_kw={"height_ratios": (3, 1)},
    sharex=True,
)
hep.histplot(
    var_2_values,
    edges,
    histtype="step",
    yerr=var_2_err,
    label=var_2_label,
    ax=ax,
    density=normalize,
)

hep.histplot(
    var_1_values,
    edges,
    histtype="step",
    yerr=var_1_err,
    label=var_1_label,
    ax=ax,
    density=normalize,
)
ratio = var_1_values / var_2_values
ratio_err = (
    plotter.tools.propagate_err(
        sigmaA=var_1_err,
        sigmaB=var_2_err,
        operation="/",
        A=var_1_values,
        B=var_2_values,
    ),
)[0]


# var_1_norm = np.sum(var_1_values)
# var_2_norm = np.sum(var_2_values)
# ratio = (var_1_values / var_1_norm) / (var_2_values / var_2_norm)
hep.histplot(
    ratio,
    edges,
    histtype="errorbar",
    yerr=ratio_err,
    # plotter.tools.propagate_err(
    #     sigmaA=var_1_err / var_1_norm,
    #     sigmaB=var_2_err / var_2_norm,
    #     operation="/",
    #     A=var_1_values,
    #     B=var_2_values,
    # ),
    color="Black",
    ax=rax,
)
# if ratio_stat_err_val_1:
#     normErrLow = (var_1_values - var_1_err) / var_1_norm
#     normErrHigh = (var_1_values + var_1_err) / var_1_norm
# else:
#     normErrLow = (var_2_values - var_2_err) / var_2_norm
#     normErrHigh = (var_2_values + var_2_err) / var_2_norm

# rax.fill_between(
#     edges,
#     plotter.tools.fill_stat_holes(ratio - normErrLow),
#     plotter.tools.fill_stat_holes(ratio + normErrHigh),
#     color="tab:blue",
#     linewidth=0,
#     alpha=0.3,
#     step="post",
#     label="stat. uncertainty",
# )

rax.set_ylim([0.8, 1.2])
# draw line at 1.0
rax.axhline(y=1.0, color="tab:red", linestyle="-")

ax.set_ylabel("Events")
rax.set_ylabel("Ratio", horizontalalignment="center")


hep.atlas.set_xlabel("Neural Network Score")
# ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))

if "m_hh" in var:
    hep.atlas.set_xlabel("$m_{HH}$ (TeV)")
elif "tomaatos" in var:
    hep.atlas.set_xlabel("Neural Network Score")
# hep.mpl_magic()
newLim = list(ax.get_ylim())
newLim[1] = newLim[1] * 1.3
ax.set_ylim(newLim)
# rax.legend(loc="upper right")
ax.legend(loc="upper right")

plt.tight_layout()

rax.get_xaxis().get_offset_text().set_position((2, 0))

plt.savefig(plot_path + f"_{var_1_label}_{var_2_label}_ratio.pdf")
print("saving to: " + plot_path + f"_{var_1_label}_{var_2_label}_ratio.pdf")

plt.close(fig)
