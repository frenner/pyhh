#!/usr/bin/env python3
import argparse
import sys

import fitting.main
import merger.main
import plotter.main
import selector.main
import selector.submit

mc_test_file = "/lustre/fs22/group/atlas/freder/hh/samples/user.frenner.EJ_2024_03_27.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13144_p5855_TREE/user.frenner.37958056._000001.output-tree.root"
data_test_file = "/lustre/fs24/group/atlas/dbattulga/ntup_SH_Nov2023/Data/user.dabattul.ntup_phys_data_09112023.data16_13TeV.periodI.grp16_v01_p5855_TREE/user.dabattul.35442229._000135.output-tree.root"


def main():
    parser = argparse.ArgumentParser("pyhh")
    subparsers = parser.add_subparsers(dest="command")
    # select args
    parser_select = subparsers.add_parser("select", help="run object selection")
    parser_select.add_argument("--file", type=str, required=False)
    parser_select.add_argument("--fill", action="store_true", default=False)
    parser_select.add_argument("--dump", action="store_true", default=False)
    parser_select.add_argument("--debug", action="store_true", default=False)
    parser_select.add_argument("--do_systematics", action="store_true", default=False)
    parser_select.add_argument("--cluster", action="store_true", default=False)
    parser_select.add_argument("--test", action="store_true", default=False)

    # make submit file
    parser_submit = subparsers.add_parser("make-submit", help="HTCondor submit file")
    parser_submit.add_argument("--sample", type=str, default=None, required=True)
    parser_submit.add_argument("--do_systematics", action="store_true", default=False)

    # merge args
    parser_merge = subparsers.add_parser(
        "merge", help="merge files of same logical dataset"
    )
    parser_merge.add_argument("--sample", type=str, default=None, required=True)
    parser_merge.add_argument("--hists", action="store_true")
    parser_merge.add_argument("--dumped", action="store_true")
    parser_merge.add_argument("--debug", action="store_true")
    # # plot args
    parser_plot = subparsers.add_parser("plot", help="run plotting")
    parser_plot.add_argument("--sample", type=str, default=None)
    # # fit args
    parser_fit = subparsers.add_parser("fit", help="run fitting")
    parser_fit.add_argument("--prepare", action="store_true", default=False)
    parser_fit.add_argument("--combine", action="store_true", default=False)
    parser_fit.add_argument("--variable", type=str, default=None, required=True)
    parser_fit.add_argument("--hypothesis", type=str, default=None, required=False)
    parser_fit.add_argument("--systs", action="store_true", default=False)
    parser_fit.add_argument("--ranking", action="store_true", default=False)
    parser_fit.add_argument("--subsys", action="store_true", default=False)

    # show help if no subparser given
    args = parser.parse_args(args=None if sys.argv[1:] else ["--help"])
    if args.command == "select":
        if args.test:
            args.debug = True
            args.file = data_test_file
            success = selector.main.run(args)
            args.file = mc_test_file
            success = selector.main.run(args)
        else:
            success = selector.main.run(args)

    if args.command == "make-submit":
        success = selector.submit.run(args)

    if args.command == "merge":
        success = merger.main.run(args)

    if args.command == "plot":
        success = plotter.main.run()

    if args.command == "fit":
        if args.prepare:
            fitting.main.prepare(args)
        if args.combine:
            fitting.main.combine(args)
        if args.hypothesis:
            fitting.main.run(args)
        # TODO: add success
        success = 0
    sys.exit(success)
