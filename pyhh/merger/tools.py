import os
import subprocess

import h5py


def init_out_file(read_file_path, merge_file, hists=False, dumps=False):
    """
    init empty merge file by reading keys from a to be merged file

    Parameters
    ----------
    read_file_path : str
        path to some file to be merged
    merge_file : h5py.File
        to be merged outfile
    hists : bool, optional
        histogram datastructure, by default False
    dumps : bool, optional
        dumped variable datastructure, by default False

    Returns
    -------
    h5py.File
        h5 out file object
    """

    with h5py.File(read_file_path, "r") as read_file:
        for hist in read_file.keys():
            # copy read_file as merge_file
            read_file.copy(hist, merge_file)
        if hists:
            # init datastructure values to 0 if histogram structure
            for hist in merge_file.keys():
                # go over hist, w, w2
                hist_vars = list(merge_file[hist].keys())
                # don't want to add up edges
                if "edges" in hist_vars:
                    hist_vars.remove("edges")
                for ds in hist_vars:
                    merge_file[hist][ds][:] = 0
        if dumps:
            for ds in merge_file.keys():
                merge_file[ds].resize((0,))

    return merge_file


def get_pbar_location():
    """
    figure out progress bar position by nr of running python processes

    Returns
    -------
    int
        progress bar position
    """
    # import time
    # time.sleep(1)
    process_ids = subprocess.check_output(
        "ps -u freder | grep pyhh |  awk '{print $1}'",
        shell=True,
        text=True,
        universal_newlines=True,
    )
    process_ids = process_ids.split("\n")[:-1]
    pid_map = {}
    for k in range(len(process_ids)):
        pid_map[process_ids[k]] = k
    this_process_id = str(os.getpid())
    return pid_map[this_process_id]
