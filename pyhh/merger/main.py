#!/usr/bin/env python3

import h5py
import merger.tools
import selector.analysis
import selector.configuration
import selector.tools
from tools.logging import log
from tqdm.auto import tqdm


def run(args):
    """
    Merges histogram samples by adding the histograms of an entire sample or
    concatenates dumped variables into one file. It uses files created with
    pyhh.selector.

    Parameters
    ----------
    args : Namespace
        args from pyhh.main
    """

    if args.hists:
        filelist = selector.tools.construct_file_list(
            args.sample, merge_processed_hists=True
        )
        merge_file_path = (
            selector.configuration.hist_top_path + "hists-" + args.sample + ".h5"
        )
        with h5py.File(merge_file_path, "w") as merge_file:
            merge_file = merger.tools.init_out_file(
                read_file_path=filelist[0], merge_file=merge_file, hists=True
            )
            log.info("Merge histograms into: " + merge_file_path)
            # progressbar
            pbar = tqdm(
                total=len(filelist),
                position=merger.tools.get_pbar_location(),
                leave=True,
            )
            # loop over files to merge and add values into merged out file
            for ith_file_path in filelist:
                if args.debug:
                    log.debug(ith_file_path)
                with h5py.File(ith_file_path, "r") as f_i:
                    for hist in f_i.keys():
                        hist_vars = list(merge_file[hist].keys())
                        # don't want to add up the edges
                        hist_vars.remove("edges")
                        for ds in hist_vars:
                            merge_file[hist][ds][:] += f_i[hist][ds][:]
                pbar.update(1)
            pbar.close()

    if args.dumped:
        filelist = selector.tools.construct_file_list(
            args.sample, merge_processed_dumps=True
        )
        merge_file_path = (
            selector.configuration.dump_top_path + "dump-" + args.sample + ".h5"
        )
        # append vars to final file
        with h5py.File(merge_file_path, "w") as merge_file:
            merge_file = merger.tools.init_out_file(
                read_file_path=filelist[0], merge_file=merge_file, dumps=True
            )
            log.info("Merge dumped variables into: " + merge_file_path)
            pbar = tqdm(
                total=len(filelist),
                position=merger.tools.get_pbar_location(),
                leave=True,
            )
            # loop over input files
            for ith_file_path in filelist:
                with h5py.File(ith_file_path, "r") as f_i:
                    for ds_key in f_i.keys():
                        selector.tools.write_vars_to_h5(
                            f=merge_file,
                            values=f_i[ds_key][:],
                            ds_key=ds_key,
                        )
                pbar.update(1)
        pbar.close()
    return
