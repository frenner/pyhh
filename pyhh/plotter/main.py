#!/usr/bin/env python3
import os

import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
import plotter.colors
import plotter.load_hists
import plotter.plots
import plotter.tools

# def draw_text(ax):
#     """
#     Draw two text-boxes, anchored by different corners to the upper-left
#     corner of the figure.
#     """
#     at = AnchoredText(
#         "Figure 1a",
#         loc="upper left",
#         prop=dict(size=8),
#         frameon=True,
#     )
#     at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
#     ax.add_artist(at)

#     at2 = AnchoredText(
#         "Figure 1(b)",
#         loc="lower left",
#         prop=dict(size=8),
#         frameon=True,
#         bbox_to_anchor=(0.0, 1.0),
#         bbox_transform=ax.transAxes,
#     )
#     at2.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
#     ax.add_artist(at2)


def run():
    np.seterr(divide="ignore", invalid="ignore")
    plt.style.use(hep.style.ATLAS)
    plot_path = "/lustre/fs22/group/atlas/freder/hh/run/plots/thesis/"
    if not os.path.isdir(plot_path):
        os.makedirs(plot_path)

    var = "tomatos_cls_5_2000_study_6_lr_0p0005_bw_min_0p001_slope_20000"
    # var = "tomatos_bce_5_10000_lr_0p0005"
    # var = "m_hh_5"
    var = "tomatos_cls_5_10000_study_8_lr_0p0005_bw_min_0p001_slope_20000"

    # hists = plotter.load_hists.run(systs=False)
    hists = plotter.load_hists.run(
        # histkeys=[
        #     # "massplane_NOSYS.xbb_1",
        #     # "massplane_NOSYS.xbb_2",
        #     # "tomatos_cls_5_NOSYS.SR_xbb_1",
        #     # "tomatos_cls_5_NOSYS.SR_xbb_2",
        #     # "tomatos_cls_5_NOSYS.CR_xbb_2",
        # ]
    )
    plotter.plots.trigger_eff(hists, plot_path)

    # plotter.plots.mc_data_ratio(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="m_hh_5_NOSYS.SR_xbb_2",
    #     bkg_estimate=True,
    #     s_over_b=True,
    #     log_scale=False,
    #     signal_key="k2v0",
    #     draw_ttbar=False,
    # )
    # plotter.plots.mc_data_ratio(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="atos_cls_5_sys_NOSYS.SR_xbb_2",
    #     bkg_estimate=True,
    #     s_over_b=False,
    #     log_scale=False,
    #     signal_key="k2v0",
    #     draw_ttbar=False,
    # )
    plotter.plots.nominal_hist(
        hists,
        plot_path=plot_path,
        histkey=f"{var}_NOSYS.SR_xbb_2",
    )
    # plotter.plots.mc_data_ratio(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="m_hh_NOSYS.CR_xbb_2",
    #     bkg_estimate=True,
    #     s_over_b=True,
    #     log_scale=False,
    #     signal_key="k2v0",
    #     draw_ttbar=False,
    # )

    # plotter.plots.massplane(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="massplane_NOSYS.xbb_1",
    #     sample="run2",
    # )
    # plotter.plots.massplane(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="massplane_NOSYS.xbb_2",
    #     sample="run2",
    # )
    # plotter.plots.massplane(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="massplane_NOSYS.xbb_2",
    #     sample="SM",
    # )
    # plotter.plots.massplane(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="massplane_NOSYS.xbb_2",
    #     sample="k2v0",
    # )

    # limits()
    # for var in histkeys_with_regions:
    #     if "massplane" in var:
    #         massplane(hists,var)
    #     else:
    #         mc_data_ratio(hists, var, bkg_estimate=False, s_over_b=True)

    # for var in histkeys_with_regions:
    #     if "2b2b" in var and not "massplane" in var:
    #         mc_data_ratio(hists, var, bkg_estimate=True, s_over_b=True)

    # makeGrid()

    # plotter.plots.compareABCD(
    #     hists,
    #     plot_path=plot_path,
    #     histkey="tomatos_cls_5_NOSYS.CR_xbb_2",
    #     lowTaghistkey="tomatos_cls_5_NOSYS.CR_xbb_1",
    #     subtract_ttbar=False,
    # )

    plotter.plots.compareABCD(
        hists,
        plot_path=plot_path,
        histkey=f"{var}_NOSYS.VR_xbb_2",
        lowTaghistkey=f"{var}_NOSYS.VR_xbb_1",
        subtract_ttbar=False,
    )

    ratio_values = (
        hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["h"]
        / np.sum(hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["h"])
    ) / (
        hists["run2"][f"{var}_NOSYS.VR_xbb_1"]["h"]
        / np.sum(hists["run2"][f"{var}_NOSYS.VR_xbb_1"]["h"])
    )
    ratio_err = (
        plotter.tools.propagate_err(
            sigmaA=hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["err"],
            sigmaB=hists["run2"][f"{var}_NOSYS.VR_xbb_1"]["err"],
            operation="/",
            A=hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["h"],
            B=hists["run2"][f"{var}_NOSYS.VR_xbb_1"]["h"],
        ),
    )

    plotter.plots.simple_ratio(
        plot_path=plot_path + f"{var}_ratio_CR_VR.pdf",
        var_1_label="Data, CR, 1 GN2X tag",
        var_2_label="Data, VR, 1 GN2X tag",
        text=" ",
        var_1_values=hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["h"],
        var_2_values=hists["run2"][f"{var}_NOSYS.VR_xbb_1"]["h"],
        var_1_err=hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["err"],
        var_2_err=hists["run2"][f"{var}_NOSYS.VR_xbb_1"]["err"],
        edges=hists["run2"][f"{var}_NOSYS.CR_xbb_1"]["edges"],
        ratio_values=ratio_values,
        ratio_err=ratio_err,
        x_label="$m_\mathrm{HH}$ [TeV]" if "m_hh" in var else "Neural Network Score",
        normalize=True,
        ratio_lim=[1.2, 0.8],
        figsize=(6.5, 6),
        y_label="Normalized",
    )
