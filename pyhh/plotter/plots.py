import json
import os

import fitting.tools
import matplotlib.pyplot as plt
import mplhep as hep
import numpy as np
import plotter.colors
import plotter.tools
import selector.helpers
from matplotlib import ticker as mticker
from matplotlib.offsetbox import AnchoredText
from matplotlib.ticker import MaxNLocator
from pdf2image import convert_from_path
from tools.logging import log


def trigger_eff(hists, plot_path):
    # fmt: off
    data_trigger = hists["run2"]["leadingLargeRpt_NOSYS.trigger_m_50"]["hRaw"]
    data_trigger_ref = hists["run2"]["leadingLargeRpt_NOSYS.trigger_ref_m_50"]["hRaw"]

    data_trigger_err = hists["run2"]["leadingLargeRpt_NOSYS.trigger_m_50"]["err"]
    data_trigger_ref_err = hists["run2"]["leadingLargeRpt_NOSYS.trigger_ref_m_50"]["err"]

    k2v0_trigger_ref = hists["k2v0"]["leadingLargeRpt_NOSYS.trigger_ref_m_50"]["hRaw"]
    k2v0_trigger = hists["k2v0"]["leadingLargeRpt_NOSYS.trigger_m_50"]["hRaw"]
    k2v0_trigger_trigger_ref = hists["k2v0"]["leadingLargeRpt_NOSYS.trigger_trigger_ref_m_50"]["hRaw"]
    k2v0_triggerAll = hists["k2v0"]["leadingLargeRpt_NOSYS.all"]["hRaw"]

    # fmt: on
    edges = hists["k2v0"]["leadingLargeRpt_NOSYS.trigger_m_50"]["edges"]

    data_eff = data_trigger / data_trigger_ref
    data_eff_err = plotter.tools.getEfficiencyErrors(
        passed=data_trigger,
        reference=data_trigger_ref,
    )

    #  normalize
    values = np.repeat((edges[:-1] + edges[1:]) / 2.0, data_trigger.astype(int))
    norm_data_trigger = np.array(plt.hist(values, edges, density=True)[0], dtype=float)
    # norm_data_trigger /= np.max(norm_data_trigger) * 1

    values = np.repeat((edges[:-1] + edges[1:]) / 2.0, k2v0_trigger.astype(int))
    norm_k2v0_trigger = np.array(plt.hist(values, edges, density=True)[0], dtype=float)
    # norm_k2v0_trigger /= np.max(norm_k2v0_trigger) * 1

    # values = np.repeat((edges[:-1] + edges[1:]) / 2.0, trigger_leadingLargeR.astype(int))
    # trigger_leadingLargeR = np.array(
    #     plt.hist(values, edges, density=True, cumulative=True)[0], dtype=float
    # )

    k2v0_eff = k2v0_trigger / k2v0_triggerAll
    k2v0_eff_err = plotter.tools.getEfficiencyErrors(
        passed=k2v0_trigger,
        reference=k2v0_triggerAll,
    )

    k2v0_eff_trigger_trigger_ref = k2v0_trigger_trigger_ref / k2v0_trigger_ref
    k2v0_eff_trigger_trigger_ref_err = plotter.tools.getEfficiencyErrors(
        passed=k2v0_trigger_trigger_ref,
        reference=k2v0_trigger_ref,
    )
    k2v0_eff_trigger_ref = k2v0_trigger_ref / k2v0_triggerAll
    k2v0_eff_trigger_ref_err = plotter.tools.getEfficiencyErrors(
        passed=k2v0_trigger_ref,
        reference=k2v0_triggerAll,
    )

    # scale factors
    sf = data_eff / k2v0_eff_trigger_trigger_ref

    sf_err = plotter.tools.propagate_err(
        np.max(data_eff_err, axis=0),
        np.max(k2v0_eff_trigger_trigger_ref_err, axis=0),
        "/",
        data_eff,
        k2v0_eff_trigger_trigger_ref,
    )

    with open(
        "/lustre/fs22/group/atlas/freder/hh/pyhh/pyhh/selector/trigger_sfs.json", "w"
    ) as f:
        json.dump(
            {"edges": edges.tolist(), "eff": sf.tolist(), "error": sf_err.tolist()}, f
        )

    fig, ax, rax = simple_ratio(
        plot_path=plot_path + "trigger_ratio_plot.pdf",
        var_1_label="Run 2 Data \n(Trigger & Reference Trigger) / (Reference Trigger)",
        var_2_label=r"$\kappa_\mathrm{2V}=0$ Signal"
        + "\n(Trigger & Reference Trigger) / (Reference Trigger)",
        var_1_values=data_eff,
        var_2_values=k2v0_eff_trigger_trigger_ref,
        var_1_err=data_eff_err,
        var_2_err=k2v0_eff_trigger_trigger_ref_err,
        edges=edges,
        ratio_values=sf,
        ratio_err=sf_err,
        normalize=False,
        ratio_lim=[0.6, 1.2],
        x_lim=[260e3, 1000e3],
        x_label="Leading Large-R Jet p$_\mathrm{T}$ [GeV]",
        y_label="Efficiency",
        figsize=(10, 9.25),
        grid=True,
        third_panel=True,
    )
    hep.histplot(
        k2v0_eff_trigger_ref,
        bins=edges,
        histtype="step",
        ax=ax,
        yerr=k2v0_eff_trigger_ref_err,
        label=r"$\kappa_\mathrm{2V}=0$ Signal"
        + "\n(Reference Trigger) / (All MC events)",
    )

    hep.histplot(
        k2v0_eff,
        bins=edges,
        histtype="step",
        ax=ax,
        yerr=k2v0_eff_err,
        label=r"$\kappa_\mathrm{2V}=0$ Signal" + "\n(Trigger) / (All MC events)",
    )

    ax, mid_ax, rax = fig.get_axes()
    hep.histplot(
        norm_k2v0_trigger,
        bins=edges,
        histtype="fill",
        alpha=0.7,
        ax=mid_ax,
        color="tab:purple",
        yerr=k2v0_eff_err,
        label=r"$\kappa_\mathrm{2V}=0$ Signal Triggered Events",
    )

    hep.histplot(
        norm_data_trigger,
        bins=edges,
        histtype="fill",
        alpha=0.7,
        ax=mid_ax,
        color="tab:gray",
        yerr=k2v0_eff_err,
        label="Run 2 Data Triggered Events",
    )
    mid_ax.legend()
    mid_ax.set_ylim(top=np.max(norm_data_trigger))
    mid_ax.set_ylabel("Prob.\nDensity", horizontalalignment="center")
    mid_ax.grid()
    ax.xaxis.set_major_locator(MaxNLocator(nbins=8))
    ax.set_ylim([0.0, 1.05])

    ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))
    ax.axvline(x=450e3, color="black", linestyle="-", label="450 GeV Cut")
    mid_ax.axvline(x=450e3, color="black", linestyle="-")
    rax.axvline(x=450e3, color="black", linestyle="-")
    ax.legend(loc="center right")

    plt.savefig(plot_path + "trigger_ratio_plot.pdf")


def accEff_mhh():
    keys = [
        "nTriggerPass_mhh",
        "nTwoLargeR_mhh",
        "nTwoSelLargeR_mhh",
        # "btagLow_1b1j_mhh",
        # "btagLow_2b1j_mhh",
        # "btagLow_2b2j_mhh",
        # "btagHigh_1b1b_mhh",
        # "btagHigh_2b1b_mhh",
        # "btagHigh_2b2b_mhh",
    ]
    hists_ = []
    for key in keys:
        print(key)
        hists_.append(hists[key]["hRaw"])
        print(hists[key]["hRaw"])
    print(hists["mhh"]["hRaw"])
    hists_cumulative, hists_cumulative_err = plotter.tools.CumulativeEfficiencies(
        hists_, baseline=hists["mhh_twoLargeR"]["hRaw"], stopCumulativeFrom=4
    )
    labels = [
        "passed Trigger",
        "≥2 LargeR",
        "Kinematic Selection",
        # "btagLow 1b1j",
        # "btagLow 2b1j",
        # "btagLow 2b2j",
        # "btagHigh 1b1b",
        # "btagHigh 2b1b",
        # "btagHigh 2b2b",
    ]
    #         "$p_T(H1)$>450 GeV, $p_T(H2)$>250 GeV, $|\eta\| < 2.0$",

    plt.figure()

    for i, (h, err, label) in enumerate(
        zip(hists_cumulative, hists_cumulative_err, labels)
    ):
        hep.histplot(
            h,
            file["nTriggerPass_mhh"]["edges"],
            histtype="errorbar",
            label=label,
            yerr=err,
            alpha=0.7,
            solid_capstyle="projecting",
            capsize=3,
            color="C{}".format(i),
        )

    hep.atlas.text(" Simulation", loc=1)
    hep.atlas.set_ylabel("Acc x Efficiency")
    hep.atlas.set_xlabel("$m_{hh}$ $[GeV]$ ")
    ax = plt.gca()

    # ax.set_ylim([0, 1.2])
    plt.legend(loc="upper left", bbox_to_anchor=(0.01, 0.9))
    hep.rescale_to_axessize
    plt.tight_layout()
    ax.get_xaxis().get_offset_text().set_position((2, 0))
    ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))
    plt.savefig(plot_path + "accEff_mhh.pdf")
    plt.close()


def triggerpT():
    plt.figure()
    err = plotter.tools.getEfficiencyErrors(
        passed=hists["leadingLargeRpT_trigger"]["hRaw"],
        total=hists["leadingLargeRpT"]["hRaw"],
    )
    hep.histplot(
        hists["leadingLargeRpT_trigger"]["h"] / hists["leadingLargeRpT"]["h"],
        file["leadingLargeRpT"]["edges"],
        histtype="errorbar",
        yerr=err,
        density=False,
        # alpha=0.75,
        solid_capstyle="projecting",
        capsize=3,
        label="trigPassed_HLT_j420_a10_lcw_L1J100",
    )
    hep.atlas.text(" Simulation", loc=1)
    hep.atlas.set_ylabel("Trigger efficiency")
    hep.atlas.set_xlabel("Leading Large R Jet p$_T$ $[GeV]$ ")
    ax = plt.gca()
    plt.tight_layout()
    ax.get_xaxis().get_offset_text().set_position((2, 0))
    ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))
    plt.legend(loc="lower right")
    plt.savefig(plot_path + "triggerpT.pdf")
    plt.close()


def massplane(hists, plot_path, histkey, sample):
    plt.figure()
    xbins = hists[sample][histkey]["xbins"]
    ybins = hists[sample][histkey]["ybins"]
    histValues = hists[sample][histkey]["h"]
    plane = hep.hist2dplot(
        histValues,
        xbins=xbins,
        ybins=ybins,
    )
    plane.pcolormesh.set_cmap("Blues")

    hep.atlas.set_ylabel("m$_\mathrm{H2}$ [GeV]")
    hep.atlas.set_xlabel("m$_\mathrm{H1}$ [GeV]")
    ax = plt.gca()

    X, Y = np.meshgrid(xbins, ybins)
    linewidth = 1.5
    CS1 = plt.contour(
        X, Y, selector.helpers.Xhh(X, Y), [1.6], colors="tab:red", linewidths=linewidth
    )
    fmt = {}
    strs = ["SR"]
    for l, s in zip(CS1.levels, strs):
        fmt[l] = s
    ax.clabel(CS1, CS1.levels[::2], inline=True, fmt=fmt, fontsize=12)

    # # to show opening of SR contour
    # for i in np.linspace(0, 5, 10):
    #     CS1 = plt.contour(X, Y, Plotting.tools.Xhh(X, Y), [i], linewidths=linewidth)
    #     fmt = {}
    #     strs = [str(np.round(i, 2))]
    #     for l, s in zip(CS1.levels, strs):
    #         fmt[l] = s
    #     ax.clabel(CS1, CS1.levels[::2], inline=True, fmt=fmt, fontsize=12)
    CS1 = plt.contour(
        X,
        Y,
        selector.helpers.CR_hh(X, Y),
        [100e3],
        colors="orange",
        linewidths=linewidth,
    )
    fmt = {}
    strs = ["VR"]
    for l, s in zip(CS1.levels, strs):
        fmt[l] = s
    ax.clabel(CS1, CS1.levels[::2], inline=True, fmt=fmt, fontsize=12)

    CS1 = plt.contour(
        X,
        Y,
        selector.helpers.CR_hh(X, Y),
        [170e3],
        colors="tab:green",
        linewidths=linewidth,
    )
    fmt = {}
    strs = ["CR"]
    for l, s in zip(CS1.levels, strs):
        fmt[l] = s
    ax.clabel(CS1, CS1.levels[::2], inline=True, fmt=fmt, fontsize=12)
    # if blind:
    #     CS1 = plt.contourf(X, Y, Plotting.tools.Xhh(X, Y), [0, 1.6], colors="black")

    plt.tight_layout()
    ax.get_xaxis().get_offset_text().set_position((2, 0))
    ax.get_yaxis().get_offset_text().set_position((2, 0))
    ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))
    ax.yaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))
    ax.set_aspect("equal")

    # plotter.tools.plotLabel(histkey, ax)
    # plt.text(f"VBF 4b, {region}, 2b2j")
    # plt.legend(loc="upper right")

    # hep.atlas.label(
    #     # data=False,
    #     lumi="140.0",
    #     loc=1,
    #     ax=ax,
    #     llabel="Internal",
    # )
    bb_tag = histkey.split("_")[-1]
    if "run2" in sample:
        sample_text = "Data"
    elif "SM" in sample:
        sample_text = "SM"
    elif "k2v0" in sample:
        sample_text = "$\kappa_\mathrm{2V}=0$"

    ax.text(
        x=0.075,
        y=0.95,
        s=r"Run 2, VBF $HH \rightarrow 4b$"
        + "\n"
        + sample_text
        + f", GN2X tag {bb_tag}",
        transform=ax.transAxes,
        verticalalignment="top",
        horizontalalignment="left",
        fontsize=12,
    )
    # hep.atlas.text(text=rf"Run 2, VBF $HH \rightarrow 4b$ \n {sample_text}",)
    out_path = plot_path + histkey + "_" + sample + ".pdf"
    plt.savefig(out_path)
    log.info(out_path)
    plt.close()


def mc_data_ratio(
    hists,
    plot_path,
    histkey,
    s_over_b=False,
    bkg_estimate=False,
    rebin_factor=None,
    log_scale=False,
    signal_key="SM",
    draw_ttbar=True,
):
    log.info("Plotting " + histkey)
    s = hists[signal_key][histkey]["h"]
    s_err = hists[signal_key][histkey]["err"]
    data = hists["run2"][histkey]["h"]
    data_err = hists["run2"][histkey]["err"]
    if draw_ttbar:
        tt = hists["ttbar"][histkey]["h"]
        tt_err = hists["ttbar"][histkey]["err"]
    edges = hists["run2"][histkey]["edges"]

    jj, jj_err = get_multijets(hists, histkey, bkg_estimate, subtract_ttbar=draw_ttbar)
    if rebin_factor:
        s, edges_, s_err = plotter.tools.factorRebin(
            h=s,
            edges=edges,
            factor=rebin_factor,
            err=s_err,
        )
        data, edges_, data_err = plotter.tools.factorRebin(
            h=data,
            edges=edges,
            factor=rebin_factor,
            err=data_err,
        )
        tt, edges_, tt_err = plotter.tools.factorRebin(
            h=tt,
            edges=edges,
            factor=rebin_factor,
            err=tt_err,
        )
        jj, edges_, jj_err = plotter.tools.factorRebin(
            h=jj,
            edges=edges,
            factor=rebin_factor,
            err=jj_err,
        )
        edges = edges_

    # prediction
    if draw_ttbar:
        pred = tt + jj
        pred_err = plotter.tools.propagate_err(tt_err, jj_err, operation="+")
    else:
        pred = jj
        pred_err = jj_err

    ratio = data / pred
    ratio_err = plotter.tools.propagate_err(
        data_err,
        pred_err,
        "/",
        data,
        pred,
    )

    plt.figure()
    if s_over_b:
        fig, (ax, rax, rax2) = plt.subplots(
            nrows=3,
            ncols=1,
            figsize=(8, 8 * 1.25),
            gridspec_kw={"height_ratios": (3, 1, 1)},
            sharex=True,
        )
    else:
        fig, (ax, rax) = plt.subplots(
            nrows=2,
            ncols=1,
            figsize=(8, 8),
            gridspec_kw={"height_ratios": (3, 1)},
            sharex=True,
        )
    # stack plot
    if draw_ttbar:
        hep.histplot(
            [tt, jj],
            edges,
            stack=True,
            histtype="fill",
            # yerr=True,
            label=["$t\overline{t}$", "Background"],
            ax=ax,
            color=["hh:darkpink", "hh:medturquoise"],
            edgecolor="black",
            linewidth=0.5,
        )
    else:
        hep.histplot(
            jj,
            edges,
            stack=False,
            histtype="fill",
            # yerr=True,
            label=["Background"],
            ax=ax,
            color=["hh:medturquoise"],
            edgecolor="black",
            linewidth=0.5,
        )

    # error stackplot
    ax.fill_between(
        edges,
        np.append(pred - pred_err, 0),
        np.append(pred + pred_err, 0),
        color="dimgrey",
        linewidth=0,
        alpha=0.3,
        step="post",
        label="stat. uncertainty",
    )

    # data
    hep.histplot(
        data,
        edges,
        histtype="errorbar",
        yerr=data_err,
        color="Black",
        label="data",
        ax=ax,
    )
    # Signal

    hep.histplot(
        s,  # * 10000,
        edges,
        histtype="step",
        # yerr=True,
        label=signal_key + " Signal",  # x $10^4$",
        ax=ax,
        color="hh:darkyellow",  # "orangered",
        linewidth=1.25,
    )
    ax.legend(loc="upper right")
    # ax.autoscale()
    # ax.get_ylim()
    # if ymax:
    #     ax.set_ylim([1e-3, 1e6])
    # else:
    #     ax.set_ylim([1e-3, 1e6])
    if log_scale:
        ax.set_yscale("log")

    # ax.set_ylim([0.0, 8])

    # ratio plot
    hep.histplot(
        ratio,
        edges,
        histtype="errorbar",
        yerr=ratio_err,
        ax=rax,
        color="Black",
    )
    normErrLow = (pred - pred_err) / pred
    normErrHigh = (pred + pred_err) / pred
    # error ratioplot
    rax.fill_between(
        edges,
        plotter.tools.fill_stat_holes(normErrLow),
        plotter.tools.fill_stat_holes(normErrHigh),
        color="dimgrey",
        linewidth=0,
        alpha=0.3,
        step="post",
        # label="stat. uncertainty",
    )
    rax.set_ylim([0.0, 2])
    # draw line at 1.0
    rax.axhline(y=1.0, color="tab:red", linestyle="-")

    plt.tight_layout()

    if s_over_b:
        sqrt_pred = np.sqrt(pred)
        sqrt_pred_err = plotter.tools.propagate_err(
            A=pred, sigmaA=pred_err, operation="^", exp=0.5
        )
        ratio2 = s / sqrt_pred
        ratio2_err = plotter.tools.propagate_err(
            s_err,
            sqrt_pred_err,
            "/",
            s,
            sqrt_pred,
        )
        hep.histplot(
            ratio2,
            edges,
            histtype="errorbar",
            yerr=ratio2_err,
            ax=rax2,
            color="Black",
        )
        rax2.set_ylabel(
            "$\mathrm{S}/\sqrt{\mathrm{Pred.}}$", horizontalalignment="center"
        )
        # rax2.set_ylabel(
        #     r"$ \frac{\mathrm{S}}{\sqrt{\mathrm{Pred.}}}$", horizontalalignment="center"
        # )
    ax.set_ylabel("Events")
    rax.set_ylabel(
        r"$ \frac{\mathrm{Data}}{\mathrm{Pred.}}$", horizontalalignment="center"
    )

    labels = plotter.tools.plotLabel(histkey, ax)

    if "eta" in histkey or "phi" in histkey or "dR" in histkey:
        hep.atlas.set_xlabel(f"{labels['var']}")
    else:
        hep.atlas.set_xlabel(f"{labels['var']} [GeV]")
        ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))

    # hep.mpl_magic()
    # newLim = list(ax.get_ylim())
    # newLim[1] = newLim[1] * 100
    # ax.set_ylim(newLim)

    plt.tight_layout()
    if s_over_b:
        rax2.get_xaxis().get_offset_text().set_position((2, 0))
    else:
        rax.get_xaxis().get_offset_text().set_position((2, 0))

    # to show subticks of logplot
    # ax.yaxis.set_major_locator(mticker.LogLocator(numticks=999))
    # ax.yaxis.set_minor_locator(mticker.LogLocator(numticks=999, subs="auto"))
    # rax2.yaxis.set_major_locator(mticker.LogLocator(numticks=999))
    # rax2.yaxis.set_minor_locator(mticker.LogLocator(numticks=999, subs="auto"))
    if bkg_estimate:
        plt.savefig(plot_path + f"{histkey}_bkg_estimate_ratio.pdf")
        log.info("saving to : " + plot_path + f"{histkey}_bkg_estimate_ratio.pdf")
    else:
        plt.savefig(plot_path + f"{histkey}_ratio.pdf")
        log.info("saving to : " + plot_path + f"{histkey}_ratio.pdf")

    plt.close(fig)


def get_multijets(hists, histkey, bkg_estimate, subtract_ttbar):
    if bkg_estimate:
        lowTaghistkey = histkey[:-1] + "1"
        dataLowTag = hists["run2"][lowTaghistkey]["h"]
        dataLowTag_err = hists["run2"][lowTaghistkey]["err"]
        observable = histkey.split("_NOSYS")[0]
        w_CR, err_w_CR = fitting.tools.get_bkg_weight(
            subtract_ttbar=False, fit_variable=observable
        )

        if subtract_ttbar:
            ttLowTag = hists["ttbar"][lowTaghistkey]["h"]
            ttLowTag_err = hists["ttbar"][lowTaghistkey]["err"]
            jj = (dataLowTag - ttLowTag) * w_CR
            jj_err = plotter.tools.propagate_err(
                sigmaA=plotter.tools.propagate_err(
                    sigmaA=dataLowTag_err,
                    sigmaB=ttLowTag_err,
                    operation="-",
                ),
                sigmaB=np.ones(jj.shape) * err_w_CR,
                operation="*",
                A=dataLowTag - ttLowTag,
                B=np.ones(jj.shape) * w_CR,
            )
        else:
            jj = dataLowTag * w_CR
            jj_err = plotter.tools.propagate_err(
                sigmaA=dataLowTag_err,
                sigmaB=err_w_CR,
                operation="*",
                A=dataLowTag,
                B=w_CR,
            )
    else:
        jj = hists["dijet"][histkey]["h"]
        jj_err = hists["dijet"][histkey]["err"]

    return jj, jj_err


def systematic_impact(
    hists,
    histkey,
    sys_up,
    sys_down,
):
    log.info("Plotting " + histkey)
    s = hists["SM"][histkey]["h"]
    s_err = hists["SM"][histkey]["err"]
    data = hists["run2"][histkey]["h"]
    data_err = hists["run2"][histkey]["err"]
    tt = hists["ttbar"][histkey]["h"]
    tt_err = hists["ttbar"][histkey]["err"]
    edges = hists["run2"][histkey]["edges"]

    jj, jj_err = get_multijets(hists, histkey, bkg_estimate=True)

    # prediction
    pred = tt + jj
    pred_err = plotter.tools.propagate_err(tt_err, jj_err, operation="+")

    ratio_up = data / pred
    ratio_err = plotter.tools.propagate_err(
        data_err,
        pred_err,
        "/",
        data,
        pred,
    )

    plt.figure()
    fig, (ax, rax) = plt.subplots(
        nrows=2,
        ncols=1,
        figsize=(8, 8),
        gridspec_kw={"height_ratios": (3, 1)},
        sharex=True,
    )
    # stack plot
    hep.histplot(
        [tt, jj],
        edges,
        stack=True,
        histtype="fill",
        # yerr=True,
        label=["$t\overline{t}$", "Background"],
        ax=ax,
        color=["hh:darkpink", "hh:medturquoise"],
        edgecolor="black",
        linewidth=0.5,
    )

    # error stackplot
    ax.fill_between(
        edges,
        np.append(pred - pred_err, 0),
        np.append(pred + pred_err, 0),
        color="dimgrey",
        linewidth=0,
        alpha=0.3,
        step="post",
        label="stat. uncertainty",
    )

    # data
    hep.histplot(
        data,
        edges,
        histtype="errorbar",
        yerr=data_err,
        color="Black",
        label="data",
        ax=ax,
    )
    # Signal

    hep.histplot(
        s * 10000,
        edges,
        histtype="step",
        # yerr=True,
        label="SM Signal x $10^4$",
        ax=ax,
        color="hh:darkyellow",  # "orangered",
        linewidth=1.25,
    )
    ax.legend(loc="upper right")
    ax.autoscale()
    # ax.get_ylim()
    # if ymax:
    #     ax.set_ylim([1e-3, 1e6])
    # else:
    #     ax.set_ylim([1e-3, 1e6])
    ax.set_yscale("log")

    # ratio plot
    hep.histplot(
        ratio,
        edges,
        histtype="errorbar",
        yerr=ratio_err,
        ax=rax,
        color="Black",
    )
    normErrLow = (pred - pred_err) / pred
    normErrHigh = (pred + pred_err) / pred
    # error ratioplot
    rax.fill_between(
        edges,
        plotter.tools.fill_stat_holes(normErrLow),
        plotter.tools.fill_stat_holes(normErrHigh),
        color="dimgrey",
        linewidth=0,
        alpha=0.3,
        step="post",
        # label="stat. uncertainty",
    )
    rax.set_ylim([0.0, 2])
    # draw line at 1.0
    rax.axhline(y=1.0, color="tab:red", linestyle="-")

    plt.tight_layout()

    if s_over_b:
        sqrt_pred = np.sqrt(pred)
        sqrt_pred_err = plotter.tools.propagate_err(
            A=pred, sigmaA=pred_err, operation="^", exp=0.5
        )
        ratio2 = s / sqrt_pred
        ratio2_err = plotter.tools.propagate_err(
            s_err,
            sqrt_pred_err,
            "/",
            s,
            sqrt_pred,
        )
        hep.histplot(
            ratio2,
            edges,
            histtype="errorbar",
            yerr=ratio2_err,
            ax=rax2,
            color="Black",
        )
        rax2.set_ylabel(
            "$\mathrm{S}/\sqrt{\mathrm{Pred.}}$", horizontalalignment="center"
        )
        # rax2.set_ylabel(
        #     r"$ \frac{\mathrm{S}}{\sqrt{\mathrm{Pred.}}}$", horizontalalignment="center"
        # )
    ax.set_ylabel("Events")
    rax.set_ylabel(
        r"$ \frac{\mathrm{Data}}{\mathrm{Pred.}}$", horizontalalignment="center"
    )

    labels = plotter.tools.plotLabel(histkey, ax)

    if "eta" in histkey or "phi" in histkey or "dR" in histkey:
        hep.atlas.set_xlabel(f"{labels['var']}")
    else:
        hep.atlas.set_xlabel(f"{labels['var']} [GeV]")
        ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))

    # hep.mpl_magic()
    newLim = list(ax.get_ylim())
    newLim[1] = newLim[1] * 100
    ax.set_ylim(newLim)

    plt.tight_layout()
    if s_over_b:
        rax2.get_xaxis().get_offset_text().set_position((2, 0))
    else:
        rax.get_xaxis().get_offset_text().set_position((2, 0))

    # to show subticks of logplot
    ax.yaxis.set_major_locator(mticker.LogLocator(numticks=999))
    ax.yaxis.set_minor_locator(mticker.LogLocator(numticks=999, subs="auto"))
    # rax2.yaxis.set_major_locator(mticker.LogLocator(numticks=999))
    # rax2.yaxis.set_minor_locator(mticker.LogLocator(numticks=999, subs="auto"))
    if bkg_estimate:
        plt.savefig(plot_path + f"{histkey}_bkg_estimate_ratio.pdf")
        log.info("saving to : " + plot_path + f"{histkey}_bkg_estimate_ratio.pdf")
    else:
        plt.savefig(plot_path + f"{histkey}_ratio.pdf")
        log.info("saving to : " + plot_path + f"{histkey}_ratio.pdf")

    plt.close(fig)


def compareABCD(hists, plot_path, histkey, lowTaghistkey, subtract_ttbar, factor=None):
    data = hists["run2"][histkey]["h"]
    data_err = hists["run2"][histkey]["err"]
    data_2 = hists["run2"][lowTaghistkey]["h"]
    data_err_2 = hists["run2"][lowTaghistkey]["err"]
    if subtract_ttbar:
        tt = hists["ttbar"][histkey]["h"]
        tt_err = hists["ttbar"][histkey]["err"]
        tt_2 = hists["ttbar"][lowTaghistkey]["h"]
        tt_err_2 = hists["ttbar"][lowTaghistkey]["err"]

    else:
        tt = np.zeros_like(data)
        tt_err = np.zeros_like(data_err)
        tt_2 = np.zeros_like(data_2)
        tt_err_2 = np.zeros_like(data_err_2)
    edges = hists["run2"][histkey]["edges"]

    if factor:
        data, edges_, data_err = plotter.tools.factorRebin(
            h=data,
            edges=edges,
            factor=factor,
            err=data_err,
        )
        tt, edges_, tt_err = plotter.tools.factorRebin(
            h=tt,
            edges=edges,
            factor=factor,
            err=tt_err,
        )

        data_2, edges_, data_err_2 = plotter.tools.factorRebin(
            h=data_2,
            edges=edges,
            factor=factor,
            err=data_err_2,
        )
        tt_2, edges_, tt_err_2 = plotter.tools.factorRebin(
            h=tt_2,
            edges=edges,
            factor=factor,
            err=tt_err_2,
        )
        edges = edges_

    observable = histkey.split("_NOSYS")[0]
    w_CR, err_w_CR = fitting.tools.get_bkg_weight(
        subtract_ttbar=False, fit_variable=observable
    )

    jj = data - tt
    jj_err = plotter.tools.propagate_err(sigmaA=data_err, sigmaB=tt_err, operation="-")

    jj_2 = data_2 - tt_2
    jj_err_2 = plotter.tools.propagate_err(
        sigmaA=data_err_2, sigmaB=tt_err_2, operation="-"
    )

    plt.figure()
    fig, (ax, rax) = plt.subplots(
        nrows=2,
        ncols=1,
        figsize=(6, 6),
        gridspec_kw={"height_ratios": (3, 1)},
        sharex=True,
    )

    hep.histplot(
        jj,
        edges,
        histtype="errorbar",
        yerr=jj_err,
        label="data, 2 GN2X",
        ax=ax,
    )

    bkg_estimate = jj_2 * w_CR
    bkg_estimateErr = (
        plotter.tools.propagate_err(
            sigmaA=jj_err_2,
            sigmaB=np.ones(jj_err_2.shape) * err_w_CR,
            operation="*",
            A=jj_2,
            B=np.ones(jj_err_2.shape) * w_CR,
        ),
    )
    #  bkg_estimateErr = (
    #         plotter.tools.propagate_err(
    #             sigmaA=jj_err_2, sigmaB=err_w_CR, operation="*", A=jj, B=w_CR
    #         ),
    #     )

    hep.histplot(
        bkg_estimate,
        edges,
        histtype="errorbar",
        yerr=bkg_estimateErr,
        label=r"$w_\mathrm{CR}\times$data, 1 GN2X",
        ax=ax,
    )

    hep.histplot(
        jj / bkg_estimate,
        edges,
        histtype="errorbar",
        yerr=plotter.tools.propagate_err(
            sigmaA=jj_err,
            sigmaB=bkg_estimateErr,
            operation="/",
            A=jj,
            B=bkg_estimate,
        ),
        color="Black",
        ax=rax,
    )
    print(
        jj / bkg_estimate,
    )
    normErrLow = (jj - jj_err) / jj
    normErrHigh = (jj + jj_err) / jj

    rax.fill_between(
        edges,
        plotter.tools.fill_stat_holes(normErrLow),
        plotter.tools.fill_stat_holes(normErrHigh),
        color="tab:blue",
        linewidth=0,
        alpha=0.3,
        step="post",
        label="2 GN2X stat. uncertainty",
    )
    rax.legend(loc="lower left", fontsize=9)

    # hep.atlas.text(" Simulation", loc=1)
    ax.legend()
    ax.set_ylabel("Events")
    rax.set_ylabel("Ratio", horizontalalignment="center")
    rax.set_ylim([0, 2])
    rax.axhline(y=1.0, color="tab:red", linestyle="-")

    if "m_hh" in histkey:
        hep.atlas.set_xlabel("$m_\mathrm{HH}$ [TeV]")
    else:
        hep.atlas.set_xlabel("Neural Network Score")
    # ax.set_xlim([0, 1])

    # hep.atlas.label(
    #     # data=False,
    #     lumi="140.0",
    #     loc=1,
    #     ax=ax,
    #     llabel="Internal",
    # )
    bb_tag = histkey.split("_")[-1]
    sample_text = "Data"

    ax.text(
        x=0.05,
        y=0.95,
        s=r"Run 2, VBF $HH \rightarrow 4b$" + "\n" + sample_text,
        transform=ax.transAxes,
        verticalalignment="top",
        horizontalalignment="left",
        fontsize=12,
    )

    newLim = list(ax.get_ylim())
    newLim[1] = newLim[1] * 1.2
    ax.set_ylim(newLim)
    plt.xlim(edges[0], edges[-1])

    plt.tight_layout()
    rax.get_xaxis().get_offset_text().set_position((2, 0))
    # ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))

    # plotter.tools.plotLabel(histkey, ax)
    log.info(plot_path + histkey + "_compareABCD.pdf")

    plt.savefig(plot_path + histkey + "_compareABCD.pdf")
    plt.close()


def limits():
    fitResults = json.load(
        open("/lustre/fs22/group/atlas/freder/hh/run/fitResults.json")
    )
    fig, ax = plt.subplots()
    ax.plot(
        fitResults["k2v"],
        fitResults["obs"],
        color="black",
    )
    ax.plot(
        fitResults["k2v"],
        fitResults["exp"],
        color="black",
        linestyle="dashed",
    )
    ax.fill_between(
        fitResults["k2v"],
        fitResults["-2s"],
        fitResults["2s"],
        color="hh:darkyellow",
        linewidth=0,
    )
    ax.fill_between(
        fitResults["k2v"],
        fitResults["-1s"],
        fitResults["1s"],
        color="hh:medturquoise",
        linewidth=0,
    )

    ax.set_ylabel(r"95% CL upper limit on $\sigma_{VBF,HH}$ (fb)")
    ax.set_xlabel(r"$\kappa_{\mathrm{2v}}$")
    ax.xaxis.set_major_locator(mticker.MaxNLocator(integer=True))
    ax.set_yscale("log")
    ax.legend(
        [
            "Observed",
            "Expected",
            "Expected Limit $\pm 1\sigma$",
            "Expected Limit $\pm 2\sigma$",
        ]
    )
    hep.atlas.label(
        # data=False,
        lumi="140.0",
        loc=1,
        ax=ax,
        llabel="Internal",
    )

    plt.tight_layout()
    log.info(plot_path + "limit.pdf")
    plt.savefig(plot_path + "limit.pdf")
    plt.close()


def simple_ratio(
    plot_path,
    var_1_label,
    var_2_label,
    var_1_values,
    var_2_values,
    var_1_err,
    var_2_err,
    edges,
    ratio_values,
    ratio_err,
    x_label,
    text=None,
    x_lim=None,
    y_label="Events",
    normalize=False,
    ratio_lim=[0, 2],
    figsize=(6, 6),
    grid=False,
    third_panel=False,
):
    log.info("Plotting ratio of " + var_1_label + "/" + var_2_label)
    plt.figure()

    if third_panel:
        fig, (ax, mid_ax, rax) = plt.subplots(
            3,
            1,
            gridspec_kw={"height_ratios": [4, 1, 1]},
            figsize=figsize,
            sharex=True,
        )
        # fig.subplots_adjust(hspace=5)
    else:
        fig, (ax, rax) = plt.subplots(
            nrows=2,
            ncols=1,
            figsize=figsize,
            gridspec_kw={"height_ratios": (3, 1)},
            sharex=True,
        )

    if grid:
        ax.grid(zorder=0)
        rax.grid(zorder=0)
    hep.histplot(
        var_1_values,
        edges,
        histtype="step",
        yerr=var_1_err,
        label=var_1_label,
        ax=ax,
        density=normalize,
        linewidth=2,
    )
    hep.histplot(
        var_2_values,
        edges,
        histtype="step",
        yerr=var_2_err,
        label=var_2_label,
        ax=ax,
        density=normalize,
        linewidth=2,
    )

    hep.histplot(
        ratio_values,
        edges,
        histtype="errorbar",
        yerr=ratio_err,
        color="Black",
        ax=rax,
    )

    rax.set_ylim(ratio_lim)
    # draw line at 1.0
    rax.axhline(y=1.0, color="tab:red", linestyle="-")

    ax.set_ylabel(y_label)
    rax.set_ylabel("Ratio", horizontalalignment="center")

    if not x_label:
        labels = plotter.tools.plotLabel(var_1_label, ax)
        if "eta" in var_1_label or "phi" in var_1_label or "dR" in var_1_label:
            hep.atlas.set_xlabel(f"{labels['var']}")
        else:
            hep.atlas.set_xlabel(f"{labels['var']} [GeV]")
            ax.xaxis.set_major_formatter(plotter.tools.OOMFormatter(3, "%1.1i"))

    else:
        hep.atlas.set_xlabel(x_label)

    # hep.mpl_magic()
    newLim = list(ax.get_ylim())
    newLim[1] = newLim[1] * 1.3
    ax.set_ylim(newLim)
    if x_lim:
        ax.set_xlim(x_lim)
    plt.xlim(edges[0], edges[-1])

    rax.legend(loc="upper right")
    ax.legend(loc="upper right")

    plt.tight_layout()

    rax.get_xaxis().get_offset_text().set_position((2, 0))

    if text:
        ax.text(
            x=0.075,
            y=0.95,
            s=r"Run 2, VBF $HH \rightarrow 4b$" + "\n" + text,
            transform=ax.transAxes,
            verticalalignment="top",
            horizontalalignment="left",
            fontsize=12,
        )
    plt.savefig(plot_path)
    log.info("saving to : " + plot_path)

    return fig, ax, rax


def nominal_hist(
    hists,
    plot_path,
    histkey,
    log_scale=False,
):
    log.info("Plotting " + histkey)
    k2v0 = hists["k2v0"][histkey]["h"]
    vbf = hists["SM"][histkey]["h"]
    ggf = hists["ggf"][histkey]["h"]
    s_err = hists["k2v0"][histkey]["err"]
    data = hists["run2"][histkey]["h"]
    data_err = hists["run2"][histkey]["err"]
    edges = hists["run2"][histkey]["edges"]

    jj, jj_err = get_multijets(hists, histkey, bkg_estimate=True, subtract_ttbar=False)
    # lowTaghistkey = histkey[:-1] + "1"

    # jj_err = hists["run2"][lowTaghistkey]["err"]
    pred = jj
    pred_err = jj_err

    plt.figure(figsize=(6, 5))
    ax = plt.gca()

    hep.histplot(
        jj,
        edges,
        stack=False,
        histtype="fill",
        # yerr=True,
        label=["Background"],
        ax=ax,
        color=["hh:darkyellow"],
        edgecolor="black",
        linewidth=0.5,
    )

    # error stackplot
    ax.fill_between(
        edges,
        np.append(pred - pred_err, 0),
        np.append(pred + pred_err, 0),
        color="dimgrey",
        linewidth=0,
        alpha=0.3,
        step="post",
        label="Stat. + Bkg Estimate Unc.",
    )

    # Signal

    hep.histplot(
        [vbf * 1000, ggf * 50, k2v0],
        edges,
        histtype="step",
        # yerr=True,
        label=[
            rf"1000 x SM VBF",
            rf"50 x SM GGF",
            rf"$\kappa_\mathrm{{2V}}=0$ VBF",
        ],
        ax=ax,
        # color="hh:darkyellow",  # "orangered",
        linewidth=1.5,
    )

    ax.legend(loc="upper right")
    # ax.autoscale()
    # ax.get_ylim()
    # if ymax:
    #     ax.set_ylim([1e-3, 1e6])
    # else:
    #     ax.set_ylim([1e-3, 1e6])
    if log_scale:
        ax.set_yscale("log")

    # ax.set_ylim([0.0, 8])
    ax.set_ylabel("Events")

    plt.tight_layout()

    if "m_hh" in histkey:
        ax.get_xaxis().get_offset_text().set_position((2, 0))

    labels = plotter.tools.plotLabel(histkey, ax)

    if "m_hh" in histkey:
        hep.atlas.set_xlabel("$m_\mathrm{HH}$ [TeV]")
        plt.xlim(edges[0], edges[-1])
    else:
        hep.atlas.set_xlabel("Neural Network Score")

    # hep.mpl_magic()
    newLim = list(ax.get_ylim())
    newLim[1] = newLim[1] * 1.05
    ax.set_ylim(newLim)

    # plt.tight_layout()

    plt.savefig(plot_path + f"{histkey}_nominal_hist.pdf")
    log.info("saving to : " + plot_path + f"{histkey}_nominal_hist.pdf")
