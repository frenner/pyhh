import matplotlib.ticker
import mplhep as hep
import numpy as np


def plotLabel(histkey, ax):
    keyParts = histkey.split(".")
    var = keyParts[0]
    sel = keyParts[1]
    selParts = sel.split("_")
    varParts = var.split("_")

    if "pt" in varParts[0]:
        varParts.pop(0)
        varParts.insert(0, "T")
        varParts.insert(0, "p")
    labels = {}
    if "CR" in histkey:
        labels["region"] = "Control Region"
    elif "VR" in histkey:
        labels["region"] = "Validation Region"
    elif "SR" in histkey:
        labels["region"] = "Signal Region"
    else:
        labels["region"] = ""

    if len(selParts) > 1:
        labels["btag"] = "GN2X tags: " + selParts[-1]
    else:
        labels["btag"] = ""

    labels["vbf"] = "VBF 4b"

    # var
    labels["var"] = varParts[0] + "$_{\mathrm{" + ",".join(varParts[1:]) + "}}$"

    labels["plot"] = ("\n").join(
        ["Run 2, " + labels["vbf"], labels["region"]]  # + ", " + labels["btag"]]
    )

    # hep.atlas.label(
    #         # data=False,
    #         lumi="140.0",
    #         loc=1,
    #         ax=ax,
    #         llabel="Internal",
    #     ),
    # print(ax.__dict__)
    # anchored_label = AnchoredText(
    #     s=hep.atlas.label(
    #         # data=False,
    #         lumi="140.0",
    #         loc=1,
    #         ax=ax,
    #         llabel="Internal",
    #     ),
    #     loc="upper left",
    #     frameon=False,
    # )

    # anchored_text = AnchoredText(
    #     s=labels["plot"],
    #     loc="upper left",
    #     frameon=False,
    # )
    # ax.add_artist(anchored_label)

    # ax.add_artist(anchored_text)

    # hep.atlas.label(
    #     # data=False,
    #     lumi="140.0",
    #     loc=1,
    #     ax=ax,
    #     llabel="Internal",
    # )
    ax.text(
        x=0.05,
        y=0.95,
        s=labels["plot"],
        transform=ax.transAxes,
        verticalalignment="top",
        horizontalalignment="left",
        fontsize=12,
    )

    return labels


def EfficiencyErrorBayesian(k, n, bUpper):
    # per bin
    if n == 0:
        if bUpper:
            return 0
        else:
            return 1

    firstTerm = ((k + 1) * (k + 2)) / ((n + 2) * (n + 3))
    secondTerm = ((k + 1) ** 2) / ((n + 2) ** 2)
    error = np.sqrt(firstTerm - secondTerm)
    ratio = k / n
    if bUpper:
        if (ratio + error) > 1:
            return 1.0
        else:
            return ratio + error
    else:
        if (ratio - error) < 0:
            return 0.0
        else:
            return ratio - error


def getEfficiencyErrors(passed, reference):
    """
    get relative upper and lower error bar positions by calculating a bayesian
    Error based on:
    # https://indico.cern.ch/event/66256/contributions/2071577/attachments/1017176/1447814/EfficiencyErrors.pdf
    # https://lss.fnal.gov/archive/test-tm/2000/fermilab-tm-2286-cd.pdf
    # http://phys.kent.edu/~smargeti/STAR/D0/Ullrich-Errors.pdf

    Parameters
    ----------
    passed : np.ndarray
        values that passed a cut
    reference : np.ndarray
        baseline

    Returns
    -------
    np.ndarray
        2xN array holding relative errors to values
    """
    upper_err = np.array(
        [
            EfficiencyErrorBayesian(passed, reference, bUpper=True)
            for passed, reference in zip(passed, reference)
        ]
    )
    lower_err = np.array(
        [
            EfficiencyErrorBayesian(passed, reference, bUpper=False)
            for passed, reference in zip(passed, reference)
        ]
    )

    value_position = passed / reference
    relative_errors = np.array([value_position - lower_err, upper_err - value_position])
    return relative_errors


class OOMFormatter(matplotlib.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        matplotlib.ticker.ScalarFormatter.__init__(
            self, useOffset=offset, useMathText=mathText
        )

    def _set_order_of_magnitude(self):
        self.orderOfMagnitude = self.oom

    def _set_format(self, vmin=None, vmax=None):
        self.format = self.fformat
        if self._useMathText:
            self.format = r"$\mathdefault{%s}$" % self.format


def CumulativeEfficiencies(hists, baseline, stopCumulativeFrom):
    # calculate cumulatives and errors for efficiency plots
    ratio = []
    cumulatives = []
    baseline_err = []
    cumulatives_err = []

    for i in range(len(hists)):
        ratio.append(hists[i] / baseline)
        print(ratio)
        if i == 0:
            cumulatives.append(ratio[0])
        elif i >= stopCumulativeFrom:
            cumulatives.append(cumulatives[stopCumulativeFrom - 2] * ratio[i])
        else:
            cumulatives.append(cumulatives[i - 1] * ratio[i])
        # error wrt baseline
        baseline_err.append(getEfficiencyErrors(passed=hists[i], total=baseline))

    # error propagation
    for i in range(len(hists)):
        err_sum = 0
        if i == 0:
            cumulatives_err.append(baseline_err[0])
            continue
        elif i >= stopCumulativeFrom:
            for k in range(stopCumulativeFrom - 1):
                err_sum += pow((baseline_err[k] / ratio[k]), 2)
            err_sum += pow((baseline_err[i] / ratio[i]), 2)
        else:
            for k in range(i):
                err_sum += pow((baseline_err[k] / ratio[k]), 2)
        propagated_err = np.array(cumulatives[i]) * np.sqrt(err_sum)
        cumulatives_err.append(propagated_err)
    #         triggerPass = nTriggerPass_truth_mhh / nTruthEvents
    #         twoLargeR = triggerPass * nTwoLargeR_truth_mhh / nTruthEvents

    #         # errors
    #         nTriggerPass_err = tools.getEfficiencyErrors(
    #             passed=nTriggerPass_truth_mhh, total=nTruthEvents
    #         )
    #         nTwoLargeR_err = tools.getEfficiencyErrors(
    #             passed=nTwoLargeR_truth_mhh, total=nTruthEvents
    #         )
    #         # error propagation
    #         twoLargeR_err = twoLargeR * np.sqrt(
    #             np.power(nTriggerPass_err / triggerPass, 2)
    #             + np.power(nTwoLargeR_err / twoLargeR, 2)
    #         )
    return cumulatives, cumulatives_err


def propagate_err(
    sigmaA,
    sigmaB,
    operation,
    A=None,
    B=None,
    exp=None,
):
    """
    calculate propagated error based from
    https://en.wikipedia.org/wiki/Propagation_of_uncertainty

    Parameters
    ----------
    sigmaA : ndarray
        standard error of A
    sigmaB : ndarray
        standard error of B
    operation : str
        +, -, *, /, ^
    A : ndarray, optional
        A values, by default None
    B : ndarray, optional
        B values, by default None
    exponent : ndarray
        for power operation on A
    Returns
    -------
    np.ndarray
        propagated error
    """

    if "+" in operation or "-" in operation:
        error = np.sqrt(np.power(sigmaA, 2) + np.power(sigmaB, 2))
    if "*" in operation:
        error = np.abs(A * B) * np.sqrt(
            np.power(np.divide(sigmaA, A, out=np.zeros_like(sigmaA), where=A != 0), 2)
            + np.power(np.divide(sigmaB, B, out=np.zeros_like(sigmaB), where=B != 0), 2)
        )
    if "/" in operation:
        error = np.abs(A / B) * np.sqrt(
            np.power(np.divide(sigmaA, A, out=np.zeros_like(sigmaA), where=A != 0), 2)
            + np.power(np.divide(sigmaB, B, out=np.zeros_like(sigmaB), where=B != 0), 2)
        )
    if "^" in operation and exp != None:
        error = np.abs(np.power(A, exp) / A * (exp * sigmaA))

    return error


def factorRebin(
    h,
    edges,
    factor=int(2),
    err=None,
):
    """
    rebin a histogram with equally spaced bins, the last resulting bin entry not
    necessarily has the same bin width as the other ones.

    Parameters
    ----------
    h : ndarray
        histogram counts
    edges : ndarray
         edges to h
    factor : int, optional
        factor by which to reduce the hist bins, by default int(2)
    err : ndarray, optional
        error to h, by default None

    Returns
    -------
    ndarray, ndarray, ndarray
        newH, newEdgesIndices, newErr

    """

    newBinNr = int(h.shape[0] / factor)
    # edges indices of old bins that suit the factor of new bins e.g. for 99
    # bins and a factor of 10 gives
    # [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
    newEdgesIndices = subintervals(0, h.shape[0], newBinNr)
    # get indices ranges in old bins for new bins, append rest if last index
    # not matching original last index
    # [[0, 9], [10, 19], [20, 29], [30, 39], [40, 49], [50, 59], [60, 69], [70, 79], [80, 89], [90, 99]]
    intervals = []
    for i, j in zip(newEdgesIndices[:-1], newEdgesIndices[1:]):
        intervals += [[i, j - 1]]
    intervals[-1][-1] = h.shape[0]
    if newEdgesIndices[-1] != h.shape[0]:
        newEdgesIndices[-1] = h.shape[0]

    hNew = []
    errNew = []
    for slice in intervals:
        hNew.append(np.sum(h[slice[0] : slice[1]]))
        # error propagate
        if err is not None:
            squaredSigma = 0
            for e in err[slice[0] : slice[1]]:
                squaredSigma += np.power(e, 2)
            errNew.append(np.sqrt(squaredSigma))

    edgesNew = edges[newEdgesIndices]
    return np.array(hNew), np.array(edgesNew), np.array(errNew)


def subintervals(a, b, n):
    """n subintervals in the range [a,b]"""

    lst = [int(a + x * (b - a) / n) for x in range(n + 1)]
    return lst


def missing_elements(L):
    """find missing values in int sequence"""
    start, end = L[0], L[-1]
    return sorted(set(range(start, end + 1)).difference(L))


def fill_stat_holes(a):
    """
    append one nan and replace nan next to hole with value before to get stat
    error bars correct
    """

    a = np.append(a, np.nan)
    nonNanIndices = np.where(~np.isnan(a))[0]
    for i in nonNanIndices:
        if np.isnan(a[i + 1]):
            a[i + 1] = 0

    # correct for last value
    if np.isnan(a[-1]):
        a[-1] = a[-2]

    return a


def savegrid(ims, plotName, rows=None, cols=None, fill=True, showax=False):
    if rows is None != cols is None:
        raise ValueError("Set either both rows and cols or neither.")

    if rows is None:
        rows = len(ims)
        cols = 1

    gridspec_kw = {"wspace": 0, "hspace": 0} if fill else {}
    fig, axarr = plt.subplots(rows, cols, dpi=500, gridspec_kw=gridspec_kw)

    if fill:
        bleed = 0
        fig.subplots_adjust(
            left=bleed, bottom=bleed, right=(1 - bleed), top=(1 - bleed)
        )

    for ax, im in zip(axarr.ravel(), ims):
        ax.imshow(im)
        if not showax:
            ax.set_axis_off()

    kwargs = {"pad_inches": 0.01} if fill else {}
    fig.savefig(plotName, **kwargs)


def makeGrid():
    btags = ["2b2b", "2b2j"]
    regions = ["CR", "VR", "SR"]
    # vbf = ["", "noVBF"]

    for var in histkeys:
        log.info(f"making grid for variable {var}")
        plotsPerGrid = []
        for btag in btags:
            for reg in regions:
                if "massplane" in var:
                    plot = var + "." + reg + "_" + btag
                else:
                    plot = var + "." + reg + "_" + btag + "_ratio"
                plot += ".pdf"
                plotsPerGrid += [plot]
        plotsPerGridWithPath = [plotPath + x for x in plotsPerGrid]
        y = len(regions)
        x = len(btags)

        ims = [
            np.array(convert_from_path(file, 500)[0]) for file in plotsPerGridWithPath
        ]

        savegrid(
            ims,
            f"/lustre/fs22/group/atlas/freder/hh/run/plots/grids/{var}.png",
            rows=x,
            cols=y,
        )

    # for bkg_estimate
    for var in histkeys:
        log.info(f"making grid for variable {var} Background estimate")
        plotsPerGrid = []
        btag = "2b2b"
        for suffix in ["bkg_estimate_ratio", "ratio"]:
            for reg in ["VR"]:
                if "massplane" not in var:
                    plot = var + "." + reg + "_" + btag + "_" + suffix
                    plot += ".pdf"
                    plotsPerGrid += [plot]
        plotsPerGridWithPath = [plotPath + x for x in plotsPerGrid]
        y = 2
        x = 1

        ims = [
            np.array(convert_from_path(file, 500)[0]) for file in plotsPerGridWithPath
        ]

        savegrid(
            ims,
            f"/lustre/fs22/group/atlas/freder/hh/run/plots/grids/{var}_bkg_estimate.png",
            rows=x,
            cols=y,
        )
