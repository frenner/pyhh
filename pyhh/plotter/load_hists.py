import h5py
import numpy as np

files = {}

# this does not work with bkg estimate...
base_path = "/lustre/fs22/group/atlas/freder/hh/run/histograms/"
# base_path = "/lustre/fs22/group/atlas/freder/hh/run/histograms/no_vbf_cut/"

files["k2v0"] = f"{base_path}/hists-l1cvv0cv1.h5"
files["SM"] = f"{base_path}/hists-l1cvv1cv1.h5"
files["run2"] = f"{base_path}/hists-run2.h5"
files["ggf"] = f"{base_path}/hists-cHHH01d0.h5"

# files[
#     "SM"
# ] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/user.frenner.HH4b.2023_05_10.502970.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv1cv1.e8263_s3681_r13145_p5440_TREE/user.frenner.33394664._000001.output-hh4b.root.h5"
# files[
#     "ttbar"
# ] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/user.frenner.HH4b.2023_05_10.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.e6337_s3681_r13167_p5631_TREE/user.frenner.33394923._000037.output-hh4b.root.h5"
# files[
#     "run2"
# ] = "/lustre/fs22/group/atlas/freder/hh/run/histograms/user.dabattul.ntup_data_24022023.data18_13TeV.periodD_TREE/user.dabattul.32511558._000049.output-hh4b.root.h5"


def get_hist(file, name):
    # access [1:-1] to remove underflow and overflow bins
    h = np.array(file[name]["histogram"][1:-1])
    hRaw = np.array(file[name]["histogramRaw"][1:-1])
    edges = np.array(file[name]["edges"][:])
    err = np.sqrt(file[name]["w2sum"][1:-1])
    return {"h": h, "hRaw": hRaw, "edges": edges, "err": err}


def get_2d_hist(file, name):
    h = np.array(file[name]["histogram"][1:-1, 1:-1])
    hRaw = np.array(file[name]["histogramRaw"][1:-1, 1:-1])
    xbins = np.array(file[name]["edges"][0][1:-1])
    ybins = np.array(file[name]["edges"][1][1:-1])
    err = np.sqrt(file[name]["w2sum"][1:-1, 1:-1])
    return {"h": h, "hRaw": hRaw, "xbins": xbins, "ybins": ybins, "err": err}


def load(file, histkeys=None, systs=False):
    hists = {}
    if histkeys == None:
        for key in file.keys():
            if systs and "NOSYS" not in key:
                continue
            if "massplane" in key:
                hists[key] = get_2d_hist(file, key)
            else:
                hists[key] = get_hist(file, key)
    else:
        for key in histkeys:
            if systs and "NOSYS" not in key:
                continue
            if "massplane" in key:
                hists[key] = get_2d_hist(file, key)
            else:
                hists[key] = get_hist(file, key)

    return hists


def run(histkeys=None, systs=False):
    all_hists = {}
    # loops over datatype files
    for sample, file in files.items():
        with h5py.File(file, "r") as f:
            all_hists[sample] = load(f, histkeys, systs=False)

    return all_hists
