#!/bin/bash

# exit immediately if one fails
set -e

signal=/lustre/fs22/group/atlas/freder/hh/samples/user.frenner.EJ_2023_09_18.502971.MGPy8EG_hh_bbbb_vbf_novhh_l1cvv0cv1.e8263_s3681_r13144_p5658_TREE/user.frenner.34863513._000001.output-hh4b.root
ttbar=/lustre/fs22/group/atlas/freder/hh/samples/user.frenner._EJ_2023_08_21.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.e6337_s3681_r13167_p5631_TREE/user.frenner.34429725._000001.output-hh4b.root
data=/lustre/fs24/group/atlas/dbattulga/ntup_SH_Jun2023/Data/user.dabattul.ntup_phys_06062023.data15_13TeV.periodD_TREE/user.dabattul.33660315._000001.output-hh4b.root

echo -e "\n\e[33mTest pyhh selector on MC20_SM \e[0m"
pyhh select --file $signal --debug

# echo -e "\n\e[33mTest pyhh selector on TTBAR \e[0m"
# pyhh select --file $ttbar --debug

echo -e "\n\e[33mTest pyhh selector on DATA \e[0m"
pyhh select --file $data --debug

if [ "$1" = "systs" ]; then
    echo -e "\n\e[33mTest pyhh selector with systematics on MC20_SM \e[0m"
    pyhh select --file $signal --debug --do_systematics

    # echo -e "\n\e[33mTest pyhh selector with systematics on TTBAR \e[0m"
    # pyhh select --file $ttbar --debug --do_systematics

fi
